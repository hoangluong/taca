<?php
$user_data = $this->session->userdata('user_data');
?>
<!DOCTYPE html>
<html class="no-js no-svg">
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>/publics/css/style.css">
    <script src="http://wowthemez.com/templates/sintex/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>

<body>
<div id="page" class="site">
    <div class="header">
        <div class="container-fluid">
            <div class="col-md-3">
                <div class="logo"><a href="<?php echo site_url()?>"><img src="<?php echo base_url() ?>/publics/images/logo.png"
                                                  alt=""></a></div>
            </div>
            <div class="col-md-9">
                <div class="member-info right">
                    <?php
                    if (isset($user_data['id'])) {
                        ?>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-haspopup="true" aria-expanded="false"><?php echo $user_data['name']; ?> <span
                                        class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo base_url('course/my') ?>">Khóa học của tôi</a></li>
                                    <li><a href="#">Thông tin</a></li>

                                    <li role="separator" class="divider"></li>
                                    <li><a href="<?php echo base_url('logout') ?>">Thoát</a></li>
                                </ul>
                            </li>
                        </ul>
                    <?php } else { ?>
                        <ul class="loginout navbar-right">
                            <li><a href="<?php echo base_url('login') ?>">Đăng nhập</a></li>
                            <li><a href="" class="btn btn-primary">Đăng ký</a></li>
                        </ul>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="main courses">
        <div class="container-fluid">
            <?php $this->load->view($template); ?>

        </div>
    </div>
    <div class="footer">
        <div class="container">
            <div class="col-md-12">
               <div class="f-title"> HỌC VIỆN TACA</div>
            </div>
            <div class="col-md-4">

                <ul>
                    <li class="title">Trụ sở chính</li>
                    <li>Tầng 4 Tòa Sea Bank</li>
                    <li>Số 170 Trần Duy Hưng,Cầu Giấy, Hà Nội</li>
                    <li>0985 611 911</li>
                    <li>Support@taca.edu.vn</li>
                </ul>
            </div>
            <div class="col-md-4">

                <ul>
                    <li class="title">Văn phòng TP.HCM</li>
                    <li>Trung Tâm bồi dưỡng chính trị</li>
                    <li>39 Trần Quốc Thảo, Phường 6, Quận 3, TP.HCM</li>
                    <li>0941 611 911 </li>

                </ul>
            </div>
        </div>
    </div>

</div>
</body>
