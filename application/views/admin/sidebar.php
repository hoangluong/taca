<?php $ci =& get_instance();
$current_url = base_url().ltrim($_SERVER['REQUEST_URI'], '/');
$user_data = $ci->session->userdata('user_data');
$menus = array(
    'dashboard' => array(
        'icon' => 'fa fa-dashboard',
        'title' => 'Dashboard'
    )
);

$menus['main'] = array(
    'header' => true,
    'title' => 'Courses Management System'
);
$menus['admin/course'] = array(
    'icon' => 'fa fa-fw fa-database',
    'title' => 'Khóa học'
);
$menus['1'] = array(
    'header' => 'true',
    'title' => 'Giáo viên'
);
$menus['admin/classes'] = array(
    'icon' => 'fa fa-fw fa-database',
    'title' => 'Quản lý lớp học'
);
$menus['423423'] = array(
    'icon' => 'glyphicon glyphicon-user',
    'title' => 'Danh sách học viên'
);
$menus['4234231'] = array(
    'icon' => 'glyphicon glyphicon-user',
    'title' => 'Phản hồi học viên'
);
$menus['42342312'] = array(
    'icon' => 'glyphicon glyphicon-user',
    'title' => 'Bài tập cuối module'
);
$menus['423421312'] = array(
    'icon' => 'glyphicon glyphicon-user',
    'title' => 'Bài tập cuối khóa'
);

if($user_data['role']==1 || $user_data['role']==3){
    $menus['manager'] = array(
        'header' => true,
        'title' => 'Account Management'
    );
    $menus['admin/user'] = array(
        'icon' => 'glyphicon glyphicon-user',
        'title' => 'Quản lý người dùng'
    );
}
$menus['auth/setting'] = array(
    'icon' => 'glyphicon glyphicon-cog',
    'title' => 'Thông tin cá nhân'
);

?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu" style="margin-top:8px;">
            <?php foreach($menus as $path=>$item){
                if(isset($item['header']) && $item['header']){?>
                    <li class="header"><?php echo $item['title'];?></li>
                    <?php continue;
                }
                $url = base_url($path);
                $is_active = strpos($current_url, '/'.$path)!==false ? true : false;
                $have_sub = isset($item['sub']) && !empty($item['sub']);
                if($have_sub){
                    foreach($item['sub'] as $sub_path=>$sub){
                        if(base_url($sub_path)==$current_url || strpos($current_url, '/'.$sub_path)!==false){
                            $is_active = true;
                            break;
                        }
                    }
                }
                ?>
                <li class="<?php if($is_active){echo 'active ';}?>treeview">
                    <a href="<?php echo $url;?>">
                        <i class="<?php echo $item['icon']?>"></i>
                        <span><?php echo $item['title'];?></span>
                        <?php if($have_sub){?>
                        <i class="fa fa-angle-left pull-right"></i>
                        <?php }?>
                    </a>
                    <?php if($have_sub){?>
                        <ul class="treeview-menu">
                            <?php foreach($item['sub'] as $sub_path=>$sub){?>
                            <li class="<?php if(base_url($sub_path)==$current_url||strpos($current_url, $sub_path)!==false){echo 'active';}?>">
                                <a href="<?php echo base_url($sub_path);?>"><?php echo $sub;?></a>
                            </li>
                            <?php }?>
                        </ul>
                    <?php }?>
                </li>
            <?php }?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
