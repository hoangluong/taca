<?php $this->load->view('admin/header') ?>
<?php $this->load->view('admin/sidebar'); ?>
    <div class="content-wrapper" style="min-height: 946px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo $page_title; ?></h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <?php $this->load->view($template); ?>
        </section><!-- /.content -->
    </div>
    <div class="showTreeCourse">
        <div class="showTreeCourseClose">X</div>
        <div class="treeContent">

        </div>
    </div>

    <style>
        .showTreeCourse {
            position: absolute;
            background: #00000090;
            top: 0;
            bottom: 0;
            right: 0;
            left: 50%;
            display: none;
            z-index: 99999;
            padding: 10px;
        }
        .showTreeCourse .treeContent{
            background: #ffffff;
            padding: 10px;
        }
        .tree-module>a{
            font-size: 15px;
            color: #333300;
            display: block;
            padding: 10px;
            text-transform: uppercase;
        }
        .tree-module,.tree-video{
            padding: 5px;
            list-style: none;
        }
        .tree-video a{
            padding: 0;
        }
    </style>

<?php $this->load->view('admin/footer') ?>