<?php $asset = base_url('assets/admin/');
?><!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>


    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php if(isset($meta_title) && $meta_title){
            echo $meta_title;
        }else{
            if (isset($page_title)) {
                echo strip_tags($page_title);
            }else{echo '';}
        }?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo $asset.'bootstrap/css/bootstrap.min.css'; ?>">
    <!-- Font IonIcons -->
    <link rel="stylesheet" href="<?php echo $asset.'css/ionicons.min.css'; ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo $asset.'css/font-awesome.min.css'; ?>">
<?php if (isset($asset_css) && !empty($asset_css)) {
    foreach ($asset_css as $item) {
        echo "\t".'<link rel="stylesheet" href="' . $item . '">'."\n";
    }
}?>
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo $asset.'plugins/iCheck/all.css'; ?>">
    <link rel="stylesheet" href="<?php echo $asset.'plugins/select2/select2.min.css'; ?>">
    <link rel="stylesheet" href="<?php echo $asset.'vcc-finder/style.css'; ?>">
    <link rel="stylesheet" href="<?php echo $asset.'bootstrap/css/bootstrap-datepicker.min.css'; ?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $asset.'dist/css/AdminLTE.min.css'; ?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo $asset.'dist/css/skins/_all-skins.min.css'; ?>">
    <link rel="stylesheet" href="<?php echo $asset.'css/style.css'; ?>">
    
    <!-- jQuery 2.1.4 -->
    <script src="<?php echo $asset.'plugins/jQuery/jQuery-2.1.4.min.js'; ?>"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo $asset.'plugins/jQueryUI/jquery-ui.min.js'; ?>"></script>
    <script src="<?php echo $asset.'bootstrap/js/bootstrap.min.js'; ?>"></script>
    <script src="<?php echo $asset.'bootstrap/js/bootstrap-datepicker.min.js'; ?>"></script>
    <script src="<?php echo $asset.'plugins/select2/select2.min.js'; ?>"></script>
    <script src="<?php echo $asset.'plugins/tinymce/tinymce.min.js'; ?>"></script>
    <script src="<?php echo $asset.'plugins/ckeditor/ckeditor.js'; ?>"></script>
    <script src="<?php echo $asset.'vcc-finder/script.js'; ?>"></script>
    <script type="text/javascript">
        var base_url = '<?php echo base_url('');?>';
    </script>
<?php if (isset($asset_js) && !empty($asset_js)) {
        foreach ($asset_js as $item) {
            echo "\t".'<script type="text/javascript" src="' . $item . '"></script>'."\n";
        }
    } ?>
    <script type="text/javascript" src="<?php echo $asset.'js/script.js'; ?>"></script>



<!-- End Facebook Pixel Code -->
</head>
<body class="hold-transition skin-black-light sidebar-mini">
<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo base_url('') ?>" class="logo">
            <!-- logo for regular state and mobile devices -->
            <span class="logo-mini"><img src="https://taca.edu.vn/wp-content/uploads/2017/09/Logo-Taca.png" alt="Logo"></span>
            <span class="logo-lg"><b><img src="https://taca.edu.vn/wp-content/uploads/2017/09/Logo-Taca.png"/></b></span>
        </a>
        <?php $ci =& get_instance(); ?>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <div class="connection-status visible-lg ng-scope" style="float:left">
                    <table width="100%" cellpadding="0" cellspacing="0" style="margin:15px;color:white">
                        <tbody>
                        <tr>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <ul class="nav navbar-nav">


                    <!-- User Account: style can be found in dropdown.less -->
                    <li style="padding-top:10px;" class="dropdown user user-menu">
                        <a style="display:inline" href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <span>Hi, <b>
                          <?php $user = $ci->session->userdata('user_data');
                          if (!empty($user)) {
                              echo $user['name'] ? $user['name'] : $user['username'];
                          } ?>
                      </b></span>
                        </a>/<a style="display:inline" href="<?php echo site_url('auth/logout') ?>">Thoát</a>

                    </li>

                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <style>
        .user-menu a:hover {
            background: none !important;
        }
    </style>