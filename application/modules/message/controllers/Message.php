<?php

class Message extends front_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('message/message_model');
    }

    function recive()
    {
        $info = $this->input->post();
        $arr = array(
            'user_id' => $info['sender'],
            'video_id' => $info['video'],
            'message' => $info['message_send']
        );
        $this->message_model->add($arr);
        print_r($arr);
        die;
    }

    function load($videoId)
    {
        $messages = $this->message_model->get_by_video($videoId);
        $user_data = $this->session->userdata('user_data');
        $html = '';
        foreach ($messages as $message) {
            if ($user_data['id'] == $message['user_id']) {
                $html .= '<div class="outgoing_msg"><div class="sent_msg">';
                $html .= '<p>' . $message['message'] . '</p>';
                $html .= '<span class="time_date">' . date('H:i d/m/Y', strtotime($message['created_on'])) . '</span>';
                $html .= ' </div></div></div>';
            } else {
                $html .= '<div class="incoming_msg"><div class="incoming_msg_img"><img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"></div> <div class="received_msg">';
                $html.='<div class="received_withd_msg">';
                $html .= '<p>' . $message['message'] . '</p>';
                $html .= '<span class="time_date">' . date('H:i d/m/Y', strtotime($message['created_on'])) . '</span>';
                $html .= ' </div></div></div>';
            }
        }
        echo $html;die;

    }
}