<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Message_model extends Base_model
{

    protected $table = 'messages';

    function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function get_by_video($video_id){
        $this->db->select('M.*');
        $this->db->from('messages M');
        $this->db->join('user U', 'U.id = M.user_id', 'inner');
        $this->db->where('M.video_id',$video_id);
        $this->db->order_by('created_on','asc');
        $this->db->limit(100);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();

        return $return;
    }
}