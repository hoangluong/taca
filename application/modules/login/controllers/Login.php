<?php

class Login extends front_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('course/course_model');
        $this->load->model('teacher/teacher_model');
        $user_data = $this->session->userdata('user_data');

    }

    function index()
    {
        if($this->session->userdata('user_id')) $this->redirect_back();

        $data = array();
        if($_POST){
            $data['message'] = $this->checkLogin();
        }

        $temp['template'] = 'login_view';
        $this->load->view('site_layout.php', $temp);
    }

}