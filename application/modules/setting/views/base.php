<?php if (!isset($data) || !$data) die('Không tìm thấy dữ liệu');
$data_config = json_decode(html_entity_decode($data->config_value));
$robot_args = get_meta_robot();

?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <a class="btn btn-default" href="<?php echo base_url('dashboard')?>"><i class="glyphicon glyphicon-arrow-left"></i> Dashboard</a>
            </div><!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="" method="post">
                <?php if (isset($message)) { ?>
                    <!-- alert alert-info alert-dismissible -->
                    <div style="display: block;"
                         class="alert <?php echo $message['success'] ? 'alert-info ' : 'alert-danger '; ?>alert-dismissible"
                         id="message">
                        <button type="button" id="btn-close-msg" class="close" data-dismiss="alert" aria-hidden="true">×
                        </button>
                        <p id="message-content"><?php echo $message['msg']; ?></p>
                    </div>
                <?php } ?>
                <div class="box-body">
                    <div class="form-group">
                        <label for="site_name" class="col-sm-4 control-label">Website name</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="site_name" value="<?php echo post_data('site_name', $data_config);?>" name="site_name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="site_phone" class="col-sm-4 control-label">Phone</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="site_phone" value="<?php echo post_data('site_phone', $data_config);?>" name="site_phone">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="site_email" class="col-sm-4 control-label">Email</label>
                        <div class="col-sm-8">
                            <input type="email" class="form-control" id="site_email" value="<?php echo post_data('site_email', $data_config);?>" name="site_email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="site_skype" class="col-sm-4 control-label">Skype</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="site_skype" value="<?php echo post_data('site_skype', $data_config);?>" name="site_skype">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="meta_title" class="col-sm-4 control-label">SEO: meta_title</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="meta_title" value="<?php echo post_data('meta_title', $data_config);?>" name="meta_title">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="meta_keywords" class="col-sm-4 control-label">SEO: meta_keywords</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="meta_keywords" value="<?php echo post_data('meta_keywords', $data_config);?>" name="meta_keywords">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="meta_description" class="col-sm-4 control-label">SEO: meta_description</label>
                        <div class="col-sm-8">
                            <textarea id="meta_description" class="form-control" rows="3" name="meta_description" placeholder="Nhập mô tả"><?php echo post_data('meta_description', $data_config);?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="facebook_api_key" class="col-sm-4 control-label">Facebook API Key</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="facebook_api_key" value="<?php echo post_data('facebook_api_key', $data_config);?>" name="facebook_api_key">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="facebook_app_secret" class="col-sm-4 control-label">facebook APP Secret</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="facebook_app_secret" value="<?php echo post_data('facebook_app_secret', $data_config);?>" name="facebook_app_secret">
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <input type="hidden" name="id" value="<?php echo $data ? $data->id : 0;?>">
                <div class="box-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> <?php echo $data ? 'Save' : 'Add';?></button>
                </div><!-- /.box-footer -->
            </form>
        </div><!-- /.box -->
    </div><!--/.col (left) -->
</div>   <!-- /.row -->