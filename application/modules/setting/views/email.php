<?php if (!isset($data) || !$data) die;
$data_config = json_decode(html_entity_decode($data->config_value), false);
?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <a class="btn btn-default" href="<?php echo base_url('dashboard')?>"><i class="glyphicon glyphicon-arrow-left"></i> Dashboard</a>
            </div><!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="" method="post">
                <?php if (isset($message)) { ?>
                    <!-- alert alert-info alert-dismissible -->
                    <div style="display: block;"
                         class="alert <?php echo $message['success'] ? 'alert-info ' : 'alert-danger '; ?>alert-dismissible"
                         id="message">
                        <button type="button" id="btn-close-msg" class="close" data-dismiss="alert" aria-hidden="true">×
                        </button>
                        <p id="message-content"><?php echo $message['msg']; ?></p>
                    </div>
                <?php } ?>
                <div class="box-body">
    				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
	                    <div class="form-group">
	                        <label for="protocol">Protocol:</label>
	                        <input type="text" name="protocol" id="protocol" value="<?php echo post_data('protocol', $data_config);?>" placeholder="smtp" class="form-control">
	                    </div>
	                    <div class="form-group">
	                        <label for="smtp_host">SMTP host:</label>
	                        <input type="text" name="smtp_host" id="smtp_host" value="<?php echo post_data('smtp_host', $data_config);?>" class="form-control">
	                    </div>
	                    <div class="form-group">
	                        <label for="smtp_port">SMTP port:</label>
	                        <input type="text" name="smtp_port" id="smtp_port" value="<?php echo post_data('smtp_port', $data_config);?>" class="form-control">
	                    </div>
	                    <div class="form-group">
	                        <label for="smtp_user">SMTP User:</label>
	                        <input type="text" name="smtp_user" id="smtp_user" value="<?php echo post_data('smtp_user', $data_config);?>" class="form-control">
	                    </div>
	                    <div class="form-group">
	                        <label for="smtp_pass">SMTP pass:</label>
	                        <input type="text" name="smtp_pass" id="smtp_pass" value="<?php echo post_data('smtp_pass', $data_config);?>" class="form-control">
	                    </div>
	                    <div class="form-group">
	                        <label for="mailtype">Mail type:</label>
	                        <input type="text" name="mailtype" id="mailtype" value="<?php echo post_data('mailtype', $data_config);?>" class="form-control">
	                    </div>
	                    <div class="form-group">
	                        <label for="charset">Charset:</label>
	                        <input type="text" name="charset" id="charset" value="<?php echo post_data('charset', $data_config);?>" class="form-control">
	                    </div>
	                    <div class="form-group">
	                        <label for="sender">Sender:</label>
	                        <input type="text" name="sender" id="sender" value="<?php echo post_data('sender', $data_config);?>" class="form-control">
	                    </div>
	                    <div class="form-group">
	                        <label for="recipients">Receiver email:</label>
	                        <input type="text" name="recipients" id="recipients" value="<?php echo post_data('recipients', $data_config);?>" class="form-control" placeholder="example@gmail.com">
	                    </div>
                    </div><!--/.col -->
                </div><!-- /.box-body -->
                <input type="hidden" name="id" value="<?php echo $data ? $data->id : 0;?>">
                <div class="box-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> <?php echo $data ? 'Save' : 'Add';?></button>
                </div><!-- /.box-footer -->
            </form>
        </div><!-- /.box -->
    </div><!--/.col (left) -->
</div>   <!-- /.row -->