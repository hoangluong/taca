<?php if (!isset($data) || !$data) die('Không tìm thấy dữ liệu');
$data_config = json_decode(html_entity_decode($data->config_value));
$robot_args = get_meta_robot();

?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <a class="btn btn-default" href="<?php echo base_url('dashboard')?>"><i class="glyphicon glyphicon-arrow-left"></i> Dashboard</a>
            </div><!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="" method="post">
                <?php if (isset($message)) { ?>
                    <!-- alert alert-info alert-dismissible -->
                    <div style="display: block;"
                         class="alert <?php echo $message['success'] ? 'alert-info ' : 'alert-danger '; ?>alert-dismissible"
                         id="message">
                        <button type="button" id="btn-close-msg" class="close" data-dismiss="alert" aria-hidden="true">×
                        </button>
                        <p id="message-content"><?php echo $message['msg']; ?></p>
                    </div>
                <?php } ?>
                <div class="box-body">
                    <div class="form-group">
                        <label for="form_buy_action" class="col-sm-4 control-label">Form buy action</label>
                        <div class="col-sm-8">
                            <input id="form_buy_action" value="<?php echo post_data('form_buy_action', $data_config);?>" name="form_buy_action" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email_business" class="col-sm-4 control-label">Email business</label>
                        <div class="col-sm-8">
                            <input id="email_business" value="<?php echo post_data('email_business', $data_config);?>" name="email_business" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name_1" class="col-sm-4 control-label">Package name 1</label>
                        <div class="col-sm-8">
                            <input id="name_1" value="<?php echo post_data('name_1', $data_config);?>" name="name_1" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="price_1" class="col-sm-4 control-label">Price 1</label>
                        <div class="col-sm-8">
                            <input id="price_1" value="<?php echo post_data('price_1', $data_config);?>" name="price_1" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name_2" class="col-sm-4 control-label">Package name 2</label>
                        <div class="col-sm-8">
                            <input id="name_2" value="<?php echo post_data('name_2', $data_config);?>" name="name_2" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="price_2" class="col-sm-4 control-label">Price 2</label>
                        <div class="col-sm-8">
                            <input id="price_2" value="<?php echo post_data('price_2', $data_config);?>" name="price_2" type="text" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name_3" class="col-sm-4 control-label">Package name 3</label>
                        <div class="col-sm-8">
                            <input id="name_3" value="<?php echo post_data('name_3', $data_config);?>" name="name_3" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="price_3" class="col-sm-4 control-label">Price 3</label>
                        <div class="col-sm-8">
                            <input id="price_3" value="<?php echo post_data('price_3', $data_config);?>" name="price_3" type="text" class="form-control">
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <input type="hidden" name="id" value="<?php echo $data ? $data->id : 0;?>">
                <div class="box-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> <?php echo $data ? 'Save' : 'Add';?></button>
                </div><!-- /.box-footer -->
            </form>
        </div><!-- /.box -->
    </div><!--/.col (left) -->
</div>   <!-- /.row -->