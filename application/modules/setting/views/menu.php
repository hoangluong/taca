<?php if (!isset($data) || !$data) die('Không tìm thấy dữ liệu');
$data_config = json_decode(html_entity_decode($data->config_value));
$data_items = array();
if(isset($_POST['title']) && isset($_POST['link'])){
    foreach($_POST['title'] as $index=>$title){
        $data_items['title'][$index] = isset($_POST['title'][$index]) ? $_POST['title'][$index] : '';
        $data_items['link'][$index] = isset($_POST['link'][$index]) ? $_POST['link'][$index] : '';
        $data_items['attr_id'][$index] = isset($_POST['attr_id'][$index]) ? $_POST['attr_id'][$index] : '';
    }
}else{
    if($data_config){
        foreach($data_config as $index=>$item){
            $data_items['title'][$index] = isset($item->title) ? $item->title : '';
            $data_items['link'][$index] = isset($item->link) ? $item->link : '';
            $data_items['attr_id'][$index] = isset($item->attr_id) ? $item->attr_id : '';
        }
    }
}
if(empty($data_items)){
    $data_items['title'][] = '';
    $data_items['link'][] = '';
    $data_items['attr_id'][] = '';
}
$total_items = count($data_items['title']);
?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <a class="btn btn-default" href="<?php echo base_url('dashboard')?>"><i class="glyphicon glyphicon-arrow-left"></i> Dashboard</a>
            </div><!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="" method="post">
                <?php if (isset($message)) { ?>
                    <!-- alert alert-info alert-dismissible -->
                    <div style="display: block;"
                         class="alert <?php echo $message['success'] ? 'alert-info ' : 'alert-danger '; ?>alert-dismissible"
                         id="message">
                        <button type="button" id="btn-close-msg" class="close" data-dismiss="alert" aria-hidden="true">×
                        </button>
                        <p id="message-content"><?php echo $message['msg']; ?></p>
                    </div>
                <?php } ?>
                <div class="box-body">
                    <table class="table table-bordered" id="list_menu_items">
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Tiêu đề</th>
                            <th>Link</th>
                            <th>Id</th>
                            <th style="width: 10px">-</th>
                        </tr>
                        <?php if($total_items>0){
                            for($index=0;$index<$total_items;$index++){?>
                                <tr>
                                    <td class="index"><?php echo $index+1;?>.</td>
                                    <td>
                                        <input type="text" class="form-control" value="<?php echo $data_items['title'][$index];?>" name="title[]" placeholder="Tiêu đề">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" value="<?php echo $data_items['link'][$index];?>" name="link[]" placeholder="Link">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" value="<?php echo $data_items['attr_id'][$index];?>" name="attr_id[]" placeholder="id">
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-danger btn-sm btn-remove"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                            <?php }?>
                        <?php }?>
                    </table>
                    <button type="button" class="btn btn-warning pull-right" id="add_menu_item"><i class="fa fa-plus"></i> Thêm menu</button>
                </div><!-- /.box-body -->
                <input type="hidden" name="id" value="<?php echo $data ? $data->id : 0;?>">
                <div class="box-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> <?php echo $data ? 'Save' : 'Add';?></button>
                </div><!-- /.box-footer -->
            </form>
        </div><!-- /.box -->
    </div><!--/.col (left) -->
</div>   <!-- /.row -->

<script type="text/javascript">
    var add_menu_item = function(){
        var $list_menu_items = $('#list_menu_items');
        var total_row = $list_menu_items.find('tr').length;
        $list_menu_items.append('<tr><td class="index">'+total_row+'.</td>' +
            '<td><input type="text" class="form-control" value="" name="title[]" placeholder="Tiêu đề"></td>' +
            '<td><input type="text" class="form-control" value="" name="link[]" placeholder="Link"></td>' +
            '<td><input type="text" class="form-control" value="" name="attr_id[]" placeholder="id"></td>' +
            '<td><button type="button" class="btn btn-danger btn-sm btn-remove"><i class="fa fa-trash"></i></button></td></tr>');
        event_table_menu();
    };
    var event_table_menu = function(){
        var $list_menu_items = $('#list_menu_items');
        var $btn_remove = $list_menu_items.find('.btn-remove');
        $btn_remove.unbind().click(function(){
            $(this).parent().parent().remove();
            var $index = $list_menu_items.find('td.index');
            var _index = 1;
            $index.each(function(){
                $(this).html(_index+'.');
                _index++;
            });
        });

    };
    $(document).ready(function(){
        $('#add_menu_item').click(function(){
            add_menu_item();
        });
    });
</script>