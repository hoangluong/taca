<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends Admin_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('setting_model');
        $this->config_key = 'main_menu';
    }

    public function index(){
        if($_POST){
            $temp['message'] = $this->save_menu();
        }
        $temp['data'] = $this->setting_model->find_by('config_key', $this->config_key);
        if(!$temp['data']){
            $data = array('config_key'=>$this->config_key, 'config_label'=>'Main menu', 'config_value'=>'');
            $res = $this->setting_model->add($data);
            if($res){
                $data['id'] = $res;
            }
            $temp['data'] = (object)$data;
        }

        $temp['page_title'] = 'Main menu';
        $temp['template'] = 'setting/menu';
        $this->load->view('admin/layout.php',$temp);
    }
    public function save_menu(){
        $title = $this->input->post('title', TRUE);
        $link = $this->input->post('link', TRUE);
        $attr_id = $this->input->post('attr_id', TRUE);
        $id = $this->input->post('id', true);
        $data = array();
        foreach($title as $index=>$item){
            $data[] = array('title'=>$title[$index], 'link'=>$link[$index], 'attr_id'=>$attr_id[$index]);
        }
        $this->setting_model->update($id, array(
            'config_value' => json_encode($data)
        ));
        return $this->message('Updated is successful.', true);
    }
}