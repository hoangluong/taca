<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Google_mail extends Admin_Controller{

    public function __construct(){
        parent::__construct();
        $user_data = $this->session->userdata('user_data');
        $this->load->model('setting_model');
    }

    public function index(){
        if($_POST){
            $temp['message'] = $this->save();
        }
        $temp['data'] = $this->setting_model->find_by('config_key', 'google_mail');
        if(!$temp['data']){
            $data = array('config_key'=>'google_mail', 'config_label'=>'Google mail setting', 'config_value'=>'');
            $res = $this->setting_model->add($data);
            if($res){
                $data['id'] = $res;
            }
            $temp['data'] = (object)$data;
        }

        $temp['page_title'] = 'Cài đặt gửi mail';
        $temp['template'] = 'setting/email';
        $this->load->view('admin/layout.php',$temp);
    }

    private function save(){
        $data = array();
        $request = $_POST;
        if(!isset($request['id']) && !$request['id']){
            return $this->message('Error. Not found data.');
        }
        $id = $this->input->post('id', true);
        unset($request['id']);
        if(isset($request['save'])){
            unset($request['save']);
        }
        foreach($request as $key=>$val){
            $data[$key] = $this->input->post($key, true);
        }
        $this->setting_model->update($id, array(
            'config_value' => json_encode($data)
        ));
        return $this->message('Updated is successful.', true);
    }
}