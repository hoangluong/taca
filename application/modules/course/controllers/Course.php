<?php

class Course extends front_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('course/course_model');
        $this->load->model('teacher/teacher_model');
        $this->load->model('message/message_model');
        $this->load->model('question/question_model');
    }

    function my()
    {
        $temp = array();
        $user_data = $this->session->userdata('user_data');
        if (!isset($user_data['id'])) {
            redirect(base_url('login'));
        }
        $temp['myCourses'] = $this->course_model->get_my_course($user_data['id']);
        $temp['template'] = 'my_course_view';
        $this->load->view('site_layout.php', $temp);
    }

    function index($courseId)
    {
        $temp = array();
        $modules = $this->course_model->get_all_module_by_course($courseId);
        $temp['modules'] = $modules;
        $course = $this->course_model->get_course($courseId);
        $temp['course'] = $course[0];
        $temp['template'] = 'course_index_view';
        $this->load->view('site_layout.php', $temp);
    }

    function learn($videoId)
    {
        $user_data = $this->session->userdata('user_data');
        $temp = array();
        $temp['user_id'] = $user_data['id'];
        $video = $this->course_model->get_video($videoId);
        $message = $this->message_model->get_by_video($videoId);
        $temp['messages'] = $message;
        $modules = $this->course_model->get_all_module_by_course($video[0]['course_id']);
        $temp['modules'] = $modules;
        $temp['video'] = $video;
        $question = $this->question_model->get_question_by('video',$videoId);

        $temp['questions'] = $question;
        $temp['template'] = 'learn_view';
        $this->load->view('site_layout.php', $temp);
    }

    function detail($courseId)
    {
        $temp = array();
        $modules = $this->course_model->get_all_module_by_course($courseId);
        $temp['modules'] = $modules;
        $course = $this->course_model->get_course($courseId);
        $temp['course'] = $course[0];

        $temp['template'] = 'course_detail_view';
        $this->load->view('site_layout.php', $temp);
    }

    function checkRegister()
    {
        $user_data = $this->session->userdata('user_data');

        $courseID = 4;//$this->input->post('courseId', true);
        $info = $this->course_model->chk_course_user_registered($courseID, $user_data['id']);
        print_r($info);
        die;
    }

    function setupCourse($courseID)
    {
        $result['status'] = true;
        $result['message'] = '';
        $user_data = $this->session->userdata('user_data');
        try {
            if (!isset($user_data)) {
                throw new Exception('Not Login');
            }
            $course = $this->course_model->get_course($courseID);

            if (!isset($course[0]['id'])) {
                throw new Exception('Error Course');
            }
            $allVideos = $this->course_model->getAllVideoByCourse($courseID);
            // print_r($allVideos);die;
            foreach ($allVideos as $item) {
                $arr = array(
                    'user_id' => $user_data['id'],
                    'course_id' => $courseID,
                    'module_id' => $item['module_id'],
                    'video_id' => $item['id'],
                    'passed' => 0
                );
                $this->course_model->insertLearnTracking($arr);
            }
        } catch (Exception $e) {
            $result['status'] = false;
            $result['message'] = $e->getMessage();
        }
        echo json_encode($result);
        exit();
    }

    function thi($moduleID)
    {
        $temp = array();
        $module = $this->course_model->get_module($moduleID);

        $temp['module'] = $module;
        $modules = $this->course_model->get_all_module_by_course($module[0]['course_id']);
        $temp['modules'] = $modules;
        $question = $this->course_model->getAllQuestion($moduleID);
        $temp['questions'] = $question;
        $temp['template'] = 'thi_view';
        $this->load->view('site_layout.php', $temp);
    }

    function ajax_checking_score()
    {
        $user_data = $this->session->userdata('user_data');
        $type = $this->input->post('type', true);
        $type_field_id = $type . '_id';
        $id = $this->input->post('id', true);
        $result = $this->input->post('result', true);
        $result = json_decode(stripslashes($result), true);

        $question = $this->question_model->get_question_by($type_field_id, $id);
        $dapan = array();
        foreach ($question as $item) {
            $dapan[$item['id']] = $item['answer_true'];
        }

        $point = 0;
        $total_question = count($dapan);
        foreach ($dapan as $item => $value) {
            if ($dapan[$item] == $result[$item]) {
                $point++;
            }

        }
        // Insert result
        $data = array(
            $type . '_id' => $id,
            'score' => $point,
            'total_questions' => $total_question,
            'updated_time' => date('Y-m-d H:i:s'),
            'user_id' => $user_data['id']
        );
        $this->course_model->updateScore($data, $type);
        echo json_encode(array('score' => $point, 'total' => $total_question));
        die;
        die;
    }
}