<?php
$_CI = &get_instance();
$_CI->load->model('Course_model');
?>
<div class="course-detail">
    <div class="col-md-2">
        <div class="row">
            <div class="course-navigation">
                <div class="course-navigation-item selected">
                    Tổng quan
                </div>
                <ul class="listModule">
                    <?php
                    $m = 0;
                    foreach ($modules as $item) {
                        $m++;
                        ?>
                        <li>
                            <a href="#">
                                Module <?php echo $m . ': ' . $item['title']; ?>
                            </a>
                            <ul class="sub-menu">
                                <?php
                                $videos = $_CI->course_model->get_all_video_by_module($item['id']);
                                foreach ($videos as $videoLoop) {
                                    ?>
                                    <li><a href="<?php echo base_url('course/learn/' . $videoLoop['id']) ?>"><i
                                                    class="fa fa-play-circle"></i> <?php echo $videoLoop['title']; ?>
                                        </a></li>
                                <?php } ?>

                            </ul>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-7 bdr-left" style="min-height: 500px">
        <div class="clearfix">
            <div class="col-md-2">
                <div class="back">
                    <a href="<?php echo base_url('course/index/' . $module[0]['course_id']) ?>">
                        <i class="fa fa-arrow-left"></i> Trở về</a>
                </div>
            </div>
            <div class="col-md-10">
                <h1 class="module-title">Bài thi kết thúc module <?php echo $module[0]['title']; ?></h1>
            </div>
        </div>
        <hr class="mg0">
        <div class="article-start">
            <input type="button" class="btn btn-primary" value="Bắt đầu làm bài" id="article-start">
        </div>
        <div class="thi-content" style="display: none;">
            <?php
            $init = array();
            $i=0;
            foreach ($questions as $item) {
                $i++;
                $init[$item['id']] = '0';
                ?>
                <div class="question-item">
                    <div class="content">
                        <b>Câu hỏi <?php echo $i;?>.</b> <?php echo $item['question'] ?>
                    </div>
                    <ul>
                        <li><label for="">A.</label> <?php echo $item['answer_1'] ?></li>
                        <li><label for="">B.</label> <?php echo $item['answer_2'] ?></li>
                        <li><label for="">C.</label> <?php echo $item['answer_3'] ?></li>
                        <li><label for="">D.</label> <?php echo $item['answer_4'] ?></li>
                    </ul>

                    <div class="answer-list">
                        <ul>
                            <li>
                                <div data-value="1"
                                     class="ans ans_1 q_<?php echo $item['id'];?>" data-question="<?php echo $item['id'];?>">A
                                </div>
                            </li>
                            <li>
                                <div data-value="2"
                                     class="ans ans_2 q_<?php echo $item['id'];?>" data-question="<?php echo $item['id'];?>">B
                                </div>
                            </li>
                            <li>
                                <div data-value="3"
                                     class="ans ans_3 q_<?php echo $item['id'];?>" data-question="<?php echo $item['id'];?>">C
                                </div>
                            </li>
                            <li>
                                <div data-value="4"
                                     class="ans ans_4 q_<?php echo $item['id'];?>" data-question="<?php echo $item['id'];?>">D
                                </div>
                            </li>
                        </ul>
                    </div>
                    <hr>
                </div>
            <?php } ?>
        </div>

    </div>
    <div class="col-md-3">
        <h2>Thời gian làm bài</h2>
        <hr>
        <div class="clockdiv"><?php echo '30:00'; ?></div>
        <input type="button" class="btn btn-success" id="nopbai" value="Nộp bài"
               style="width:100%;display: none;">
    </div>
</div>
<script>
    $('#article-start').click(function () {

        $('.thi-content,#nopbai').show();
        var timestart = now = new Date().getTime();
        $('#time').val(timestart);
        var fiveMinutes = <?php echo 30 * 60;?>, display = $('.clockdiv');
        startTimer(fiveMinutes, display);
        $(this).attr('disabled', 'disabled').val('Đang làm bài ...');
    })
    ;
    var res = <?php echo json_encode($init)?>;
    var module_id = <?php echo $module[0]['id'];?>;
    $('.ans').click(function () {
        var thisQuestion = $(this).attr('data-question');
        $('.q_' + thisQuestion).removeClass('active');
        $(this).addClass('active');
        res[$(this).attr('data-question')] = $(this).attr('data-value');
        console.log(res);
    });


    function startTimer(duration, display) {
        var timer = duration, minutes, seconds;

        window.s = setInterval(function () {
            minutes = parseInt(timer / 60, 10)
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            $('.clockdiv').html(minutes + ":" + seconds);

            if (--timer < 0) {
                timer = duration;

            }
            if (minutes == 0 && seconds == 0) {

                alert('end Time');
                clearInterval(window.s);
            }

        }, 1000);
    }
    $('#nopbai').click(function () {
        console.log(res);
        $('.total-result').show();
        clearInterval(window.s);
        var endstart = new Date().getTime();
        var startr = $('#time').val();
        var t = endstart - startr;
        $('#time').val(endstart - startr);
       send();
    });

    function send() {
        $.post('<?php echo base_url()?>/course/ajax_checking_score', {
            action: 'baithi',
            result: JSON.stringify(res),
            id: module_id,
            type: 'module',
            time: $('#time').val()
        }, function (data) {
            var data = JSON.parse(data);

            if (data.status == true) {
                $('.score').html(data.point + '/' + data.count);
            }


        });
    }

</script>
