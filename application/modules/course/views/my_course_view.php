<div class="col-md-2">
    <div class="row">
        <ul class="course-menu">
            <li><a href="">Khóa học của tôi</a></li>
            <li><a href="">Đang học</a></li>
            <li><a href="">Đã hoàn thành</a></li>
        </ul>
    </div>
</div>
<div class="col-md-10">
    <div class="row">
        <h1 class="block-title">My Courses</h1>
        <ul class="list-courses">
            <?php

            foreach ($myCourses as $course) {
                ?>
                <li>
                    <section class="clearfix">
                        <div class="row">
                            <div class="col-md-3">
                                <img src="https://taca.edu.vn/wp-content/uploads/2018/01/23755040_1435515686502636_2962736162233463650_n.jpg"
                                     width="100" class="circle-picker" alt="">
                            </div>
                            <div class="col-md-6">
                                <div class="cource-info">
                                    <div class="title-course"><?php echo $course['title']; ?></div>
                                    <div class="teacher">Giáo viên: <?php echo $course['teacher_name'] ?></div>
                                    <div class="date">Ngày tham gia: <?php echo date('d/m/Y',strtotime($course['created_on'])); ?></div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="flex">
                                    <a href="<?php echo base_url('course/index/' . $course['course_id'] . '-detail') ?>"
                                       class="cource-continued">Tiếp tục</a>
                                </div>
                            </div>
                        </div>
                    </section>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>