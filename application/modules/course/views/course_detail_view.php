<?php
$_CI = &get_instance();
$_CI->load->model('Course_model');
?>

<div class="course-detail">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="row">

            <div class="course-content" style="border-left: 0;">
                <h1><?php echo $course['title']; ?></h1>
                <hr>
                <div class="register-now" style="text-align: center">
                    <a class="btn btn-primary">Học thử ngay</a>
                </div>
                <br>
                <div class="video-play">
                    <iframe src="https://player.vimeo.com/video/<?php echo $course['video_try'];?>" style="width: 100%;height: 600px;" frameborder="0"
                            webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>
                <div class="course-content-body">

                    <div class="course-card">
                        <?php echo $course['content']; ?>

                    </div>

                    <div class="courseweekcards">
                        <ul>
                            <?php
                            $j = 0;

                            foreach ($modules as $item) {
                                $j++;
                                ?>
                                <li>
                                    <div class="card-detail">
                                        <h2 class="header">
                                            <span><?php echo '<b>' . $j . '</b>' . ': ' . $item['title']; ?></span>
                                        </h2>
                                        <div class="body-content">
                                            <div class="module-desc">
                                                <?php echo $item['desc']; ?>
                                            </div>
                                            <section class="module-section">
                                                <?php

                                                $videos = $_CI->course_model->get_all_video_by_module($item['id']);
                                                foreach ($videos as $video) {
                                                    ?>
                                                    <div class="chuyende clearfix">
                                                        <div class="title bdr-right"><?php echo $video['title'] ?></div>

                                                    </div>
                                                <?php } ?>
                                            </section>
                                        </div>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

