<?php
$_CI = &get_instance();
$_CI->load->model('Course_model');
?>

<div class="course-detail">
    <div class="col-md-2">
        <div class="row">
            <div class="course-navigation">
                <div class="course-navigation-item selected">
                    Tổng quan
                </div>
                <ul class="listModule">
                    <?php
                    $m = 0;
                    foreach ($modules as $item) {
                        $m++;
                        ?>
                        <li>
                            <a href="#">
                                Module <?php echo $m . ': ' . $item['title']; ?>
                            </a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-10">
        <div class="row">
            <div class="course-content">
                <div class="clearfix">
                    <div class="col-md-1">
                        <div class="back">
                            <i class="fa fa-arrow-left"></i> Trở về
                        </div>
                    </div>
                    <div class="col-md-11">
                        <h1><?php echo $course['title']; ?></h1>
                    </div>
                </div>
                <hr class="mg0">
                <div class="course-content-body">

                    <div class="course-card">
                        <?php $course['desc']; ?>

                    </div>

                    <div style="position: relative;width: 100%;margin: 50px 0" class="clearfix">
                        <ul class="progressbar">
                            <?php
                            $numModule = count($modules);
                            $widthPercent = 100 / ($numModule + 2);
                            ?>
                            <li class="first-step active" style="width: <?php echo $widthPercent . '%' ?>">Bắt đầu</li>
                            <?php

                            $k = 0;

                            foreach ($modules as $module) {
                                $k++;
                                ?>
                                <li title="<?php echo $module['title']; ?>"
                                    style="width: <?php echo $widthPercent . '%' ?>">Module <?php echo $k; ?></li>
                            <?php } ?>
                            <li class="last-step" style="width: <?php echo $widthPercent . '%' ?>">Kết thúc</li>
                        </ul>
                    </div>
                    <div class="courseweekcards">
                        <ul>
                            <li>
                                <div class="card-detail">
                                    <h2 class="header">
                                        <span>Học thử</span>
                                    </h2>
                                    <div class="body-content">
                                        <section class="module-section">
                                            <div class="chuyende clearfix">
                                                <div class="title bdr-right">Học thử</div>
                                                <div class="title  learn-chuyende"
                                                     style="width: 30%;text-align: center;">
                                                    <a href="javascript:void(0);" id="hocthu">Video</a></div>
                                            </div>

                                            <div class="video-play" id="hocthu-<?php echo $course['id']; ?>"
                                                 style="display: none;">
                                                <iframe src="https://player.vimeo.com/video/<?php echo $course['video_try']; ?>"
                                                        style="width: 100%;height: 600px;" frameborder="0"
                                                        webkitallowfullscreen mozallowfullscreen
                                                        allowfullscreen></iframe>
                                            </div>
                                            <script>
                                                $('#hocthu').click(function () {
                                                    $("#hocthu-<?php echo $course['id'];?>").toggle('slideUp');
                                                });
                                            </script>
                                        </section>
                                    </div>
                                </div>
                            </li>
                            <?php
                            $j = 0;
                            foreach ($modules as $item) {
                                $j++;
                                ?>
                                <li>
                                    <div class="card-detail">
                                        <h2 class="header">
                                            <span><?php echo '<b>' . $j . '</b>' . ': ' . $item['title']; ?></span>
                                        </h2>
                                        <div class="body-content">
                                            <section class="module-section">
                                                <?php

                                                $videos = $_CI->course_model->get_all_video_by_module($item['id']);
                                                foreach ($videos as $video) {
                                                    $chk = $_CI->course_model->videoTracking($video['id']);

                                                    ?>
                                                    <div class="chuyende clearfix" <?php echo ($chk[0]['passed'] == 0 && $chk[0]['is_continued'] == 0) ? 'style="color:#ccc;"' : ''; ?>>
                                                        <div class="title bdr-right"><?php echo $video['title'] ?></div>
                                                        <div class="title  learn-chuyende"
                                                             style="width: 30%;text-align: center;">
                                                            <?php
                                                            if ($chk[0]['passed'] == 1 || $chk[0]['is_continued'] == 1) {
                                                                ?>
                                                                <a href="<?php echo base_url('course/learn/' . $video['id']) ?>">Video</a>

                                                            <?php } else { ?>
                                                                <i class="fa fa-lock"></i>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <div class="chuyende clearfix" style="color:#ccc;">
                                                    <div class="title bdr-right">Bài thi cuối module</div>
                                                    <div class="title  learn-chuyende"
                                                         style="width: 30%;text-align: center;">
                                                        <a href="" style="color:#ccc;">Làm bài</a>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        function checkRegister() {
            $.ajax({
                url: '<?php echo base_url()?>'
            });
        }

        function chkVideo() {
            $.ajax({
                url: '<?php echo base_url()?>/'
            });
        }
    });
</script>
<style>
    .progressbar {
        counter-reset: step;
    }

    .progressbar li {
        list-style-type: none;
        width: <?php echo ($numModule > 8)?'100px':'200px';?>;
        float: left;
        font-size: 12px;
        position: relative;
        text-align: center;
        text-transform: uppercase;
        color: #7d7d7d;
    }

    .progressbar .first-step:before, .progressbar .last-step:before {
        width: 10px;
        height: 10px;
        margin: 11px auto 10px auto;
        line-height: 0;
        color: transparent;
    }

    .progressbar li:before {
        width: 30px;
        height: 30px;
        content: counter(step);
        counter-increment: step;
        line-height: 30px;
        border: 2px solid #7d7d7d;
        display: block;
        text-align: center;
        margin: 0 auto 10px auto;
        border-radius: 50%;
        background-color: white;
    }

    .progressbar li:after {
        width: 100%;
        height: 2px;
        content: '';
        position: absolute;
        background-color: #7d7d7d;
        top: 15px;
        left: -50%;
        z-index: -1;
    }

    .progressbar li:first-child:after {
        content: none;
    }

    .progressbar li.active {
        color: green;
    }

    .progressbar li.active:before {
        border-color: #55b776;
    }

    .progressbar li.active + li:after {
        background-color: #55b776;
    }

</style>
