<?php
$_CI = &get_instance();
$_CI->load->model('Course_model');
?>
<div class="course-detail">
    <div class="col-md-2">
        <div class="row">
            <div class="course-navigation">
                <div class="course-navigation-item selected">
                    Tổng quan
                </div>

                <?php
                $m = 0;
                foreach ($modules as $item) {
                    $m++;
                    ?>
                    <h2><?php echo $m . ': ' . $item['title']; ?></h2>
                    <div class="main-timeline">
                        <?php
                        $videos = $_CI->course_model->get_all_video_by_module($item['id']);
                        $i = 0;
                        foreach ($videos as $videoLoop) {
                            $i++;
                            ?>
                            <div class="timeline">
                                <a href="" class="timeline-content">
                                    <div class="title">chuyên đề <?php echo $i; ?></div>
                                    <p class="description"><?php echo $videoLoop['title']; ?></p>
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>


                <!--<ul class="listModule">
                    <?php
                $m = 0;
                foreach ($modules as $item) {
                    $m++;
                    ?>
                        <li>
                            <a href="#">
                                Module <?php echo $m . ': ' . $item['title']; ?>
                            </a>
                            <ul class="sub-menu">
                                <?php
                    $videos = $_CI->course_model->get_all_video_by_module($item['id']);
                    foreach ($videos as $videoLoop) {
                        ?>
                                    <li><a href="<?php echo base_url('course/learn/' . $videoLoop['id']) ?>"><i
                                                    class="fa fa-play-circle"></i> <?php echo $videoLoop['title']; ?>
                                        </a></li>
                                <?php } ?>

                            </ul>
                        </li>
                    <?php } ?>
                </ul> -->

            </div>
        </div>
    </div>
    <div class="col-md-7 bdr-left">
        <div class="clearfix">
            <div class="col-md-2">
                <div class="back">
                    <a href="<?php echo base_url('course/index/' . $video[0]['course_id']) ?>">
                        <i class="fa fa-arrow-left"></i> Trở về</a>
                </div>
            </div>
            <div class="col-md-10">
                <h1 class="module-title"><?php echo $video[0]['title']; ?></h1>
            </div>
        </div>
        <hr>
        <div class="video-play">
            <iframe src="https://player.vimeo.com/video/<?php echo $video[0]['video']; ?>"
                    style="width: 100%;height: 600px;" frameborder="0"
                    webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
        <div class="video-content">
            <?php echo $video[0]['desc']; ?>
            <div class="quiz-video">
                <h3>Kiểm tra</h3>
                <div class="thi-content"  >
                    <?php
                    $init = array();
                    $i=0;
                    foreach ($questions as $item) {
                        $i++;
                        $init[$item['id']] = '0';
                        ?>
                        <div class="question-item">
                            <div class="content">
                                <b>Câu hỏi <?php echo $i;?>.</b> <?php echo $item['question'] ?>
                            </div>
                            <ul>
                                <li><label for="">A.</label> <?php echo $item['answer_1'] ?></li>
                                <li><label for="">B.</label> <?php echo $item['answer_2'] ?></li>
                                <li><label for="">C.</label> <?php echo $item['answer_3'] ?></li>
                                <li><label for="">D.</label> <?php echo $item['answer_4'] ?></li>
                            </ul>

                            <div class="answer-list">
                                <ul>
                                    <li>
                                        <div data-value="1"
                                             class="ans ans_1 q_<?php echo $item['id'];?>" data-question="<?php echo $item['id'];?>">A
                                        </div>
                                    </li>
                                    <li>
                                        <div data-value="2"
                                             class="ans ans_2 q_<?php echo $item['id'];?>" data-question="<?php echo $item['id'];?>">B
                                        </div>
                                    </li>
                                    <li>
                                        <div data-value="3"
                                             class="ans ans_3 q_<?php echo $item['id'];?>" data-question="<?php echo $item['id'];?>">C
                                        </div>
                                    </li>
                                    <li>
                                        <div data-value="4"
                                             class="ans ans_4 q_<?php echo $item['id'];?>" data-question="<?php echo $item['id'];?>">D
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <hr>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>

    </div>
    <div class="col-md-3">
        <div class="messaging clearfix">
            <div class="inbox_msg">

                <div class="mesgs">
                    <div class="msg_history">
                        <?php
                        foreach ($messages as $message) {
                            if ($user_id == $message['user_id']) {
                                ?>
                                <div class="outgoing_msg">

                                    <div class="sent_msg">

                                        <p><?php echo $message['message']; ?></p>
                                        <span class="time_date"><?php echo date('H:i d/m/Y', strtotime($message['created_on'])) ?></span>

                                    </div>
                                </div>
                                <?php
                            } else {
                                ?>
                                <div class="incoming_msg">
                                    <div class="incoming_msg_img"><img
                                                src="https://ptetutorials.com/images/user-profile.png"
                                                alt="sunil"></div>
                                    <div class="received_msg">
                                        <div class="received_withd_msg">
                                            <p><?php echo $message['message']; ?></p>
                                            <span class="time_date"><?php echo date('H:i d/m/Y', strtotime($message['created_on'])) ?></span>
                                        </div>
                                    </div>
                                </div>

                                <?php
                            }
                        }
                        ?>

                    </div>
                    <div class="type_msg">
                        <form action="" id="messageForm">
                            <div class="input_msg_write">
                                <input type="text" name="message_send" class="write_msg" placeholder="Type a message"/>
                                <input type="hidden" name="sender" value="<?php echo $user_id ?>">
                                <input type="hidden" name="video" value="<?php echo $video[0]['id'] ?>">
                                <button class="msg_send_btn" type="submit"><i class="fa fa-paper-plane-o"
                                                                              aria-hidden="true"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
<script>
    $(document).ready(function () {

        // process the form
        $('form#messageForm').submit(function (event) {

            // get the form data
            // there are many ways to get this data using jQuery (you can use the class or id also)
            var formData = {
                'message_send': $('input[name=message_send]').val(),
                'sender': $('input[name=sender]').val(),
                'video': $('input[name=video]').val(),

            };

            // process the form
            $.ajax({
                type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                url: '<?php echo base_url('message/recive')?>', // the url where we want to POST
                data: formData, // our data object
                encode: true
            })
                .done(function (data) {
                    $.get('<?php echo base_url('message/load/' . $video[0]['id']);?>', function (data2) {
                        $('.msg_history').html(data2);
                    });
                });

            // stop the form from submitting the normal way and refreshing the page
            event.preventDefault();
        });

    });

</script>
<style>

    img {
        max-width: 100%;
    }

    .recent_heading h4 {
        color: #05728f;
        font-size: 21px;
        margin: auto;
    }

    .srch_bar input {
        border: 1px solid #cdcdcd;
        border-width: 0 0 1px 0;
        width: 80%;
        padding: 2px 0 4px 6px;
        background: none;
    }

    .srch_bar .input-group-addon button {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
        border: medium none;
        padding: 0;
        color: #707070;
        font-size: 18px;
    }

    .srch_bar .input-group-addon {
        margin: 0 0 0 -27px;
    }

    .chat_ib h5 {
        font-size: 15px;
        color: #464646;
        margin: 0 0 8px 0;
    }

    .chat_ib h5 span {
        font-size: 13px;
        float: right;
    }

    .chat_ib p {
        font-size: 14px;
        color: #989898;
        margin: auto
    }

    .chat_img {
        float: left;
        width: 11%;
    }

    .chat_ib {
        float: left;
        padding: 0 0 0 15px;
        width: 88%;
    }

    .chat_people {
        overflow: hidden;
        clear: both;
    }

    .chat_list {
        border-bottom: 1px solid #c4c4c4;
        margin: 0;
        padding: 18px 16px 10px;
    }

    .inbox_chat {
        height: 550px;
        overflow-y: scroll;
    }

    .active_chat {
        background: #ebebeb;
    }

    .incoming_msg_img {
        display: inline-block;
        width: 6%;
    }

    .received_msg {
        display: inline-block;
        padding: 0 0 0 10px;
        vertical-align: top;
        width: 92%;
    }

    .received_withd_msg p {
        background: #ebebeb none repeat scroll 0 0;
        border-radius: 3px;
        color: #646464;
        font-size: 14px;
        margin: 0;
        padding: 5px 10px 5px 12px;
        width: 100%;
    }

    .time_date {
        color: #747474;
        display: block;
        font-size: 12px;
        margin: 8px 0 0;
    }

    .received_withd_msg {
        width: 57%;
    }

    .mesgs {
        float: left;
        padding: 30px 15px 0 25px;
        width: 100%;
    }

    .sent_msg p {
        background: #05728f none repeat scroll 0 0;
        border-radius: 3px;
        font-size: 14px;
        margin: 0;
        color: #fff;
        padding: 5px 10px 5px 12px;
        width: 100%;
    }

    .outgoing_msg {
        overflow: hidden;
        margin: 26px 0 26px;
    }

    .sent_msg {
        float: right;
        width: 46%;
    }

    .input_msg_write input {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
        border: medium none;
        color: #4c4c4c;
        font-size: 15px;
        min-height: 48px;
        width: 100%;
    }

    .type_msg {
        border-top: 1px solid #c4c4c4;
        position: relative;
    }

    .msg_send_btn {
        background: #05728f none repeat scroll 0 0;
        border: medium none;
        border-radius: 50%;
        color: #fff;
        cursor: pointer;
        font-size: 17px;
        height: 33px;
        position: absolute;
        right: 0;
        top: 11px;
        width: 33px;
    }

    .messaging {
        padding: 0 0 50px 0;
        border-left: 1px #ccc solid;
    }

    .msg_history {
        height: 516px;
        overflow-y: auto;
    }

    /* -------------------------------- */
</style>