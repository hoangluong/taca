<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Course_model extends Base_model
{

    protected $table = 'courses';

    function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function module_save($data, $id = null)
    {
        if ($id && $id > 0) {
            $this->db->update('course_modules', $data)->where('id', $id);
            return $id;
        } else {
            $this->db->insert('course_modules', $data);
            return $this->db->insert_id();
        }
    }

    function chuyende_save($data, $id = null)
    {
        if ($id && $id > 0) {
            $this->db->update('module_videos', $data)->where('id', $id);
            return $id;
        } else {
            $this->db->insert('module_videos', $data);
            return $this->db->insert_id();
        }
    }


    function getAllCourses()
    {
        $this->db->select('courses.*, teachers.name as teacher_name');
        $this->db->from('courses');
        $this->db->join('teachers', 'teachers.id = courses.teacher_id', 'inner');
        $this->db->limit(100);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();

        return $return;
    }

    function get_course($courseId)
    {
        $this->db->select('C.*, T.name as teacher_name, C.title as course_title');
        $this->db->from('courses C');
        $this->db->join('teachers T', 'T.id = C.teacher_id', 'inner');
        $this->db->where('C.id', $courseId);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }

    function get_all_module_by_course($course_id)
    {
        $this->db->select('*')
            ->from('course_modules')
            ->where('course_id', $course_id);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();

        return $return;
    }

    function get_module($moduleId)
    {
        $this->db->select('M.*, C.title as course_title');
        $this->db->from('course_modules M');
        $this->db->join('courses C', 'C.id = M.course_id', 'inner');
        $this->db->where('M.id', $moduleId);
        $this->db->limit(100);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();

        return $return;
    }

    function get_all_video_by_module($moduleId)
    {
        $this->db->select('*')
            ->from('module_videos')
            ->where('module_id', $moduleId);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();

        return $return;
    }

    function get_my_course($user_id)
    {
        $this->db->select('*, C.title as title, T.name as teacher_name')
            ->from('classes MC')
            ->join('courses C', 'C.id = MC.course_id', 'inner')
            ->join('teachers T', 'T.id = C.teacher_id', 'inner')
            ->where('user_id', $user_id);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();

        return $return;
    }

    function get_video($video_id)
    {
        $this->db->select('V.*,M.id as module_id, M.title as module_title, C.id as course_id, C.title as course_title')
            ->from('module_videos V')
            ->join('course_modules M', 'M.id = V.module_id', 'inner')
            ->join('courses C', 'C.id = M.course_id', 'inner')
            ->where('V.id', $video_id);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();

        return $return;
    }

    function get_course_by_manager()
    {
        $user_data = $this->session->userdata('user_data');

        $this->db->select('*')
            ->from('courses')
            ->where('manager_id', $user_data['id']);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }

    function get_class_member($courseId)
    {
        $user_data = $this->session->userdata('user_data');

        $this->db->select('CL.*, U.name as user_name, U.phone as user_phone, U.email as user_email')
            ->from('classes CL')
            ->join('user U', 'U.id = CL.user_id', 'inner')
            ->join('courses C', 'C.id = CL.course_id', 'inner')
            ->where('CL.course_id', $courseId);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }

    function chk_course_user_registered($courseId, $userId)
    {
        $this->db->select('*')
            ->from('classes')
            ->where('course_id', $courseId)
            ->where('user_id', $userId);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }

    function insertLearnTracking($data)
    {
        $this->db->insert('course_trackings', $data);
        return $this->db->insert_id();
    }

    function getAllVideoByCourse($courseId)
    {
        $this->db->select('MV.*')
            ->from('module_videos MV')
            ->join('course_modules CM', 'CM.id = MV.module_id', 'inner')
            ->join('courses C', 'C.id = CM.course_id', 'inner')
            ->where('C.id', $courseId);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }

    function videoTracking($videoID)
    {
        $this->db->select('*')
            ->from('course_trackings')
            ->where('video_id', $videoID);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }

    function getAllQuestion($moduleId)
    {
        $this->db->select('*')
            ->from('questions')
            ->where('module_id', $moduleId);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }

    function updateScore($data, $type, $id = null)
    {
        $user_data = $this->session->userdata('user_data');
        if (isset($user_data)) {
            if ($id) {
                $this->db->update('scores', $data)->where($type . '_id', $id);
            }else{
                $this->db->insert('scores',$data);
            }
        }
    }
}