<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Tool extends Admin_Controller{

    /**
    * chuyển dữ liệu từ product.properties -> product.specifics
    * { propery_title: '', 'propery_des': ''} => {"title":"","value":""}
    */
    public function specifics($page=1){
        $this->load->model('product/product_model');
        $data = $this->product_model->find(array('properties <>'=>''), $page, $this->per_page, array('id'=>'desc'), array('id,properties'));
        $total_update = 0;
        if($data){
            foreach($data as $item){
                if(!$item->properties){
                    continue;
                }
                $total_update++;
                $properties = json_decode($item->properties);
                $specifics = array();
                foreach ($properties as $prop) {
                    $specifics[] = array(
                        'title' => $prop->propery_title,
                        'value' => $prop->propery_des
                    );
                }
                $specifics = json_encode($specifics);
                $this->product_model->update($item->id, array('specifics'=>$specifics));
            }
        }
        echo 'total_update: '. $total_update;
        reidrect('dashboard/tool/specifics/'.($page+1));
    }
}