<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Admin_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('auth/user_model');
        $user_data = $this->session->userdata('user_data');
        if($user_data['role']!=1 && $user_data['role']!=3) die('Do not permission');
    }

    public function index(){

        $s = stripslashes($this->input->get('s', true));
        $page = stripslashes($this->input->get('page', true));
        $page = $page ? $page : 1;
        $cond = array();
        if($s){
            $cond['s'] = $this->input->get('s', true);
        }
        //check role
        $user_data = $this->session->userdata('user_data');
        $user_data = $this->user_model->get( $user_data['id'] );
        if($user_data->role==3){
            $cond['manager'] = $user_data->id;
        }

        $contacts = $this->user_model->find($cond, $page, $this->per_page, array('id'=>'desc'));
        $total = $this->user_model->total($cond);
        $temp['total_page'] = $total%$this->per_page==0 ? $total/$this->per_page : intval($total/$this->per_page)+1;

        $temp['data'] = $contacts;

        if($user_data->role==3){
            $temp['page_title'] = 'Account system (Total: '.$total.')';
        }else{
            $temp['page_title'] = 'Account management (Total: '.$total.')';
        }
        $temp['template'] = 'user/list';
        $this->load->view('admin/layout.php',$temp);
    }
    function add(){
        $temp['page_title'] = 'Add member';

        if($_POST){
            $message = $this->save_user();
            $temp['message'] = $message;
        }
        $temp['managers'] = $this->user_model->find_all_by('role', 3, array('id', 'username','email','name'));
        $temp['template'] = 'user/form';
        $this->load->view('admin/layout.php',$temp);
    }
    function edit($id){
        if(!$id || !is_numeric($id) || $id<0) die('Link is not exists.');
        $temp['page_title'] = 'Edit member information';
        if($_POST){
            $message = $this->save_user();
            $temp['message'] = $message;
        }
        $temp['managers'] = $this->user_model->find_all_by('role', 3, array('id', 'username','email','name'));
        $temp['data'] = $this->user_model->get($id);
        if(!$temp['data']) die('Account is not exists.');
        $temp['template'] = 'user/form';
        $this->load->view('admin/layout.php',$temp);
    }
    private function save_user(){
        $id = $this->input->post('id', true);
        $email = $this->input->post('email', true);
        $phone = $this->input->post('phone', true);
        $username = $this->input->post('username', true);
        $password = $this->input->post('password', true);
        $re_password = $this->input->post('re_password', true);

        if ($email && !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return $this->message('Email is not valid');
        }
        $check_email_exists = $this->user_model->find_by('email', $email);
        if($check_email_exists && $check_email_exists->id!=$id){
            return $this->message('Email is used');
        }
        if($phone && !preg_match( '/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i', $phone )){
            return $this->message('Phone number is not valid');
        }
        $request = $this->input->post(null, true);
        $data = array();
        //check role
        $user_data = $this->session->userdata('user_data');
        if($user_data['role']==3){
            $data = array(
                'name' => $this->input->post('name', true),
                'email' => $this->input->post('email', true),
                'phone' => $this->input->post('phone', true),
                'store' => $this->input->post('store', true),
                'country' => $this->input->post('country', true)
            );
        }else{
            if($request){
                foreach($request as $field=>$val){
                    if($val=='') continue;
                    $data[$field] = $this->input->post($field, true);
                }
            }
        }
        $user_id = $this->session->userdata('user_id');
        if($user_id==$id){
            if(isset($data['role'])){
                unset($data['role']);
            }
            if(isset($data['status'])){
                unset($data['status']);
            }
        }
        $token_name = $this->security->get_csrf_token_name();
        if(isset($data[$token_name])){
            unset($data[$token_name]);
        }

        if(!$id){
            if($username){
                $username_special = url_friendly($username, '_');
                if($username_special!==$username) return $this->message('Username can not have special characters');
                $check_username = $this->user_model->find_by('username', $username);
                if($check_username) return $this->message('Username is used');
                if(!$password) return $this->message('Enter password please');
                if(!$re_password || $password!==$re_password) return $this->message('Retype password is incorrect');
                $data = $this->pre_save($data);
            }

            if(isset($data['msg'])) return $data;
            $res = $this->user_model->add($data);
            if(!$res) return $this->message('Error. Please try again');
            return $this->message('Added is successfull.', true);
        }else{
            if($password){
                if($re_password!=$password){
                    return $this->message('Retype password is incorrect');
                }
            }
            $data = $this->pre_save($data);
            if(isset($data['msg'])) return $data;
            $res = $this->user_model->update($id, $data);
            if(!$res) return $this->message('Error. Please try again');
            return $this->message('Saved is successful.', true);
        }
    }
    private function pre_save($data){
        if(isset($data['password'])){
            $data['password'] = md5($data['password']);
        }
        if(isset($data['re_password'])){
            unset($data['re_password']);
        }
        if(isset($data['id']) && !$data['id']){
            unset($data['id']);
        }
        return $data;
    }

    public function del($id){
        $confirm = $this->input->get('confirm', true);
        if(!$confirm){
            $data = array(
                'redirect' => base_url('user'),
                'del' => base_url('user/del/'.$id.'?confirm=1')
            );
            echo '<script>'.
                'var r = confirm("Are you sure delete this data.");'.
                'if(r==true){'.
                'window.location.href = "'.$data['del'].'";'.
                '}else{'.
                'window.location.href = "'.$data['redirect'].'";}</script>';
            exit();
        }else{
            if(is_numeric($id)===true || $id>0){
                $this->user_model->delete($id);
            }
            redirect('user');
        }
    }

    /* api search user*/
    public function search(){
        $s = $this->input->get('s');
        $manager = $this->input->get('m');

        $cond = array();
        if($s){
            $cond['s'] = $this->input->get('s', true);
        }
        if($manager){
            $cond['manager'] = $manager;
        }
        $per_page = 5;
        $users = $this->user_model->find($cond, 1, $per_page, array('id'=>'desc'));
        echo json_encode($users);exit();
    }
}
