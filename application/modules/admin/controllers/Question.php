<?php

class Question extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('course/course_model');
        $this->load->model('question/question_model');
        $user_data = $this->session->userdata('user_data');
        if ($user_data['role'] != 1 && $user_data['role'] != 3) die('Do not permission');
    }

    public function index($type, $id)
    {
        $temp['status'] = true;
         
        try {
            switch ($type) {

                case
                'module':
                    {
                        $info = $this->course_model->get_module($id);
                        $type_field_id = 'module_id';
                    }
                    break;
                case
                'video':
                    {
                        $info = $this->course_model->get_video($id);
                        $type_field_id = 'video_id';
                    }break;
                case
                'course':
                    {
                        $info = $this->course_model->get_course($id);
                        $type_field_id = 'course_id';
                    }break;
            }

            if (!isset($info[0]['id'])) {
                throw new Exception('Module not found!');
            }
            $question = $this->question_model->get_question_by($type, $id);
            $temp['info'] = $info[0];
            $temp['type_field_id'] = $type_field_id;
            $temp['questions'] = $question;

        } catch (Exception $e) {
            $temp['status'] = false;
            $temp['message'] = $e->getMessage();
        }

        $temp['page_title'] = 'Trắc nghiệm '.$type.': '.$info[0]['title'];
        $temp['template'] = 'question/module_index_view';
        $this->load->view('admin/layout.php', $temp);
    }

    public function video($videoId)
    {
        try {

        } catch (Exception $e) {
            $result['status'] = false;
            $result['message'] = $e->getMessage();
        }
    }

    public function ajax_load_question_form()
    {
        $module_id = $this->input->get('module_id', true);
        $module_id = (isset($module_id)) ? $module_id : 0;
        $video_id = $this->input->get('video_id', true);
        $video_id = (isset($video_id)) ? $video_id : 0;
        $course_id = $this->input->get('course_id', true);
        $course_id = (isset($course_id)) ? $course_id : 0;
        $temp['module_id'] = $module_id;
        $temp['course_id'] = $course_id;
        $temp['video_id'] = $video_id;
        $temp['template'] = 'question/ajax_question_form_view';
        echo $this->load->view('null_layout.php', $temp, true);
        die;
    }

    public function ajax_update_question()
    {
        try {
            $id = $this->input->post('id', true);
            $id = isset($id) ? $id : null;
            $module_id = $this->input->post('module_id', true);
            $module_id = (isset($module_id)) ? $module_id : 0;
            $video_id = $this->input->post('video_id', true);
            $video_id = (isset($video_id)) ? $video_id : 0;
            $course_id = $this->input->post('course_id', true);
            $course_id = (isset($course_id)) ? $course_id : 0;
            $question = $this->input->post('question', true);
            $answer_1 = $this->input->post('answer_1', true);
            $answer_2 = $this->input->post('answer_2', true);
            $answer_3 = $this->input->post('answer_3', true);
            $answer_4 = $this->input->post('answer_4', true);
            $answer_true = $this->input->post('answer_true', true);
            $arr = array(
                'question' => $question,
                'answer_1' => $answer_1,
                'answer_2' => $answer_2,
                'answer_3' => $answer_3,
                'answer_4' => $answer_4,
                'answer_true' => $answer_true,
                'module_id' => $module_id,
                'video_id' => $video_id,
                'course_id' => $course_id,
            );

            $this->question_model->save($arr, $id);
        } catch (Exception $e) {
            $result['status'] = false;
            $result['message'] = $e->getMessage();
        }
    }

    public function delete()
    {
        $result['status'] = true;
        $result['message'] = 'init';
        $id = $this->input->get('id', true);
        try {
            if (!isset($id)) {
                throw new Exception('Question not found!');
            }

            $this->db->where('id', $id)->delete('questions');
            $result['status'] = true;
            $result['message'] = 'Xóa thành công!';
        } catch (Exception $e) {
            $result['status'] = false;
            $result['message'] = $e->getMessage();
        }
        echo json_encode($result);
        die;
    }

}