<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Classes extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('course/course_model');
        $this->load->model('teacher/teacher_model');
        $user_data = $this->session->userdata('user_data');
        if ($user_data['role'] != 1 && $user_data['role'] != 3) die('Do not permission');
    }

    function index()
    {
        $courses = $this->course_model->get_course_by_manager();
        $classmates = $this->course_model->get_class_member($courses[0]['id']);
        $temp['classmates'] = $classmates;
        $temp['course'] = $courses;
        $temp['page_title'] = 'Danh sách lớp';
        $temp['template'] = 'classes/index_view';
        $this->load->view('admin/layout.php', $temp);
    }
}