<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard extends Admin_Controller{

    public function index(){
        $temp = array();
        $temp['page_title'] = 'Dashboard';
        $temp['template'] = 'dashboard/dashboard';
        $this->load->view('admin/layout.php',$temp);
    }
    public function slug(){
        $title = $this->input->post('title', true);
        echo url_friendly($title, '-');exit();
    }

    public function remove_special_character(){
        $string = $this->input->post('str');
        echo preg_replace("/[^a-zA-Z0-9\ ]/", "", $string);exit();
    }
}