<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Course extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('course/course_model');
        $this->load->model('teacher/teacher_model');
        $user_data = $this->session->userdata('user_data');
        if ($user_data['role'] != 1 && $user_data['role'] != 3) die('Do not permission');
    }

    function index()
    {
        $courses = $this->course_model->getAllCourses();
        $temp['course'] = $courses;
        $temp['page_title'] = 'Courses';
        $temp['template'] = 'course/list';
        $this->load->view('admin/layout.php', $temp);
    }

    function create()
    {
        if ($_POST) {
            $info = $this->input->post();
            $arr = array(
                'title' => $this->input->post('title', true),
                'desc' => $this->input->post('desc', true),
                'teacher_id' => $this->input->post('teacher', true),
                'status' => 1
            );
            $this->course_model->add($arr);
            redirect(base_url() . '/admin/course');
        }
        $teachers = $this->teacher_model->find(array(), 1, 1000);

        $temp['teachers'] = $teachers;

        $temp['page_title'] = 'Courses';
        $temp['template'] = 'course/create';
        $this->load->view('admin/layout.php', $temp);
    }

    function module($courseId)
    {
        $temp['course'] = $this->course_model->get_course($courseId);
        $temp['modules'] = $this->course_model->get_all_module_by_course($courseId);

        $temp['page_title'] = 'Courses module';
        $temp['template'] = 'course/module';
        $this->load->view('admin/layout.php', $temp);
    }

    function video($module)
    {

        try {
            $temp['module'] = $this->course_model->get_module($module);
            $temp['videos'] = $this->course_model->get_all_video_by_module($module);
        } catch (Exception $e) {
            $result['status'] = false;
            $result['message'] = $e->getMessage();
        }
        $temp['page_title'] = 'Chuyên đề';
        $temp['template'] = 'course/video';
        $this->load->view('admin/layout.php', $temp);
    }

    function ajax_save_module()
    {
        try {
            if (empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
                throw new Exception('Not Ajax');
            }
            $info = $this->input->post();
            $arr = array(
                'course_id' => $info['course_id'],
                'title' => $info['title'],
                'desc' => $info['desc']
            );
            $res = $this->course_model->module_save($arr);
            $result['status'] = true;
            $result['message'] = 'Add successfully';
        } catch (Exception $e) {
            $result['status'] = false;
            $result['message'] = $e->getMessage();
        }
        echo json_encode($result);
        exit();

    }

    function ajax_save_chuyende()
    {
        try {
            if (empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
                throw new Exception('Not Ajax');
            }
            $info = $this->input->post();
            $arr = array(
                'module_id' => $info['module_id'],
                'title' => $info['title'],
                'desc' => $info['content'],
                'video' => $info['video']
            );
            $res = $this->course_model->chuyende_save($arr);
            $result['status'] = true;
            $result['message'] = 'Add successfully';
        } catch (Exception $e) {
            $result['status'] = false;
            $result['message'] = $e->getMessage();
        }
        echo json_encode($result);
        exit();

    }

    function tree($courseId)
    {
        try {
            $result = array();
            $course = $this->course_model->get_course($courseId);
            $temp['course'] = $course;
            $modules = $this->course_model->get_all_module_by_course($courseId);
            $temp['modules'] = $modules;
            $temp['template'] = 'course/tree_view';
            $this->load->view('null_layout.php', $temp);
        } catch (Exception $e) {
            $result['status'] = false;
            $result['message'] = $e->getMessage();
        }

    }

    function init_course($courseId){
        $user_data = $this->session->userdata('user_data');

    }

}