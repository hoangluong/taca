<?php
$user_data = $this->session->userdata('user_data');
$user_id = $user_data['id'];
$data = isset($data) ? $data : null;
$name = post_data('name', $data, '');
$email = post_data('email', $data, '');
$phone = post_data('phone', $data, '');
$type = post_data('type', $data, '');
$username = post_data('username', $data, '');
$role = post_data('role', $data, 0);
$status = post_data('status', $data, 0);
$manager = post_data('manager', $data, 0);
?>
<form action="" method="post" enctype="multipart/form-data">
    <div class="box box-info">
        <div class="box-header">
            <a class="btn btn-info" href="<?php echo base_url('user')?>"><i class="glyphicon glyphicon-arrow-left"></i> Back</a>
        </div>
        <div class="box-body">
            <?php if (isset($message) && $message) { ?>
                <!-- alert alert-info alert-dismissible -->
                <div style="display: block;"
                     class="alert <?php echo $message['success'] ? 'alert-info ' : 'alert-danger '; ?>alert-dismissible"
                     id="message">
                    <button type="button" id="btn-close-msg" class="close" data-dismiss="alert"
                            aria-hidden="true">×
                    </button>
                    <p id="message-content"><?php echo $message['msg']; ?></p>
                </div>
            <?php } ?>

            <div class="">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="username">Username:</label>
                        <input id="username" class="form-control"<?php if($data){echo ' readonly';}?> value="<?php echo $username;?>" name="username">
                    </div>
                    <div class="form-group">
                        <label for="password">Password:</label>
                        <input type="password" name="password" id="password" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="re_password">Password retype:</label>
                        <input type="password" name="re_password" id="re_password" class="form-control">
                    </div>
                    <?php if( (!$data || $user_id!==$data->id) && $user_data['role']==1){?>
                    <div class="row form-group">
                        <label class="col-sm-12" for="re_password">Role:</label>
                        <label class="col-sm-12 col-md-4">
                            <input type="radio" name="role" class="flat-red" value="2"<?php if($role==2){echo ' checked';}?>> Member
                        </label>
                        <label class="col-sm-12 col-md-4">
                            <input type="radio" name="role" class="flat-red" value="3"<?php if($role==3){echo ' checked';}?>> Manager
                        </label>
                        <label class="col-sm-12 col-md-4">
                            <input type="radio" name="role" class="flat-red" value="1"<?php if($role==1){echo ' checked';}?>> Administrator
                        </label>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-12" for="status">Status:</label>
                        <label class="col-sm-12">
                            <input type="checkbox" value="1" name="status" id="status" class="flat-red"<?php if($status==1){echo ' checked';}?>> Activate
                        </label>
                    </div>
                    <div class="form-group">
                        <label for="manager">Manager</label>
                        <select name="manager" id="manager" class="form-control select2" data-placeholder="Select a manager">
                            <option value=""></option>
                            <option value="0">No manager</option>
                            <?php 
                            foreach($managers as $item){?>
                            <option <?php if($manager==$item->id){echo 'selected ';}?>value="<?php echo $item->id;?>"><?php echo $item->email;?></option>
                            <?php }?>
                        </select>
                    </div>
                    <?php }?>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="name">Fullname:</label>
                        <input type="text" name="name" id="name" value="<?php echo $name; ?>"
                               class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input type="text" name="email" id="email" value="<?php echo $email; ?>"
                               class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone:</label>
                        <input type="text" name="phone" id="phone" value="<?php echo $phone; ?>"
                               class="form-control">
                    </div>
                    <?php if($user_data['role']==1){?>
                    <div class="form-group">
                        <label for="expired">Expired date:</label>
                        <input type="text" name="expired" id="expired" value="<?php echo post_data('expired', $data); ?>"
                               class="form-control">
                    </div>
                    <?php }?>
                    <div class="form-group">
                        <label for="store">Store name:</label>
                        <input type="text" name="store" id="store" value="<?php echo post_data('store', $data, ''); ?>"
                               class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="country">Country</label>
                        <select name="country" id="country" class="form-control select2" placeholder="Select a country">
                            <option value=""></option>
                            <?php $countries = get_countries();
                            $country = post_data('country', $data);
                            foreach($countries as $country_key=>$country_name){?>
                            <option <?php if($country_key==$country){echo 'selected ';}?>value="<?php echo $country_key;?>"><?php echo $country_name;?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <?php if($data){?>
            <button type="submit" class="btn btn-info">Save</button>
            <?php }else{?>
            <button type="submit" class="btn btn-info">Add</button>
            <?php }?>
        </div>
    </div>
    <?php if($data){?>
        <input type="hidden" name="id" value="<?php echo $data->id;?>">
    <?php }?>
    <?php $csrf = array(
        'name' => $this->security->get_csrf_token_name(),
        'hash' => $this->security->get_csrf_hash()
    );?>
    <input type="hidden" name="<?php echo $csrf['name'];?>" value="<?php echo $csrf['hash'];?>" />
</form>