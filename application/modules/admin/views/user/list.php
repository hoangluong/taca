<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <a class="btn btn-success" href="<?php echo base_url('user/add');?>"><i class="fa fa-plus"></i>&nbsp;Add</a>

                <div class="box-tools pull-right">
                    <form action="" method="get" class="primary">
                        <div class="input-group" style="width: 250px;">
                            <input type="text" name="s" value="<?php echo htmlentities($this->input->get('s', true));?>" class="form-control input-md pull-right" placeholder="Tìm kiếm">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-md btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table class="table">
                    <tbody>
                    <tr>
                        <th style="width: 20px">ID</th>
                        <th>Họ tên</th>
                        <th>Số điện thoại</th>
                        <th>Email</th>
                        <th>Tên đăng nhập</th>
                        <th>Quyền hạn</th>
                        <th style="width: 150px">Quản lý</th>
                    </tr>
                    <?php if(isset($data) && !empty($data)){
                        foreach($data as $item){?>
                    <tr>
                        <td><?php echo $item->id;?></td>
                        <td><?php echo $item->name;?></td>
                        <td><?php echo $item->phone;?></td>
                        <td><?php echo $item->email;?></td>
                        <td><?php echo $item->username;?></td>
                        <td><?php if($item->role==1){echo 'Administrator';}elseif($item->role==3){echo 'Manager';}else{echo 'Thành viên';}?></td>
                        <td>
                            <a href="<?php echo base_url('admin/user/edit/'.$item->id);?>" class="label label-success"><i class="fa fa-pencil"></i>&nbsp;Sửa</a>
                        <?php $user_id = $this->session->userdata('user_id');
                        if($item->id!==$user_id){?>
                            &nbsp;&nbsp;|&nbsp;&nbsp;
                            <a href="<?php echo base_url('admin/user/del/'.$item->id);?>" class="label label-danger delete"><i class="fa fa-trash"></i>&nbsp;Xóa</a>
                            <?php }?>
                        </td>
                    </tr>
                        <?php }
                    }?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
            <div class="box-footer">
                <?php if(isset($total_page) && $total_page>1){
                    vcc_paging($total_page, 'user', 'page');
                }?>
            </div>
        </div><!-- /.box -->

    </div><!-- /.col -->
</div>