<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <div class="col-md-4">
                    <select name="" id="" class="form-control ">
                        <?php
                        foreach ($course as $item) {
                            ?>
                            <option value=""><?php echo $item['title']?></option>
                            <?php
                        }
                        ?>

                    </select>

                </div><!-- /.box-header -->
            </div>
            <div class="box-body">
                <table class="table">
                    <tbody>
                    <tr>
                        <th style="width: 20px">ID</th>
                        <th>Học viên</th>
                        <th>Ngày bắt đầu</th>
                        <th>Gần nhất</th>
                        <th>Trạng thái</th>
                    </tr>
                    <?php
                    foreach ($classmates as $item) {
                        ?>
                        <tr>
                            <td><?php echo $item['id']; ?></td>
                            <td>
                                <i class="glyphicon glyphicon-user"
                                   style="font-size: 50px;color: #CCCCCC; float: left;margin-right: 10px;"></i>
                                <div class="info">
                                    <strong> <?php echo $item['user_name']; ?></strong><br>
                                    <?php echo $item['user_phone']; ?><br/>
                                    <?php echo $item['user_email']; ?>
                                </div>

                            </td>
                            <td><?php echo date('d/m/Y', strtotime($item['created_on'])); ?></td>
                            <td><?php echo $item['user_email']; ?></td>
                            <td><?php echo $item['id']; ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>

            </div>
        </div><!-- /.box -->

    </div><!-- /.col -->
</div>
