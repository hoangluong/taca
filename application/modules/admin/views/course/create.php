<?php
$data = isset($data) ? $data : null;
?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 ">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <a class="btn btn-default" href="<?php echo base_url('admin/course')?>"><i class="glyphicon glyphicon-arrow-left"></i> Back</a>
            </div><!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                <?php if (isset($message)) { ?>
                    <!-- alert alert-info alert-dismissible -->
                    <div style="display: block;"
                         class="alert <?php echo $message['success'] ? 'alert-info ' : 'alert-danger '; ?>alert-dismissible"
                         id="message">
                        <button type="button" id="btn-close-msg" class="close" data-dismiss="alert" aria-hidden="true">×
                        </button>
                        <p id="message-content"><?php echo $message['msg']; ?></p>
                    </div>
                <?php } ?>
                <div class="box-body">
                    <div class="col-sm-12 col-md-12">
                        <div class="form-group">
                            <label for="title">Tên khóa học</label>
                            <input id="title" style="width: 50%;" value="<?php echo post_data('name', $data);?>" name="title" type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="desc">Mô tả khóa học</label>
                            <textarea id="desc"  name="desc" rows="10"  class="form-control"><?php echo post_data('desc', $data);?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="desc">Chi tiết</label>
                            <textarea id="content"  name="content" rows="10"  class="form-control"><?php echo post_data('desc', $data);?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="country">Giáo viên</label>
                            <select name="teacher" id="teacher" class="form-control" style="width: 50%;">
                                <?php
                                foreach ($teachers as $item){
                                ?>
                                <option value="<?php echo $item->id;?>"><?php echo $item->name;?></option>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                </div><!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" name="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> <?php echo $data ? 'Save' : 'Lưu & Tiếp tục';?></button>
                </div><!-- /.box-footer -->
            </form>
        </div><!-- /.box -->
    </div><!--/.col (left) -->
</div>   <!-- /.row -->

<script>
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.

    CKEDITOR.replace('content', {
        // Define the toolbar groups as it is a more accessible solution.
        toolbarGroups: [{
            "name": "basicstyles",
            "groups": ["basicstyles"]
        },
            {
                "name": "links",
                "groups": ["links"]
            },
            {
                "name": "paragraph",
                "groups": ["list", "blocks"]
            },
            {
                "name": "document",
                "groups": ["mode"]
            },
            {
                "name": "insert",
                "groups": ["insert"]
            },
            {
                "name": "styles",
                "groups": ["styles"]
            },
            {
                "name": "about",
                "groups": ["about"]
            }
        ],
        // Remove the redundant buttons from toolbar groups defined above.
        removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
    });
</script>