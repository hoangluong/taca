<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 3/1/2019
 * Time: 12:50 AM
 */
$_CI = &get_instance();
$_CI->load->model('course_model');
?>

<div class="alert alert-info alert-dismissible">
    <h4><?php echo $course[0]['title'];?></h4>

</div>
<ul>
    <?php
    foreach ($modules as $item) {
        ?>
        <li class="tree-module"><a href=""><i class="fa fa-fw fa-cubes"></i> <?php echo $item['title']; ?></a>
            <ul>
                <?php
                $videos = $_CI->course_model->get_all_video_by_module($item['id']);
                if (count($videos) > 0) {
                    foreach ($videos as $video) {
                        ?>
                        <li class="tree-video"><a href=""><i class="fa fa-fw fa-cube"></i><?php echo $video['title']; ?></a></li>
                    <?php }
                } ?>
            </ul>
        </li>
        <?php

    } ?>
</ul>
