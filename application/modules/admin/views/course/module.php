<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">

            <div class="panel-body">
                Khóa học: <?php echo $course[0]['title']; ?>
            </div>
        </div>
    </div>
    <div class="col-md-7">

        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    Danh sách module

                    <div class="box-tools pull-right">
                        <form action="" method="get" class="primary">
                            <div class="input-group" style="width: 250px;">
                                <input type="text" name="s"
                                       value="<?php echo htmlentities($this->input->get('s', true)); ?>"
                                       class="form-control input-md pull-right" placeholder="Tìm kiếm">
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-md btn-default"><i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">


                    <table class="table">
                        <tbody>
                        <tr>
                            <th style="width: 20px">#</th>
                            <th>Module</th>

                            <th style="text-align: right">Quản lý</th>
                        </tr>
                        <?php
                        if (isset($modules)) {
                            $i=0;
                            foreach ($modules as $item) {
                                $i++;
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $item['title'] ?></td>

                                    <td class="right" style="text-align: right">
                                        <a href="<?php echo base_url() ?>admin/course/video/<?php echo $item['id'] ?>"
                                           class="label label-info"><i class="fa fa-fw fa-file-video-o"></i>Chuyên
                                            đề</a>
                                        <a href="<?php echo base_url() ?>admin/question/index/module/<?php echo $item['id'] ?>"
                                           class="label label-success"><i class="fa fa-fw fa-file"></i>Trắc nghiệm</a>
                                    </td>
                                </tr>
                            <?php }
                        } ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div>
    <div class="col-md-5">
        <div class="box box-danger">
            <div class="box-header">
                <h3 class="box-title">Thêm mới module</h3>
            </div>
            <div class="box-body">
                <form class="form-horizontal" id="module-form"
                      action="<?php echo base_url() ?>admin/course/ajax_save_module" method="post"
                      enctype="multipart/form-data">
                    <?php if (isset($message)) { ?>
                        <!-- alert alert-info alert-dismissible -->
                        <div style="display: block;"
                             class="alert <?php echo $message['success'] ? 'alert-info ' : 'alert-danger '; ?>alert-dismissible"
                             id="message">
                            <button type="button" id="btn-close-msg" class="close" data-dismiss="alert"
                                    aria-hidden="true">×
                            </button>
                            <p id="message-content"><?php echo $message['msg']; ?></p>
                        </div>
                    <?php } ?>
                    <div class="box-body">
                        <div class="col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="title">Module </label>
                                <input id="title" value="" name="title" type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="desc">Giới thiệu</label>
                                <textarea id="desc" name="desc" rows="10" class="form-control"></textarea>
                            </div>
                        </div>
                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <input type="hidden" name="course_id" value="<?php echo $course[0]['id']; ?>">
                        <button type="submit" name="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Lưu
                        </button>
                    </div><!-- /.box-footer -->
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $("#module-form").submit(function (event) {
        alert('ok');
        event.preventDefault(); //prevent default action
        var post_url = $(this).attr("action"); //get form action url
        var request_method = $(this).attr("method"); //get form GET/POST method
        var form_data = $(this).serialize(); //Encode form elements for submission

        $.ajax({
            url: post_url,
            type: request_method,
            data: form_data
        }).done(function (response) { //
            response = JSON.parse(response);
            if (response.status == true) {
                window.location.reload();
            } else {
                alert(response.message);
            }
        });
    });
</script>