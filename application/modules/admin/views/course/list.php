<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <a class="btn btn-success" href="<?php echo base_url('admin/course/create'); ?>"><i
                            class="fa fa-plus"></i>&nbsp;Add</a>

                <div class="box-tools pull-right">
                    <form action="" method="get" class="primary">
                        <div class="input-group" style="width: 250px;">
                            <input type="text" name="s"
                                   value="<?php echo htmlentities($this->input->get('s', true)); ?>"
                                   class="form-control input-md pull-right" placeholder="Tìm kiếm">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-md btn-default"><i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table class="table">
                    <tbody>
                    <tr>
                        <th style="width: 20px">ID</th>
                        <th>Khóa học</th>
                        <th>Giáo viên</th>
                        <th>Quản lý</th>
                    </tr>
                    <?php
                    foreach ($course as $item) {
                        ?>
                        <tr>
                            <td><?php echo $item['id']; ?></td>
                            <td><?php echo $item['title']; ?></td>
                            <td><?php echo $item['teacher_name']; ?></td>
                            <td><a href="<?php echo base_url() ?>admin/course/module/<?php echo $item['id'] ?>"
                                   title="Quản lý module" class="label label-info"><i class="fa fa-fw fa-cubes"></i>
                                    quản lý module</a> | <a href="javascript:void(0);" module-id="<?php echo $item['id']?>" class="loadTreeCourse">Tree</a></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->

        </div><!-- /.box -->

    </div><!-- /.col -->
</div>

<script>
    $(document).ready(function () {
        $('.loadTreeCourse').click(function () {
            $.get( "course/tree/"+$(this).attr('module-id'), function( data ) {
                $('.treeContent').html(data);
            });
            $('.showTreeCourse').toggle('slow');

        });
        $('.showTreeCourseClose').click(function () {
            $('.showTreeCourse').toggle('slow');
        })
    });
</script>