<div class="box">
    <div class="box-header">
        Thêm câu hỏi
    </div><!-- /.box-header -->

    <div class="box-body">
        <form role="form" method="post" id="insertQuestionForm">
            <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Câu hỏi</label>
                    <textarea name="question" id="" cols="30" rows="5" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Phương án 1</label>
                    <textarea name="answer_1" id="" cols="30" rows="2" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Phương án 2</label>
                    <textarea name="answer_2" id="" cols="30" rows="2" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Phương án 3</label>
                    <textarea name="answer_3" id="" cols="30" rows="2" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Phương án 4</label>
                    <textarea name="answer_4" id="" cols="30" rows="2" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Phương án đúng</label>
                    <select name="answer_true" id="" class="form-control">
                        <option value="1">Phương án 1</option>
                        <option value="2">Phương án 2</option>
                        <option value="3">Phương án 3</option>
                        <option value="4">Phương án 4</option>

                    </select>
                </div>

            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <input type="hidden" value="<?php echo $module_id?>" name="module_id">
                <input type="hidden" value="<?php echo $video_id?>" name="video_id">
                <input type="hidden" value="<?php echo $course_id?>" name="course_id">
                <button type="submit" name="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>
<script>
    // this is the id of the form
    $("#insertQuestionForm").submit(function (e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        $.ajax({
            type: "POST",
            url: '<?php echo base_url('admin/question/ajax_update_question')?>',
            data: form.serialize(), // serializes the form's elements.
            success: function (data) {
                //       window.location.reload();
            }
        });


    });
</script>