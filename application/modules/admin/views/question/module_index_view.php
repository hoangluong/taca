<div class="row">
    <div class="clearfix">
        <div class="col-md-4">
            <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-yellow">

                    <!-- /.widget-user-image -->
                    <h3 class="widget-user-username"><?php echo $info['title'] ?></h3>
                    <h5 class="widget-user-desc">Khóa học: <?php echo $info['course_title'] ?></h5>
                </div>
                <div class="box-footer no-padding">
                    <ul class="nav nav-stacked">
                        <!--  <li><a href="#">Projects <span class="pull-right badge bg-blue">31</span></a></li>
                          <li><a href="#">Tasks <span class="pull-right badge bg-aqua">5</span></a></li>
                          <li><a href="#">Completed Projects <span class="pull-right badge bg-green">12</span></a></li>
                          <li><a href="#">Followers <span class="pull-right badge bg-red">842</span></a></li>-->
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="box">
            <div class="box-header">
                Danh sách câu hỏi
            </div><!-- /.box-header -->
            <div class="box-body">
                <table class="table">
                    <tbody>
                    <tr>
                        <th style="width: 20px">ID</th>
                        <th>Câu hỏi</th>
                        <th>Trả lời 1</th>
                        <th>Trả lời 2</th>
                        <th>Trả lời 3</th>
                        <th>Trả lời 4</th>

                        <th>Quản lý</th>
                    </tr>
                    <?php
                    foreach ($questions as $item) {
                        ?>
                        <tr>
                            <td><?php echo $item['id'] ?></td>
                            <td><?php echo $item['question']; ?></td>
                            <td class="q_1_<?php echo $item['answer_true'] ?>"><?php echo $item['answer_1']; ?></td>
                            <td class="q_2_<?php echo $item['answer_true'] ?>"><?php echo $item['answer_2']; ?></td>
                            <td class="q_3_<?php echo $item['answer_true'] ?>"><?php echo $item['answer_3']; ?></td>
                            <td class="q_4_<?php echo $item['answer_true'] ?>"><?php echo $item['answer_4']; ?></td>

                            <td><a class="delete" data-id="<?php echo $item['id']; ?>" href="javascript:void(0)">Xóa</a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->

        </div><!-- /.box -->

    </div><!-- /.col -->
    <div class="col-md-4">
        <div class="show-question-form">

        </div>
    </div>
</div>
<style>
    .q_1_1, .q_2_2, .q_3_3, .q_4_4 {
        color: red;
        font-weight: bold;
    }
</style>
<script>
    $(document).ready(function () {

        $.get("<?php echo base_url('admin/question/ajax_load_question_form').'?'.$type_field_id.'='.$info['id'];?>", function (data) {
            $(".show-question-form").html(data);

        });


        $(".delete").click(function () {
            if (confirm('Bạn có chắc chắn muốn xóa bản ghi này không?')) {
                $.ajax({
                    type: "GET",
                    url: '<?php echo base_url('admin/question/delete')?>',
                    data: {id: $(this).attr('data-id')}, // serializes the form's elements.
                    success: function (data) {
                        window.location.reload();
                    }
                });
            }
        });
    });
</script>