<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Logout extends Api_Controller{
    public function __construct(){
        parent::__construct();
    }
    function index(){
        $uid = $this->input->post('uid', true);
        $token = $this->input->post('token', true);
        $this->load->model('token_model');
        $old_data = $this->token_model->find_by('uid', $uid);
        if($old_data){
            $this->tracking($old_data->uid, 0, 'logout');
            //$this->token_model->delete_by('uid', $uid);
            $this->token_model->delete_by('token', $token);
	    }
        $this->renderJSON(array(), true, $msg='See you again', array());
    }
}