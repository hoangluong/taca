<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Find_product extends Api_Controller{
    public function __construct(){
        parent::__construct();
		$this->load->model('auth/user_model');
		$this->load->model('product/product_model');
        $this->load->helper('text');
    }
    function Ebay_getMostWatchedItems(){
        $this->check_auth();
        $data = array();
        $msg = null;
        $status = true;
        $meta = array();
        try{
            $categoryId = $this->input->post('categoryId', true);
			$url = 'http://svcs.ebay.com/MerchandisingService?OPERATION-NAME=getMostWatchedItems&SERVICE-NAME=MerchandisingService&SERVICE-VERSION=1.1.0&CONSUMER-ID=tamvu-tooldrop-PRD-9b7eb28a6-730c3766&RESPONSE-DATA-FORMAT=JSON&REST-PAYLOAD&maxResults=20&categoryId='.$categoryId;
			
			$json = file_get_contents($url);
			$json_data = json_decode($json, true);
			
			if($json_data['getMostWatchedItemsResponse']['ack']=='Success'){
				$data_product_find = $json_data['getMostWatchedItemsResponse']['itemRecommendations']['item'];
				$meta['html'] = $this->load->view('api/product-find-ebay', array('data'=>$data_product_find),true);
			}
		} catch(Exception $e){
            $msg = $e->getMessage();
			$status = false;
		}
		$this->renderJSON($data, $status, $msg, $meta);
    }
	function Ebay_search_item(){
		$data = array();
        $msg = null;
        $status = true;
        $meta = array();
        try{
            //$categoryId = $this->input->post('categoryId', true);
			$keyword =  $this->input->post('keyword', true);
			$category = $this->input->post('categoryId', true);
			$sortOrder = $this->input->post('sortOrder', true);
			
			//$keyword ="iphone 7s";
			//$category =99;
			//$sortOrder = "WatchCountDecreaseSort";
			$url = 'http://svcs.ebay.com/services/search/FindingService/v1?OPERATION-NAME=findItemsByKeywords&SERVICE-VERSION=1.0.0&SECURITY-APPNAME=tamvu-tooldrop-PRD-9b7eb28a6-730c3766&RESPONSE-DATA-FORMAT=JSON&REST-PAYLOAD&maxResults=20';
			if(!empty($keyword)) $url =$url.'&keywords='.urlencode($keyword);
			if(!empty($category)) $url =$url.'&categoryId='.$category;
			if(!empty($sortOrder)) $url =$url.'&sortOrder='.$sortOrder;
			
			//$url = 'http://svcs.ebay.com/services/search/FindingService/v1?OPERATION-NAME=findItemsByKeywords&SERVICE-VERSION=1.0.0&SECURITY-APPNAME=tamvu-tooldrop-PRD-9b7eb28a6-730c3766&RESPONSE-DATA-FORMAT=JSON&REST-PAYLOAD&categoryId=2984&keywords=clothes&sortOrder=WatchCountDecreaseSort';
			$json = file_get_contents($url);
			$json_data = json_decode($json,true);
			
			if($json_data['findItemsByKeywordsResponse'][0]['ack'][0]=='Success'){
				
				$data_product_find = $json_data['findItemsByKeywordsResponse'][0]['searchResult'][0]['item'];
				$meta['html'] = $this->load->view('api/product-find-ebay-new', array('data'=>$data_product_find),true);
				
				//echo $this->load->view('api/product-find-ebay-new', array('data'=>$data_product_find),true);
			
			}
			
			
		} catch(Exception $e){
            $msg = $e->getMessage();
			$status = false;
		}
		
		$this->renderJSON($data, $status, $msg, $meta);
		
	}
	function Ali_getBestSelling(){
		//$this->check_auth();
		$data = array();
		$msg = null;
		$status = true;
		$meta = array();
		try{
			$s = $this->input->post('s', true);
			if($s){
				$data = $this->ali_find_best_selling_v1($s, $meta);
				$meta['html'] = $this->load->view('api/product-ali-find-selling', array('data'=>$data),true);
			}else{
				$data = $this->ali_all_best_selling();
				$products = isset($data['items']) ? $data['items'] : array();
				$meta['html'] = $this->load->view('api/product-ali-all-selling', array('data'=>$products),true);
			}

		} catch(Exception $e){
			$msg = $e->getMessage();
			$status = false;
		}
		$this->renderJSON($data, $status, $msg, $meta);
	}

	private function ali_all_best_selling(){
		//1. curl https://bestselling.aliexpress.com/en?spm=2114.11010108.21.3.35f7175Y3GDbT
		$output = $this->curl_out_site("https://www.aliexpress.com/home/recommendEntry.do?sceneId=9907&countryId=ALL&_=1504940434824");
		$len = strlen($output);
		$output = substr($output, 1, $len);
		$output = substr($output, 0, -2);
		$data = json_decode($output, true);
		return $data;
	}

	/* call api tamvv */
	private function ali_find_best_selling_v2($s){
		$output = $this->curl_out_site('http://107.155.87.245:3000/getproduct_search_ali?s=' . urlencode($s));
		return json_decode($output, true);
	}

	private function ali_find_best_selling_v1($s, &$meta){
		$data = array();
		//https://www.aliexpress.com/wholesale?groupsort=1&SortType=total_tranpro_desc&g=y&SearchText=naviforce+watch+men
		$protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
		$url = 'https://www.aliexpress.com/w/wholesale-'.url_friendly($s).'.html?spm=2114.search0104.0.0.QjCubd&initiative_id=SB_20170909024647&site=glo&groupsort=1&SortType=total_tranpro_desc&SearchText='.urlencode($s);
		$output = $this->curl_out_site($url);
		if(!$output) return $data;

		$content = $this->getBlock($output, '<ul class="util-clearfix son-list util-clearfix" id="hs-below-list-items">', '</ul>');
		$meta['url'] = $url;
		file_put_contents(FCPATH.'ali_html', $output);

		if(!$content) return $data;
		$count = 0;
		do{
			$count++;
			$start = '<li';
			$end = '</li>';
			$item = $this->getBlock($content, $start, $end, true, true);
			if(!$item) break;

			$pic = $this->getBlock($item, '<a class="picRind', '</a>', true, true);
			$link = $this->getBlock($pic, 'href="', '"', true, true);

			$img = $this->getBlock($item, 'picCore pic-Core-v', '/>', true, true);
			$title = $this->getBlock($img, 'alt="', '"', true, true);
			$thumbnail = $this->getBlock($img, 'src="', '"', true, true);

			$price = trim($this->getBlock($item, 'itemprop="price">', '</span>', true, true));
			$orders = trim($this->getBlock($item, '<em title="Total Orders">', '</em>', true, true));

			$data[] = array(
				'title' => $title,
				'thumbnail' => $thumbnail,
				'price' => $price,
				'orders' => $orders,
				'link' => $link,
			);

			$content = substr($content, strpos($content, $end)+strlen($end));
			//print_r($content);exit();
		}while($item!='' && $count<=20);
		return $data;
	}
	private function curl_out_site($url){
		return file_get_contents($url);
	}
	private function curl_out_site_bk($url){
		$ch = curl_init();
		$headers = [
			':authority:www.aliexpress.com',
			'accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
			'accept-encoding:gzip, deflate, br',
			'accept-language:en-GB,en;q=0.8,ja;q=0.6,vi;q=0.4',
			'cache-control:max-age=0',
			'cookie:ali_apache_id=10.182.248.36.1503649702892.152107.3; xman_t=7RwxxH2rxi/rhMzeCPMafhvlZSOCsE1xD6f1jlAjOK6CtjstVwuizijY3kN0mflf; xman_f=D9XRtVSJyw5eundFlhIOsFjsrYnkLGwOvugWk4noO2RrhxhPTpWe3gIzPNtLF/Tse1klQVZ5NlM/wYRa7ReB7/zYOelNr08idP16x05YTDsAVqQCLf9FnA==; _uab_collina=150366051232192097238426; aeu_cid=7d528be8ede3412fa9172d1dfe5a966c-1503997489540-09962-uvrVJEu; ali_beacon_id=10.182.248.36.1503649702892.152107.3; _umdata=AC9CC5254741F6A00C8DB9D5B116C2633C567198671A769EEFCC5A8B5A663707E493375AD5547F01CD43AD3E795C914CDC01E13EFB8D6B67A437E3AAD2E1B999; cna=qs0mEhFI9CECARtIZIl3pOOe; aep_history=keywords%5E%0Akeywords%09%0A%0Aproduct_selloffer%5E%0Aproduct_selloffer%0932803158142%0932794789664%0932457370321%0932515248849; JSESSIONID=5246808D7963C91F1486A9E5FFD05BA8; _mle_tmp0=eNrz4A12DQ729PeL9%2FV3cfUx8KvOTLFSMjUyMbMwsHAxtzQzdrY0dDM0sTBztHQ1dXNzMTB1crRQ0kkusTI0NTCxNDU2sDAwMDTUSUxGE8itsDKojQIAlH0XqQ%3D%3D; acs_usuc_t=x_csrf=1d25oh55of7pb&acs_rt=41c5ce69071549559dc4df2ff4c86aa3; _gat=1; xman_us_f=x_l=1&x_locale=en_US&x_as_i=%7B%22cv%22%3A%221%22%2C%22tp1%22%3A%2255styles%22%2C%22src%22%3A%22data-feeds%22%2C%22af%22%3A733938587%2C%22cpt%22%3A1503997489540%2C%22channel%22%3A%22AFFILIATE%22%2C%22affiliateKey%22%3A%22uvrVJEu%22%2C%22tagtime%22%3A1503997489543%2C%22vd%22%3A%2230%22%7D; intl_locale=en_US; aep_usuc_f=site=glo_v&region=VN&b_locale=en_US&c_tp=USD; intl_common_forever=PhuBuGR0ujtvoTr2hEJLpXUtdoPsXIOqvEslScFFykxCL8Uwh4047A==; _ga=GA1.2.1093907746.1503649708; _gid=GA1.2.1746358936.1504939714; isg=AnV1IGmvQIegAaQXuOZXjnbahPHvWg1QlttpffeaMew7zpXAv0I51INMPqWC; ali_apache_track=; ali_apache_tracktmp=',
			'upgrade-insecure-requests:1',
			'Content-Type: application/x-www-form-urlencoded; charset=utf-8',
			'Host: www.aliexpress.com',
			'Referer: '.$url, //Your referrer address
			'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
		];
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		// set url
		curl_setopt($ch, CURLOPT_URL, $url);

		//return the transfer as a string
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		// $output contains the output string
		$output = curl_exec($ch);

		// close curl resource to free up system resources
		curl_close($ch);
		return $output;
	}

	private function getBlock($content, $start='', $end='', $without_start=false, $without_end=false){
		$len = strlen($content);
		if($without_start){
			$start_offset = $start ? strpos($content, $start) : 0;
			if($start_offset===false) return '';
			$start_offset = $start_offset+strlen($start);
		}else{
			$start_offset = $start ? strpos($content, $start) : 0;
			if($start_offset===false) return '';
		}
		if($without_end){
			$end_offset = $end ? strpos($content, $end, $start_offset) : $len;
			if($end_offset===false) return '';
		}else{
			$end_offset = $end ? strpos($content, $end, $start_offset) : $len;
			if($end_offset===false) return '';
			$end_offset = $end_offset+strlen($end);
		}
		if($start_offset>=$end_offset) return '';
		$result = substr($content, $start_offset, $end_offset-$start_offset);
		return $result;
	}
	
	
	function array_random($array, $amount = 1)
		{
			$keys = array_rand($array, $amount);

			if ($amount == 1) {
				return $array[$keys];
			}

			return array_intersect_key($array, array_flip($keys));
		}
   
}
