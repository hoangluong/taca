<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class State extends Api_Controller{
    public function __construct(){
        parent::__construct();
		$this->load->model('country/country_model');
    }
    function ali_to_ebay(){
        $this->check_auth();
        $data = array();
        $msg = null;
        $status = true;
        $meta = array();
        try{
            $uid = $this->input->post('uid', true);
            $state_code = $this->input->post('state_code', true);
            $country = $this->input->post('country', true);

            $cond = array(
                'state_code'=>$state_code,
                //'country'=>$country
            );
            $data = $this->country_model->find($cond, 1, 1, array());
            if(!$data) throw new Exception("Error. Please try again latter.");
            $data = $data[0];
            $meta['state'] = $data->state;
            $meta['country'] = $data->country_code;
            $msg = 'Success!';
        } catch(Exception $e){
            $msg = $e->getMessage();
            $status = false;
        }
        $this->renderJSON($data, $status, $msg, $meta);
    }
}
