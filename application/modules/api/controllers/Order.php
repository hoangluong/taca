<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Order extends Api_Controller{
    public function __construct(){
        parent::__construct();
		$this->load->model('order_model');
    }
	public function index(){
		echo 1;
	}
    function add(){
        $this->check_auth();
		$data = array();
        $msg = null;
        $meta = array('Order_exists' => false);
        $status = true;
		try{
            $uid = isset($this->request['uid']) ? stripslashes($this->request['uid']) : '';
            $order_ebay = isset($this->request['order_ebay']) ? stripslashes($this->request['order_ebay']) : '';
            $buyer_ebay = isset($this->request['buyer_ebay']) ? stripslashes($this->request['buyer_ebay']) : '';
            $ali_link = isset($this->request['sale_ali']) ? stripslashes($this->request['sale_ali']) : '';
            $transaction_data = isset($this->request['transaction_data']) ? stripslashes($this->request['transaction_data']) : '';
            $image = isset($this->request['image']) ? stripslashes($this->request['image']) : '';
            $qty = isset($this->request['qty']) ? stripslashes($this->request['qty']) : '';

            if(time() > strtotime('2017-10-15 23:59:59') || $uid==9) {
                $valid_order = $this->expired_check_order($uid);
                if(!$valid_order){
                    throw new Exception('Importing order is limited');
                }
            }
            $data = array(
                'uid' => $uid,
                'order_ebay' => $order_ebay,
                'buyer_ebay' => $buyer_ebay,
                'ali_link' => $ali_link,
                'qty' => $qty,
                'image' => $image,
                'status' => 1,
                'transaction_data' => $transaction_data,
                'order_date' => date('Y-m-d H:i:s')
            );

            $check_exists = $this->order_model->check_exists_order($uid, $order_ebay);
            if(!$check_exists){
                $res = $this->order_model->add($data);
                $msg = 'Order created successful';
            }else{
                if($check_exists->status==0){
                    $data['status'] = 1;
                    $msg = 'Order created successful!';
                }else{
                    $msg = 'Order updated successful!';
                }
                $this->order_model->update($check_exists->id, $data);
            }
            $this->tracking($uid, 0, 'saved_order');
		} catch(Exception $e){
            $msg = $e->getMessage();
            $status = false;
        }
		$this->renderJSON($data, $status, $msg, $meta);
    }
    private function expired_check_order($uid){
        $user_data = $this->user_model->get($uid);
        $current_time = time();
        $expired_time = strtotime($user_data->expired);
        if($user_data && $expired_time<$current_time){
            $date_offset = $current_time - $expired_time;
            $date_offset = ceil($date_offset/86400);
            $per_month = ceil($date_offset/30);
            $total_valid = $per_month * 10;
            $total = $this->order_model->total(array('uid'=>$uid, 'created_on >='=>$user_data->expired));
            if($total_valid < $total){
                return false;
            }
        }
        return true;
    }
	function list_order(){
		$this->check_auth();
        $data = array();
        $msg = null;
        $status = true;
        $meta = array();
        try{
            $uid = isset($this->request['uid']) ? stripslashes($this->request['uid']) : '';
            $cond = array(
                'uid' => $uid,
                'status' => 1
            );
            $per_page = $this->per_page;
            $page = stripslashes($this->input->post('page', true));
            $page = $page ? $page : 1;
            $total = $this->order_model->total($cond);
            $total_page = $total%$per_page==0 ? $total/$per_page : intval($total/$per_page)+1;
            $data = $this->order_model->find($cond, $page, $per_page, array('id'=>'desc'));

            $meta['html'] = $this->load->view('api/order-items', array('data'=>$data), true);
            $meta['html_paging'] = $this->load->view('api/order-paging', array('total_page'=>$total_page, 'paged'=>$page), true);
        } catch(Exception $e){
            $msg = $e->getMessage();
            $status = false;
        }
        $this->renderJSON($data, $status, $msg, $meta);
	}

    function detail_order(){
        $this->check_auth();
        $data = array();
        $msg = null;
        $status = true;
        $meta = array();
        try{
            $meta['html'] = '';
            $id = $this->input->post('id', true);
            $detail = $this->input->post('', true);
            $detail = $detail ? '-'.$detail : '';
            $data = $this->order_model->get($id);
            $meta['html'] = $this->load->view('order-detail'.$detail, array('data'=>$data), true);
        } catch(Exception $e){
            $msg = $e->getMessage();
            $status = false;
        }
        $this->renderJSON($data, $status, $msg, $meta);
    }

    function delete(){
        $this->check_auth();
        $data = array();
        $msg = null;
        $status = true;
        $meta = array();
        try{
            $uid = $this->input->post('uid', true);
            $id = $this->input->post('id', true);

            $check_exists = $this->order_model->get($id);
            if(!$check_exists) throw new Exception('Order is not exits');
            if($uid != $check_exists->uid) throw new Exception('You can not edit other order');
            //$res = $this->order_model->delete($id);
            $res = $this->order_model->update($id, array('status'=>0));
            if(!$res) throw new Exception('Error. Please try again.');
            $msg = 'Success.';
        } catch(Exception $e){
            $msg = $e->getMessage();
            $status = false;
        }
        $this->renderJSON($data, $status, $msg, $meta);
    }
    function update(){
        $this->check_auth();
        $data = array();
        $msg = null;
        $status = true;
        $meta = array();
        try{
            $uid = $this->input->post('uid', true);
            $id = $this->input->post('id', true);
            $field = $this->input->post('field', true);
            $field_data = $this->input->post('field_data', true);

            $check_exists = $this->order_model->get($id);
            if(!$check_exists) throw new Exception('Order is not exits');
            if($uid != $check_exists->uid) throw new Exception('You can not edit other order');
            $res = $this->order_model->update($id, array($field=>$field_data));

            if(!$res) throw new Exception('Error. Please try again.');
            $msg = 'Saved successful.';
        } catch(Exception $e){
            $msg = $e->getMessage();
            $status = false;
        }
        $this->renderJSON($data, $status, $msg, $meta);
    }
	
	function update_order(){
		$this->check_auth();
        $data = array();
        $msg = null;
        $status = true;
        $meta = array();
        try{
            $uid = isset($this->request['uid']) ? stripslashes($this->request['uid']) : 0;
            $id = isset($this->request['id']) ? stripslashes($this->request['id']) : 0;
            $tracking_number = isset($this->request['tracking_number']) ? stripslashes($this->request['tracking_number']) : 0;
			
            $check_exists = $this->order_model->get($id);
			
            if($check_exists){
               if($uid != $check_exists->uid) throw new Exception('You can not edit other order');
                if(!$tracking_number) throw new Exception('Tracking number error');
                $this->order_model->update($id, array('tracking_number' => $tracking_number));
		   
		   
		   }else{
                throw new Exception('Order is not exits');
           }
			
			
            $msg = 'Sửa nhãn thành công';
        } catch(Exception $e){
            $msg = $e->getMessage();
            $status = false;
        }
        $this->renderJSON($data, $status, $msg, $meta);
	}
	function get_template_email(){
		$this->check_auth();
        $data = array();
        $msg = null;
        $status = true;
        $meta = array();
        try{
            $this->load->model('email_template/email_template_model');
            $order_id = $this->input->post('order_id', true);
            $data = $this->email_template_model->find_all();
            $meta['html'] = $this->load->view('api/order-email-templates', array('data'=>$data, 'order_id'=>$order_id), true);
			
        } catch(Exception $e){
            $msg = $e->getMessage();
            $status = false;
        }
        $this->renderJSON($data, $status, $msg, $meta);
	}
	function get_template_email_order_id(){
        $this->check_auth();
        $data = '';
        $msg = null;
        $status = true;
        $meta = array();
        try{
            $this->load->model('email_template/email_template_model');
            $this->load->model('auth/user_model');
            $uid = $this->input->post('uid', true);
            //$id = isset($this->request['id']) ? stripslashes($this->request['id']) : '';
            $order_id = $this->input->post('order_id', true);
            $template_id = $this->input->post('template_id', true);

            $email_template = $this->email_template_model->get($template_id);
            if(!$email_template) throw new Exception('Mẫu email không tồn tại');
            $order = $this->order_model->get($order_id);
            if(!$order) throw new Exception('Đơn hàng không tồn tại');

            $buyer_ebay = json_decode($order->buyer_ebay);
            $user_data = $this->user_model->get($uid);
            if(!$user_data) throw new Exception('Tài khoản không tồn tại');

            $email_template->template_content = str_replace(
                array('{Guest}', '{Store owner}'),
                array($buyer_ebay->buyercontactname, $user_data->store),
                $email_template->template_content);
            $data = $email_template;
            $data->to = $buyer_ebay->email;
        } catch(Exception $e){
            $msg = $e->getMessage();
            $status = false;
        }
        $this->renderJSON($data, $status, $msg, $meta);
	}
	
	function test(){
		
		 $data = $this->order_model->get_template_email_order_id(3,1);
		 var_dump($data);
		 
	}
	
	
}