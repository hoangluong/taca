<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends Api_Controller{
    public function __construct(){
        parent::__construct();
		$this->load->model('auth/user_model');
    }
    function index(){
        $data = array();
        $msg = null;
        $status = true;
        $meta = array();
        try{
            $username = isset($this->request['username']) ? stripslashes($this->request['username']) : '';
            $password = isset($this->request['password']) ? stripslashes($this->request['password']) : '';

			if(!$username) throw new Exception('Email is required');
            if(!$password) throw new Exception('Password is required');
			$response = $this->user_model->checkUsername($username);
			if(!$response) throw new Exception('Email or password is not valid');
			if($response->password != md5($password)) throw new Exception('Email or password is not valid');
			if(!$response->status) throw new Exception('Account is not activated.');
			/*if(strtotime($response->expired)<time()){
                $meta['expired'] = true;
                throw new Exception('Account is expired. Please renew more.');
            }*/
            /*$this->load->model('token_model');
            $token_data = $this->token_model->find_by('uid', $response->id);
            if($token_data){
                $meta['logged'] = true;
                throw new Exception('Account is logged on other device. <br/><a id="logout_all" data-uid="'.$response->id.'" data-token="'.$token_data->token.'" href="#">Logout all device</a>');
            }*/

            foreach($response as $field=>$val){
                $data[$field] = $val;
            }
            unset($data['password']);
            if(strtotime($data['expired']) <= time()){
                $data['plan_info'] = 'Free Plan';
                $data['account_pro'] = false;
            }else{
                $data['plan_info'] = 'Pro Plan, Expried: '.date('d/M/Y', strtotime($data['expired']));
                $data['account_pro'] = true;
            }
            $data['token'] = $this->generate_token();
            $meta['remember_me'] = $this->input->post('remember_me', true);
            $this->save_token($data);
			$this->tracking($data['id'], 0, 'logged');
	    } catch(Exception $e){
            $msg = $e->getMessage();
            $status = false;
        }
        $this->renderJSON($data, $status, $msg, $meta);
    }
    private function save_token($data){
        $this->load->model('token_model');
        $this->token_model->add(array(
            'uid' => $data['id'],
            'name' => $data['name'],
            'username' => $data['username'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'avatar' => $data['avatar'],
            'token' => $data['token']
        ));
        return $data;
    }

    function check(){
        $this->check_auth();
    }
}
