<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Product extends Api_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('auth/user_model');
        $this->load->model('product/product_model');
        $this->load->model('product/product_content_model');
        $this->load->helper('text');
        $this->per_page = 5;
        $this->limit = 20;
    }
    function add(){
        $this->check_auth();
        $data = array();
        $msg = null;
        $status = true;
        $meta = array();
        try{
            $uid = $this->input->post('uid', true);
            $source_link = $this->input->post('source_link', true);
            $product_id =  $this->input->post('product_id', true);
            $images_thumb = $this->input->post('images_thumb', true);
            $images_gallery = $this->input->post('images_gallery', true);
            $price = $this->input->post('price', true);
            $price = $this->process_price($price);
            $total_product = $this->expired_total_product($uid);
            if($total_product >= $this->limit){
                throw new Exception('You have reached limit to import new product for free plan. <br/><a href="'.base_url('pricing').'" target="_blank">Upgrade now</a>');
            }

            if(!$product_id) throw new Exception('Not found product id');
            $check_product = $this->product_model->find(array('product_id'=>$product_id, 'uid'=>$uid), 1, 1, array());

            $source_type = 'ali';
            if(strpos($source_link, 'ebay.com')!==false){
                $source_type = 'ebay';
            }elseif(strpos($source_link, 'amazon.com')!==false){
                $source_type = 'amazon';
            }elseif(strpos($source_link, 'dhgate.com')!==false){
                $source_type = 'dhgate';
            }elseif(strpos($source_link, 'aliexpress.com')!==false){
                $source_type = 'ali';
            }
            if($images_gallery){
                foreach($images_gallery as &$item){
                    $item = str_replace('.jpg_50x50', '', $item);
                }
                $images_gallery = json_encode($images_gallery);
            }
            $specifics = $this->input->post('specifics', true);
            $properties = $this->input->post('properties', true);
            if(!$specifics && $properties){
                $properties = json_decode($properties);
                $specifics = array();
                foreach ($properties as $item) {
                    $specifics[] = array(
                        'title' => $item->propery_title,
                        'value' => $item->propery_des
                    );
                }
                $specifics = json_encode($specifics);
            }
            $gallery = str_replace('.jpg_50x50', '', $images_thumb);
            $image = $this->input->post('images', true);
            if(!$image && !empty($gallery)){
                $image = $gallery[0];
            }
			$data = array(
                'uid' => $uid,
                'product_id' => $product_id,
                'name' => trim( strip_tags($this->input->post('name', true)) ),
                'price' => $price,
                'quantity' => $this->input->post('quantity') ? $this->input->post('quantity') : 1,
                'specifics' => $specifics,
                'images' => $image,
                'images_thumb' => $gallery,
                'images_gallery' =>$images_gallery,
                'source_link' => $source_link,
                'source_type' => $source_type
            );
			
			if($check_product){
                $data_exists = $check_product[0];
                $id = $data_exists->id;
                if($data_exists->status==0){
                    $data['status'] = 1;
                    $msg = 'Product is saved!';
                }else{
                    $msg = 'Product is updated!';
                }
                $res = $this->product_model->update($id, $data);
            }else{
                $res = $this->product_model->add($data);
                if(!$res) throw new Exception("Error. Please try again latter.");
                $id = $res;
                $msg = 'Product is saved!';
            }
            $this->update_product_content($id);
            $this->save_variants($id);
            $data = $this->product_model->get($id);
            $meta['product_link'] = base_url('product/edit/'.$id);
            $this->tracking($uid, 0, 'saved_product');
        } catch(Exception $e){
            $msg = $e->getMessage();
            $status = false;
        }
        $this->renderJSON($data, $status, $msg, $meta);
    }

    private function process_price($str_price=''){
        if(!$str_price){
            return 0;
        }
        $price = trim( strip_tags($str_price) );
        $price = str_replace(array('     ', '    ', '   ', '  '), ' ', $price);
        $price = str_replace(array(' / Piece', 'GBP '), '', $price);
        $price = trim(str_replace(" /n", '', $price));
        if(strpos($price, '-')!==false){
            $arr = explode('-', $price);
            $price = trim( $arr[0] );
        }
        if(strpos($price, '$')!==false){
            $price = str_replace('$', '', strstr($price, '$') );
        }
        return $price;
    }

    private function update_product_content($product_id){
        $description = $this->input->post('description', false);
        $description = str_replace(array('Dropshipping', 'dropshipping', 'Wholesale', 'wholesale', 'Drop', 'drop'), '', $description);
        $content_exist = $this->product_content_model->find_by('pid', $product_id);
        if($content_exist){
            $this->product_content_model->update($content_exist->id, array(
                'description' => $description,
                'content_template' => ''
            ));
        }else{
            $this->product_content_model->add(array(
                'pid' => $product_id,
                'description' => $description
            ));
        }
    }

    private function save_variants($product_id){
        $variants = $this->input->post('variants', true);
        $variants = json_decode($variants);
        if(empty($variants)){
            return false;
        }
        $this->load->model('product/product_variant_model');
        $old_data = $this->product_variant_model->find_all_by('product_id', $product_id);
        $old_data_mapping = array();
        if($old_data){
            foreach($old_data as $item){
                $old_data_mapping[$item->sku] = $item;
            }
        }

        foreach($variants as $item){
            $sku = $this->get_variant_sku($item->variants);
            $item_variant_import = array(
                'product_id' => $product_id,
                'image' => isset($item->image) ? $item->image : '',
                'sku' => $sku,
                'variants' => json_encode($item->variants),
                'price' => $item->price,
                'quantity' => $item->quantity>3 ? 3 : $item->quantity
            );
            if(array_key_exists($sku, $old_data_mapping) === true){
                $old_item_data = $old_data_mapping[$sku];
                $this->product_variant_model->update($old_item_data->id, $item_variant_import);
            }else{
                $this->product_variant_model->add($item_variant_import);
            }
        }
        return true;
    }
    private function get_variant_sku($variant){
        $sku_arr = array();
        foreach($variant as $variant_name => $variant_value){
            $sku_arr[] = strtoupper( url_friendly($variant_value, '') );
        }
        return implode('_', $sku_arr);
    }
    
    private function expired_total_product($uid){
        $user_data = $this->user_model->get($uid);
        $current_time = time();
        $expired_time = strtotime($user_data->expired);
        $total_product = -1;
        if($user_data && $expired_time<$current_time){
            $time_offset = $current_time-$expired_time;
            $month_offset = ceil($time_offset/(30*86400)) - 1;
            $last_expired = strtotime('+'.(30*$month_offset-1).' day', $expired_time);
            $expired = date('Y-m-d H:i:s', $last_expired);
            $total_product = $this->product_model->total(array('uid'=>$uid, 'created_on >='=>$expired));
        }
        return $total_product;
    }
    function get(){
        $this->check_auth();
        $data = array();
        $msg = null;
        $status = true;
        $meta = array();
        try{
            $uid = $this->input->post('uid', true);
            $product_id = $this->input->post('id', true);
            $data = $this->product_model->get($product_id);
            if(!$data) throw new Exception("Error. Please try again latter.");
            if($data->uid!=$uid) throw new Exception('Product is not exists.');
            $data->properties = json_decode($data->properties);
            $data->description = '';
            $data->content_template = '';
            $product_content = $this->product_content_model->find_by('pid', $data->id);
            if($product_content){
                $data->description = $product_content->description;
                $data->content_template = $product_content->content_template;
            }
            $msg = 'Success!';
        } catch(Exception $e){
            $msg = $e->getMessage();
            $status = false;
        }
        $this->renderJSON($data, $status, $msg, $meta);
    }
    function all_product(){
        $this->check_auth();
        $data = array();
        $msg = null;
        $status = true;
        $meta = array('html'=>'');
        try{
            $uid = $this->input->post('uid', true);
            $type = $this->input->post('type', true);
            $s = $this->input->post('s', true);
            $created_on = $this->input->post('created_on', true);

            $cond = array(
                'status' => 1
            );
            if($uid){
                $cond['uid'] = $uid;
            }
            if($type){
                $cond['source_type'] = $type;
            }
            if($s){
                $cond['s'] = $s;
            }
            if($created_on){
                $date_time = strtotime($created_on);
                $cond['created_on >='] = date('Y-m-d 00:00:00', $date_time);
                $cond['created_on <='] = date('Y-m-d 23:59:59', $date_time);
            }

            $per_page = $this->per_page;
            $page = stripslashes($this->input->post('page', true));
            $page = $page ? $page : 1;
            $total = $this->product_model->total($cond);
            $total_page = $total%$per_page==0 ? $total/$per_page : intval($total/$per_page)+1;
            $select_fields = array('id','uid','product_id', 'name', 'price', 'images_thumb', 'images', 'source_link');
            $data = $this->product_model->find($cond, $page, $per_page, array('id'=>'desc'), $select_fields);
            //$meta['sql'] = $this->product_model->db->last_query();
            //if(!$data) throw new Exception("Error. Please try again latter.");
            $msg = 'Success!';
            $meta['html'] = $this->load->view('api/product-items', array('data'=>$data),true);
            $meta['html_paging'] = $this->load->view('api/product-paging', array('total_page'=>$total_page, 'paged'=>$page), true);
            $meta['total_page'] = $total_page;

            $remain_product = $this->expired_total_product($uid);
            if($remain_product >= 0 ){
                $meta['remain_product'] =  '(Limited: '.$remain_product . '/' . $this->limit.')';
            }
        } catch(Exception $e){
            $msg = $e->getMessage();
            $status = false;
        }
        $this->renderJSON($data, $status, $msg, $meta);
    }
    function remove(){
        $this->check_auth();
        $data = array();
        $msg = null;
        $status = true;
        $meta = array();
        try{
            $id = $this->input->post('id', true);
            $check = $this->product_model->get($id);
            if(!$check) throw new Exception('Product is not exists.');
            //$this->product_model->delete($id);
            $this->product_model->update($id, array('status'=>0));

        }catch(Exception $e){
            $msg = $e->getMessage();
            $status = false;
        }
        $this->renderJSON($data, $status, $msg, $meta);

    }
    function save_images(){
        $this->check_auth();
        $data = array();
        $msg = null;
        $status = true;
        $meta = array();
        try{
            $product_id = $this->input->post('id', true);
            $download_id = $this->input->post('download_id', true);
            $file_name = $this->input->post('file_name', true);
            $check_product_exists = $this->product_model->get($product_id);
            if(!$check_product_exists) throw new Exception('Product is not exists. Please re-add');
            $image_data = json_encode(array('download_id'=>$download_id, 'file_name'=>$file_name));
            $res = $this->product_model->update( $product_id, array('images'=>$image_data));
            if(!$res) throw new Exception("Error. Please try again latter.");
            $msg = 'Image is saved!';
        } catch(Exception $e){
            $msg = $e->getMessage();
            $status = false;
        }
        $this->renderJSON($data, $status, $msg, $meta);
    }

    function translate(){
        $source =  $this->input->get('source', true);
        $target =  $this->input->get('target', true);
        $q =  $this->input->get('q');
        $url = 'https://translation.googleapis.com/language/translate/v2?q='.urlencode($q).'&target='.$target.'&sl='.$source.'&key=AIzaSyBQZNSPqKPwELrkyxohME5yAH0pqW_NeK4';
        $returned_content = $this->get_data($url);
        echo $returned_content;
    }
    function get_data($url) {
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
}
