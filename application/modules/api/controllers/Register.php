<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Register extends Api_Controller{
    public function __construct(){
        parent::__construct();
		$this->load->model('auth/user_model');
    }
    public function index(){
        $data = array();
        $msg = null;
        $status = true;
        $meta = array();
        try{
            $name = $this->input->post('name', true);
            $phone = $this->input->post('phone', true);
            $email = $this->input->post('email', true);
            $password = $this->input->post('password', true);
            $re_password = $this->input->post('re_password', true);
            if(!$name) throw new Exception('Full name is required');
            if(!$email) throw new Exception('Email is required');
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) throw new Exception('Email is not valid');
            if(!$password) throw new Exception('Password is required');
            if($re_password!=$password) throw new Exception('Retype password is incorrect');

            $sex = $this->input->post('sex', true);
            $sex = $sex ? $sex : 1;

            $user_data = $this->user_model->find_by('email', $email);
            if($user_data) throw new Exception('Email is used');

            //$expired = date('Y-m-d H:i:s');
            $expired = date('Y-m-d H:i:s');
            if( time()< strtotime('2017-10-16 :00:00:00')){
                $expired = '2017-10-15 :23:59:59';
            }

            $data = array(
                'name' => $name,
                'phone' => $phone,
                'email' => $email,
                'username' => $email,
                'password' => md5($password),
                'role' => 2,
                'expired' => $expired,
                'sex' => $sex,
                'status' => 0,
                'country' => $this->input->post('country')
            );
            $res = $this->user_model->add($data);
            if(!$res) throw new Exception('Error. Please try again latter.');
            $user_id = $res;
            //$res = $this->sendEmailActivate($email, $user_data);
            $res = $this->call_api_email_activate($email, $user_id);
            if(is_array($res) && isset($res['success']) && $res['success']==false){
                throw new Exception($res['message']);
            }
            $msg = 'Register success. Please check your inbox to activate account.';
        } catch(Exception $e){
            $msg = $e->getMessage();
            $status = false;
        }
        $this->renderJSON($data, $status, $msg, $meta);
    }
    /* tool dropship call api to CRM */
    private function call_api_email_activate($email, $user_id){
        //$url = 'https://crm.tooldropship.com/api/register/api_email_activate';
        $url = 'https://tooldropship.com/api/register/api_email_activate';
        $data = 'client_email='.urlencode($email).'&user_id='.$user_id;
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
    //CRM api send email activate
    public function api_email_activate(){
        $client_email = $this->input->post('client_email');
        $user_id = $this->input->post('user_id');
        $user_data = $this->user_model->get($user_id);
        $res = $this->sendEmailActivate($client_email, $user_data);
        echo $res;
        exit();
    }
    private function sendEmailActivate($client_email, $user_data){
        $data = array('user_data' => $user_data);
        $this->load->model('setting/setting_model');
        $config_data = $this->setting_model->find_by('config_key', 'google_mail');
        if(!$config_data) return $this->message('Error. Please try again latter.');
        if(!$config_data->config_value) return $this->message('Error. Please try again latter.');
        $config_email = json_decode($config_data->config_value);
        if(empty($config_email)) return $this->message('Error. Please try again latter.');
        $base_config = $this->setting_model->find_by('config_key', 'base_config');
        $base_config = $base_config ? json_decode($base_config->config_value) : null;
        $data['base_config'] = $base_config;
        $data['reset_link'] = $this->generate_link($user_data);
        $config = array(
            'protocol' => $config_email->protocol,
            'smtp_host' => $config_email->smtp_host,
            'smtp_port' => $config_email->smtp_port,
            'smtp_user' => $config_email->smtp_user,
            'smtp_pass' => $config_email->smtp_pass,
            'mailtype'  => $config_email->mailtype,
            'charset'   => $config_email->charset
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");

        $this->email->from($config_email->smtp_user, $config_email->sender);
        $this->email->to($client_email);
        $this->email->cc($base_config->site_email);
        $this->email->cc($config_email->recipients);
        $this->email->subject('[E-Drop] Active Account for user '.$user_data->username);
        $this->email->message( $this->load->view('api/activate-account.php', $data, true));

        $res = $this->email->send();
        if(!$res){
            $error = $this->email->print_debugger();
            return $this->message($error);
        }
        return true;
    }
    private function generate_link($user_data){
        if(!$user_data->reset_token){
            $user_data->reset_token = $this->generate_token();
            $this->user_model->update($user_data->id, array('reset_token'=>$user_data->reset_token));
        }
        return 'https://tooldropship.com/auth/activate?token='.$user_data->reset_token;
    }


    public function ksearch(){
        $data = array();
        $msg = null;
        $status = true;
        $meta = array();
        try{
            $name = $this->input->post('name', true);
            $phone = $this->input->post('phone', true);
            $email = $this->input->post('email', true);
            $password = $this->input->post('password', true);
            $re_password = $this->input->post('re_password', true);
            if(!$name) throw new Exception('Full name is required');
            if(!$email) throw new Exception('Email is required');
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) throw new Exception('Email is not valid');
            if(!$password) throw new Exception('Password is required');
            if($re_password!=$password) throw new Exception('Retype password is incorrect');

            $sex = $this->input->post('sex', true);
            $sex = $sex ? $sex : -1;

            $user_data = $this->user_model->find_by('email', $email);
            if($user_data) throw new Exception('Email is used');

            //$expired = date('Y-m-d H:i:s');
            $expired = date('Y-m-d H:i:s');
            if( time()< strtotime('2017-10-16 :00:00:00')){
                $expired = '2017-10-15 :23:59:59';
            }

            $data = array(
                'name' => $name,
                'phone' => $phone,
                'email' => $email,
                'username' => $email,
                'password' => md5($password),
                'role' => 2,
                'expired' => $expired,
                'sex' => $sex,
                'status' => 0,
                'country' => $this->input->post('country'),
                'type' => 1//type = 1: find keyword extension
            );
            $res = $this->user_model->add($data);
            if(!$res) throw new Exception('Error. Please try again latter.');
            $user_id = $res;
            $user_data = $this->user_model->get($user_id);
            $user_data->secret_key = $this->generateRandomString($user_data, 6);
            $res = $this->ks_sendEmailActivate($email, $user_data);
            if(is_array($res) && isset($res['success']) && $res['success']==false){
                throw new Exception($res['message']);
            }
            $msg = 'Register success. Please check your inbox and get activate code.';
        } catch(Exception $e){
            $msg = $e->getMessage();
            $status = false;
        }
        $this->renderJSON($data, $status, $msg, $meta);
    }
    private function ks_sendEmailActivate($client_email, $user_data){
        $data = array('user_data' => $user_data);
        $this->load->model('setting/setting_model');
        $config_data = $this->setting_model->find_by('config_key', 'google_mail');
        if(!$config_data) return $this->message('Error. Please try again latter.');
        if(!$config_data->config_value) return $this->message('Error. Please try again latter.');
        $config_email = json_decode($config_data->config_value);
        if(empty($config_email)) return $this->message('Error. Please try again latter.');
        $base_config = $this->setting_model->find_by('config_key', 'base_config');
        $base_config = $base_config ? json_decode($base_config->config_value) : null;
        $data['base_config'] = $base_config;
        $config = array(
            'protocol' => $config_email->protocol,
            'smtp_host' => $config_email->smtp_host,
            'smtp_port' => $config_email->smtp_port,
            'smtp_user' => $config_email->smtp_user,
            'smtp_pass' => $config_email->smtp_pass,
            'mailtype'  => $config_email->mailtype,
            'charset'   => $config_email->charset
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");

        $this->email->from($config_email->smtp_user, $config_email->sender);
        $this->email->to($client_email);
        $this->email->cc($base_config->site_email);
        $this->email->cc($config_email->recipients);
        $this->email->subject('[E-Drop] Active Account for user '.$user_data->username);
        $this->email->message( $this->load->view('api/activate-account-code.php', $data, true));

        $res = $this->email->send();
        if(!$res){
            $error = $this->email->print_debugger();
            return $this->message($error);
        }
        return true;
    }

    private function generateRandomString($user_data, $length = 6) {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        $check_exists = $this->user_model->find_by('reset_token', $randomString);
        while($check_exists){
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            $check_exists = $this->user_model->find_by('reset_token', $randomString);
        }
        $this->user_model->update($user_data->id, array('reset_token'=>$randomString));
        return $randomString;
    }

    function kactive(){
        $data = array();
        $msg = null;
        $status = true;
        $meta = array();
        try{
            $secret_key = $this->input->post('secret_key', true);
            $check_exists = $this->user_model->find_by('reset_token', $secret_key);
            if(!$check_exists){
                throw new Exception('Your code is invalid');
            }
            $data = (array)$check_exists;
            $this->user_model->update($data['id'], array('status'=>1, 'reset_token'=>''));

            $data['token'] = $this->generate_token();
            $this->save_token($data);
            $this->tracking($data['id'], 0, 'logged');
            $msg = 'Your account is activated';
        } catch(Exception $e){
            $msg = $e->getMessage();
            $status = false;
        }
        $this->renderJSON($data, $status, $msg, $meta);
    }

    private function save_token($data){
        $this->load->model('token_model');
        $this->token_model->add(array(
            'uid' => $data['id'],
            'name' => $data['name'],
            'username' => $data['username'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'avatar' => $data['avatar'],
            'token' => $data['token']
        ));
        return $data;
    }
}
