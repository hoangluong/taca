<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Forgot extends Api_Controller{
    public function __construct(){
        parent::__construct();
		$this->load->model('auth/user_model');
    }
    function index(){
        $data = array();
        $msg = null;
        $status = true;
        $meta = array();
        try{
            $email = $this->input->post('email', true);
            if(!$email) throw new Exception('Bạn chưa nhập email');
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) throw new Exception('Email is not valid');

            $user_data = $this->user_model->find_by('email', $email);
            if(!$user_data) throw new Exception('Email is not valid.');
            $res = $this->sendEmailForgot( $email, $user_data);
            if(is_array($res) && isset($res['success']) && $res['success']==false){
                throw new Exception($res['message']);
            }
            $msg = 'Please check your email to change password!';
			$this->tracking($user_data->id, 0, 'forgot');
	    } catch(Exception $e){
            $msg = $e->getMessage();
            $status = false;
        }
        $this->renderJSON($data, $status, $msg, $meta);
    }
    private function sendEmailForgot($client_email, $user_data){
        $data = array('user_data' => $user_data);
        $this->load->model('setting/setting_model');
        $config_data = $this->setting_model->find_by('config_key', 'email');
        if(!$config_data) return $this->message('Error. Please try again latter.');
        if(!$config_data->config_value) return $this->message('Error. Please try again latter.');
        $config_email = json_decode($config_data->config_value);
        if(empty($config_email)) return $this->message('Error. Please try again latter.');
        $base_config = $this->setting_model->find_by('config_key', 'base_config');
        $base_config = $base_config ? json_decode($base_config->config_value) : null;
        $data['base_config'] = $base_config;
        $data['reset_link'] = $this->generate_link($user_data);
        $config = array(
            'protocol' => $config_email->protocol,
            'smtp_host' => $config_email->smtp_host,
            'smtp_port' => $config_email->smtp_port,
            'smtp_user' => $config_email->smtp_user,
            'smtp_pass' => $config_email->smtp_pass,
            'mailtype'  => $config_email->mailtype,
            'charset'   => $config_email->charset
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");

        $this->email->from($config_email->smtp_user, $config_email->sender);
        $this->email->to($client_email);
        $this->email->cc($base_config->site_email);
        $this->email->subject('[E-Drop] Request reset password for user '.$user_data->username);
        $this->email->message( $this->load->view('api/reset-password.php', $data, true));

        $res = $this->email->send();
        if(!$res){
            $error = $this->email->print_debugger();
            return $this->message($error);
        }
        return true;
    }
    private function generate_link($user_data){
        if(!$user_data->reset_token){
            $user_data->reset_token = $this->generate_token();
            $this->user_model->update($user_data->id, array('reset_token'=>$user_data->reset_token));
        }
        return base_url('auth/reset-password?token='.$user_data->reset_token);
    }
}