<div class="box-body table-responsive no-padding">
    <table class="table table-hover" id="table_order_list" frameBorder="0" cellpadding="0">
        <thead>
        <tr class="tr_head">
            <th>O/E ID</th>
            <th>Image</th>
            <th>Buyer</th>
            <th style="width:100px;">AliExpress</th>
            <th>Qty</th>
            <th>Date</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php if(isset($data) && $data){
            foreach($data as $item){
                $buyer = json_decode($item->buyer_ebay);?>
                <tr>
                    <td>
                        <a data-id="<?php echo $item->id;?>" class="order_ebay" href="javascript:void(0)">145</a>
                    </td>
                    <td><a href="#"><img src="<?php echo $item->image;?>"></a></td>
                    <td>
                        <a data-id="<?php echo $item->id;?>" class="buyer_ebay" href="#"><?php echo $buyer->buyercontactname;?></a>
                        <script type="text/html" class="data-item"><?php echo $item->buyer_ebay;?></script>
                    </td>
                    <td style="text-align:left;">
                        <div style="float:left;margin:0 5px 0 0;">
                            <a style="display:block;width:32px;" class="link" target="_blank" href="<?php echo $item->ali_link;?>" title="View product on AliExpress"><img style="max-width:100%;" src="/icons/ali.png"></a>
                        </div>
                        <div style="float:left;margin:0;">
                            <a style="display:block;" data-id="<?php echo $item->id;?>" class="add-order-id" href="#" title="Add/edit OrderID on Ali"><i class="icon ion-pricetag"></i></a>
                        </div>
                        <?php if($item->tracking_number){?>
                            <div style="clear: both;float:left;margin:0 5px 0 0;">
                                <a style="display:block;color:#204d74;" class="link" href="https://track.aftership.com/china-ems/<?php echo $item->tracking_number;?>location=yes,height=570,width=520,scrollbars=yes,status=yes" title="View tracking"><i class="icon ion-social-vimeo"></i></a>
                            </div>
                            <div style="float:left;margin:0 5px 0 0;">
                                <a style="display:block;color:#f0ad4e;" data-id="<?php echo $item->id;?>" class="add-tracking" href="#" title="Edit tracking"><i class="icon ion-edit"></i></a>
                            </div>
                        <?php }else{?>
                            <div style="float:left;margin:0 5px 0 0;">
                                <a style="display:block;" data-id="<?php echo $item->id;?>" class="add-tracking" href="#" title="Add tracking"><i class="icon ion-edit"></i></a>
                            </div>
                        <?php }?>
                    </td>
                    <td><?php echo $item->qty;?></td>
                    <td><?php echo date('H:i:s d/m/Y', strtotime($item->created_on));?></td>
                    <td>
                        <div><a data-id="<?php echo $item->id;?>" title="Send mail by template" class="send-mail icon ion-ios-email" href="#"></a></div>
                        <div><a data-id="<?php echo $item->id;?>" title="Remove this order" class="remove-order icon ion-android-remove-circle" href="#"></a></div>
                    </td>
                </tr>
            <?php }
        }else{?>
            <tr>
                <td colspan="7">Not found order.</td>
            </tr>
        <?php }?>
        </tbody>
    </table>
</div>