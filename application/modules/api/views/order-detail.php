<?php if (isset($data) && $data && $data->buyer_ebay) {
    $buyer = json_decode($data->buyer_ebay);
    $transaction_data = $buyer->transaction_data;?>
    <form class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-xs-4" for="orderebayid">Order ebay id:</label>

            <div class="col-xs-8">
                <input id="orderebayid" value="<?php echo $buyer->orderebayid; ?>" type="text" readonly
                       class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-4" for="item_id">Product ID:</label>

            <div class="col-xs-8">
                <input id="item_id" value="<?php echo $transaction_data->item_id; ?>" type="text" readonly
                       class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-4" for="item_name">Product name:</label>

            <div class="col-xs-8">
                <div class="form-control" readonly><?php echo $transaction_data->item_name; ?></div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-4" for="price">Product price:</label>

            <div class="col-xs-8">
                <input id="price" value="<?php echo $transaction_data->price; ?>" type="text" readonly
                       class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-4" for="qty">Quantity:</label>

            <div class="col-xs-8">
                <input id="qty" value="<?php echo $transaction_data->qty; ?>" type="text" readonly
                       class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-4">Ảnh:</label>

            <div class="col-xs-8">
                <img src="<?php echo $transaction_data->pic;?>" style="width:100px;height:auto;max-width:100%;">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-4" for="subtotal">Sub total:</label>

            <div class="col-xs-8">
                <input id="subtotal" value="<?php echo $transaction_data->subtotal; ?>" type="text" readonly
                       class="form-control">
            </div>
        </div>
    </form>
<?php }else{?>
    <h3>Not found data.</h3>
<?php } ?>