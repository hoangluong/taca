<!doctype html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
<table style="background-color:#fff;margin:0;padding:0" width="700" cellpadding="0" cellspacing="0" border="0">
    <tbody>
    <tr>
        <td style="vertical-align:top;width:683px;overflow:hidden;border:0;margin:0;padding:0">
            <p style="padding:0;margin:0;text-align:left;font-family:'Times New Roman',Times,serif;font-size:10px;margin:0;line-height:1.8;color:#333333">
                &nbsp;
            </p>
            <table width="683" cellpadding="0" cellspacing="0" border="0">
                <tbody>
                <tr>
                    <td style="width:20px;overflow:hidden;padding:0;margin:0" width="20">&nbsp;</td>
                    <td style="width:643px;overflow:hidden;padding:0;margin:0" width="643">
                        <table width="643" cellpadding="0" cellspacing="0" border="0"
                               style="background:#ffffff url('https://ci6.googleusercontent.com/proxy/D3st2HHvGxooYoBCkqzM-haMex-4Jurusnd_Alvc6xPO9nKE3cAbmLqtFzbBcxoRknGtsBHeuIW_xJOdOrKoEnafRrs-RjD9Yhnu4Af-J94h=s0-d-e1-ft#https://sale.admicro.vn/images/email/v3/vietnam/titlebg.gif') repeat-x left top">
                            <tbody>
                            <tr>
                                <td style="vertical-align:top;border:0;margin:0;padding:0;width:643px;overflow:hidden">
                                    <p style="padding:0;margin:0;text-align:left;font-family:'Times New Roman',Times,serif;margin:0;line-height:1.8;color:#111111;overflow:hidden;background:#ffffff;font-size:14px;font-weight:bold;float:left">
                                        ACCOUNT ACTIVATION REQUIRED
                                    </p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <br>

                        <p style="padding:0;margin:0;text-align:left;font-family:'Times New Roman',Times,serif;font-size:14px;margin:0;line-height:1.8;color:#333333">
                            Hi <?php echo $user_data->name;?>, thanks for joining EbayDropshipTool.com <br>

                            Please click on the following link to verify your email address:
                            Please use this secret code for active your account: <br>
                            <span style="font-size: 18px;display:inline-block;"><?php echo $user_data->secret_key;?></span><br>
                            Please, skip this email if this account is not your account.
                        </p>
                        <br>
                        <br>
                        <table width="643" cellpadding="0" cellspacing="0" border="0">
                            <tbody>
                            <tr>
                                <td style="vertical-align:top;border:0;margin:0;padding:0;width:558px;overflow:hidden">
                                    <p style="padding:0;margin:0;text-align:left;font-family:'Times New Roman',Times,serif;font-size:14px;margin:0;line-height:1.8;color:#111111;font-weight:bold">
                                        Welcome!
                                    </p>
                                </td>
                                <td style="vertical-align:top;border:0;margin:0;padding:0;width:85px;overflow:hidden">
                                    &nbsp;
                                </td>
                            </tr>
                            </tbody>
                        </table>

                    </td>
                    <td style="width:20px;overflow:hidden;padding:0;margin:0" width="20">&nbsp;</td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>
