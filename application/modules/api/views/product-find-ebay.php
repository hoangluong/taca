<?php if(isset($data) && $data){
	?>
	<div style="display:block;">
    <?php foreach($data as $item){?>
	<div class="clearfix product-item">
    <div class="serptablecell1">
        <a class="imagelinks" target="_blank" rel="nofollow" href=""><img itemprop="image" alt="" title="Click to see more photos at eBay..." class="gallery" style="" src="<?php echo $item['imageURL'] ?>"></a>
    </div>
    <div class="serptablecell2">
        <div class="serptablebasestyle">
           
			<div class="serptablebasestyle">
				<span class="labwcc" title="How many eBay users have clicked 'Watch This Item'">&nbsp;Watch:&nbsp;</span><span class="valwcc" title="How many eBay users have clicked 'Add to Watch List'"><span ><?php echo $item['watchCount']; ?></span>&nbsp;watchers&nbsp;</span>
			</div>
	  
            <div class="valtitle lovewrap padr4">
                <a class="product-item-links" title="Click to see this item..." target="_blank" rel="nofollow" href="<?php echo $item['viewItemURL']; ?>" itemprop="url"><span itemprop="name"><?php echo $item['title']; ?></span></a>
            </div>
            <div class="midspacing"></div>
				<span class="labprice">Current Bid/Price (<?php  echo $item['buyItNowPrice']['@currencyId']; ?>):&nbsp;&nbsp;</span>
				<span ><span itemprop="price" class="valprice"><?php  echo $item['buyItNowPrice']['__value__']; ?>  </span></span>
				<br>
				<span class="labfreeship">Type shipping:<?php echo $item['shippingType']; ?></span><br>
                <div class="serptablebasestyle">
                    <span class="labtime">Time Left:&nbsp;</span>
                    <span class="valtime"><?php echo $item['timeLeft']; ?></span>
                </div>
        </div>
    </div>
	</div>
	
    <?php } ?>
</div>
<?php }?>
