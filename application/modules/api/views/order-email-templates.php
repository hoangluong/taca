<div id="tool_dropship" class="tool_dropship-panel tool_dropship-panel-import" style="">
    <div class="tool_dropship-panel-header">
        <a href="#" id="tool_dropship-close-popup" class="close"><img src="icons/icon-close.png"></a>
    </div>
    <div id="tool_dropship-panel-import-content" class="tool_dropship-panel-import-content">
        <form class="form-horizontal form-send-email">
            <input name="order_id" value="<?php echo $order_id;?>" type="hidden">
            <select style="width:100%">
                <option value="0">Choose template email</option>
                <?php if(isset($data) && $data){
                    foreach($data as $item){?>
                        <option value="<?php echo $item->id;?>"><?php echo $item->template_name;?></option>
                    <?php }
                }?>
            </select>
        </form>
    </div>
</div>