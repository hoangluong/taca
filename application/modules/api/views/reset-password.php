<!doctype html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
<table
        style="background:#ffbf40 url('https://tooldropship.com/assets/images/logo.png') no-repeat left center/auto 90%;"
        width="700" cellpadding="0" cellspacing="0" border="0">
    <tbody>
    <tr>
        <td style="height:70px;width:178px;overflow:hidden;border:0;padding:0;margin:0">
            <a href="https://tooldropship.com/" style="outline:none;padding:0;margin:0" target="_blank"></a>
        </td>
        <td style="vertical-align:top;border:0;margin:0;padding:0;width:522px;overflow:hidden">
            <p style="padding:0;margin:0;text-align:left;font-family:Arial,Helvetica,sans-serif;font-size:6px;margin:0;line-height:1.5;color:#ffffff">
                &nbsp;
            </p>
            <table width="522" cellpadding="0" cellspacing="0" border="0">
                <tbody>
                <tr>
                    <td style="vertical-align:top;padding:0;margin:0;border:0;width:460px">
                        <p style="padding:0;margin:0;text-align:left;font-family:Arial,Helvetica,sans-serif;font-size:12px;margin:0;line-height:1.5;color:#ffffff">
                            <b>Điện thoại:</b> <?php echo isset($base_config->site_phone) ? $base_config->site_phone : '';?></p>

                        <p style="padding:0;margin:0;text-align:left;font-family:Arial,Helvetica,sans-serif;font-size:12px;margin:0;line-height:1.5;color:#ffffff">
                            <?php if(isset($base_config->site_email)){?>
                                <b>Email:</b>
                                <a style="text-decoration:none!important;color:#fff!important"
                                   href="mailto:<?php echo $base_config->site_email;?>" target="_blank">
                                    <span style="color:#ffffff!important"><?php echo $base_config->site_email;?></span>
                                </a>
                            <?php }?>
                            <?php if(isset($base_config->site_skype)){?>
                            <b>Skype:</b>
                            <span style="color:#ffffff!important"><?php echo $base_config->site_skype;?></span>
                            <?php }?>
                        </p>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<table style="background-color:#fff;margin:0;padding:0" width="700" cellpadding="0" cellspacing="0" border="0">
    <tbody>
    <tr>
        <td style="vertical-align:top;width:683px;overflow:hidden;border:0;margin:0;padding:0">
            <p style="padding:0;margin:0;text-align:left;font-family:'Times New Roman',Times,serif;font-size:10px;margin:0;line-height:1.8;color:#333333">
                &nbsp;
            </p>
            <table width="683" cellpadding="0" cellspacing="0" border="0">
                <tbody>
                <tr>
                    <td style="width:20px;overflow:hidden;padding:0;margin:0" width="20">&nbsp;</td>
                    <td style="width:643px;overflow:hidden;padding:0;margin:0" width="643">
                        <table width="643" cellpadding="0" cellspacing="0" border="0"
                               style="background:#ffffff url(https://ci6.googleusercontent.com/proxy/D3st2HHvGxooYoBCkqzM-haMex-4Jurusnd_Alvc6xPO9nKE3cAbmLqtFzbBcxoRknGtsBHeuIW_xJOdOrKoEnafRrs-RjD9Yhnu4Af-J94h=s0-d-e1-ft#https://sale.admicro.vn/images/email/v3/vietnam/titlebg.gif) repeat-x left top">
                            <tbody>
                            <tr>
                                <td style="vertical-align:top;border:0;margin:0;padding:0;width:643px;overflow:hidden">
                                    <p style="padding:0;margin:0;text-align:left;font-family:'Times New Roman',Times,serif;margin:0;line-height:1.8;color:#111111;overflow:hidden;background:#ffffff;font-size:14px;font-weight:bold;float:left">
                                        YÊU CẦU MẬT KHẨU MỚI
                                    </p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <br>

                        <p style="padding:0;margin:0;text-align:left;font-family:'Times New Roman',Times,serif;font-size:14px;margin:0;line-height:1.8;color:#333333">
                            Dear <?php echo $user_data->name;?>, <br>
                            Bạn hoặc ai đó đã sử dụng email này để yêu cầu đặt lại mật khẩu cho tài khoản
                            [<?php echo $user_data->username;?>].<br>
                            Click vào link dưới đây để thực hiện đặt lại mật khẩu:<br>
                            <a href="<?php echo isset($reset_link) ? $reset_link : 'javascript:void(0)';?>" target="_blank">Đặt
                                lại mật khẩu</a><br>
                            Nếu không muốn đặt lại mật khẩu vui lòng bỏ qua thư này.
                        </p>
                        <br>
                        <br>
                        <table width="643" cellpadding="0" cellspacing="0" border="0">
                            <tbody>
                            <tr>
                                <td style="vertical-align:top;border:0;margin:0;padding:0;width:558px;overflow:hidden">
                                    <p style="padding:0;margin:0;text-align:left;font-family:'Times New Roman',Times,serif;font-size:14px;margin:0;line-height:1.8;color:#111111;font-weight:bold">
                                        Trân trọng!
                                    </p>
                                </td>
                                <td style="vertical-align:top;border:0;margin:0;padding:0;width:85px;overflow:hidden">
                                    &nbsp;
                                </td>
                            </tr>
                            </tbody>
                        </table>

                    </td>
                    <td style="width:20px;overflow:hidden;padding:0;margin:0" width="20">&nbsp;</td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>