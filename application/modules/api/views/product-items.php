<?php if(isset($data) && $data){
    foreach($data as $item){?>
        <tr class="tr_head">
            <td style="width:50px;">
                <span title="<?php echo $item->product_id;?>"><?php echo $item->product_id;?></span>
            </td>
            <td>
                <a class="link" href="<?php echo base_url('product/edit/'.$item->id);?>" title="<?php echo $item->name;?>"><?php echo word_limiter($item->name,8);?></a>
            </td>
             <td><?php if(!empty( $item->images_thumb)){
                ?>
                <img src="<?php echo $item->images_thumb;?>"/>
                <?php } ?></td>
            <td><?php echo $item->price;?></td>
            <td><?php if($item->images){
                    $image = json_decode($item->images);?>
                    <a href="#" class="show-on-folder icon ion-folder" data-id="<?php echo $image->download_id;?>"></a>
                <?php }?></td>
            <td>
                <a href="<?php echo $item->source_link;?>" data-id="<?php echo $item->id;?>" class="link icon ion-eye" title="Detail product"></a><br/>
                <a href="#" data-id="<?php echo $item->id;?>" class="paste-product icon ion-checkmark-circled" title="Paste product to ebay"></a><br/>
				<a href="#" data-id="<?php echo $item->id;?>" class="remove-product icon ion-android-remove-circle" title="Remove product"></a><br/>
            </td>
        </tr>
    <?php }
}else{?>
    <tr>
        <td colspan="6">Not found product.</td>
    </tr>
<?php }?>
