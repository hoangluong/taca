<?php if (isset($data) && $data && $data->buyer_ebay) {
    $buyer = json_decode($data->buyer_ebay);
    $transaction_data = $buyer->transaction_data;?>
    <form class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-xs-4" for="buyercontactname">Name:</label>

            <div class="col-xs-8">
                <input id="buyercontactname" value="<?php echo $buyer->buyercontactname; ?>" type="text" readonly
                       class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-4" for="buyeraddress1">Address 1:</label>

            <div class="col-xs-8">
                <input id="buyeraddress1" value="<?php echo $buyer->buyeraddress1; ?>" type="text" readonly
                       class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-4" for="buyeraddress2">Address 2:</label>

            <div class="col-xs-8">
                <input id="buyeraddress2" value="<?php echo $buyer->buyeraddress2; ?>" type="text" readonly
                       class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-4" for="state">State:</label>

            <div class="col-xs-8">
                <input id="state" value="<?php echo $buyer->state; ?>" type="text" readonly class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-4" for="buyercity">City:</label>

            <div class="col-xs-8">
                <input id="buyercity" value="<?php echo $buyer->buyercity; ?>" type="text" readonly
                       class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-4" for="buyerzip">Zip code:</label>

            <div class="col-xs-8">
                <input id="buyerzip" value="<?php echo $buyer->buyerzip; ?>" type="text" readonly class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-4" for="buyercountry">Country:</label>

            <div class="col-xs-8">
                <input id="buyercountry" value="<?php echo $buyer->buyercountry; ?>" type="text" readonly
                       class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-4" for="email">Email:</label>

            <div class="col-xs-8">
                <input id="email" value="<?php echo $buyer->email; ?>" type="text" readonly class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-4" for="phone">Phone:</label>

            <div class="col-xs-8">
                <input id="phone" value="<?php echo $buyer->phone; ?>" type="text" readonly class="form-control">
            </div>
        </div>
    </form>
<?php }else{?>
    <h3>Not found data.</h3>
<?php } ?>