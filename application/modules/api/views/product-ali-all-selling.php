<?php if (isset($data) && $data) {
    ?>
    <div style="display:block;">
        <?php foreach ($data as $index=>$item) { ?>
            <div class="clearfix product-item">
                <div class="serptablecell1">
                    <a class="imagelinks" target="_blank" rel="nofollow" href="">
                        <img itemprop="image" alt=""
                             title="Click to see more photos at eBay..."
                             class="gallery" style=""
                             src="<?php
                             echo strpos($item['image200Path'], 'http') === false ? 'https:' . $item['image200Path'] : $item['image200Path']; ?>"></a>
                </div>
                <div class="serptablecell2">
                    <div class="serptablebasestyle">

                        <div class="serptablebasestyle" style="background-color:#f1f1f1;padding:0;line-height:27px;">
                            <span class="labwcc" style="color:#fff;background-color:#e62e04;text-align:center;margin-right:5px;display:inline-block;width:27px;height:27px;border-radius:0;">&nbsp;<?php echo $index+1;?></span><span
                                class="valwcc"
                                title="Sale in six month" style="color:#e62e04;background-color:transparent;"><span><?php echo $item['saleInSixMonth']; ?></span>&nbsp;orders&nbsp;</span>
                        </div>

                        <div class="valtitle lovewrap padr4">
                            <a class="product-item-links" title="Click to see this item..." target="_blank"
                               rel="nofollow" href="<?php echo $item['productUrl']; ?>" itemprop="url"><span
                                    itemprop="name"><?php echo $item['subject']; ?></span></a>
                        </div>
                        <div class="midspacing"></div>
                        <span class="labprice">Price (<?php echo $item['currencyType']; ?>):&nbsp;&nbsp;</span>
                        <span><span itemprop="price"
                                    class="valprice"><?php echo $item['lowerPrice']; ?>  </span></span>
                        <br>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
<?php }else{?>
    <div>
        <h3>Not found product.</h3>
    </div>
<?php } ?>
