<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_model extends Base_model
{
    protected $table = 'product';
    function __construct(){
        parent::__construct();
    }

    function saved_product_daily($start_date, $end_date){
        $this->db->select('`user`.`id`,`user`.`name`,`user`.`username`, count(`log`.`id`) as `total_saved`');
        $this->db->join('log', '`user`.`id`=`log`.`uid`');
        $this->db->where('`log`.`action`', 'saved_product');
        $this->db->where('`log`.`created_on` >=', $start_date.' 00:00:00');
        $this->db->where('`log`.`created_on` <=', $end_date.' 23:59:59');

        $this->db->group_by('log.uid');
        $this->db->order_by('total_saved', 'desc');

        $result = $this->db->get('user')->result();
        if(!$result) return 0;
        return $result;
    }

    function product_saved($start_date, $end_date){
        $this->db->select('`user`.`id`,`user`.`name`,`user`.`username`, count(`product`.`id`) as `total_saved`');
        $this->db->join('product', '`user`.`id`=`product`.`uid`');
        $this->db->where('`product`.`created_on` >=', $start_date.' 00:00:00');
        $this->db->where('`product`.`created_on` <=', $end_date.' 23:59:59');

        $this->db->group_by('product.uid');
        $this->db->order_by('total_saved', 'desc');

        $result = $this->db->get('user')->result();
        if(!$result) return 0;
        return $result;
    }

    function user_limited_product(){
        /*$query = $this->db->query('select t1.id,t1.name,t1.username,t1.email, count(t2.id) as total_saved,`t1`.username,t1.email from product as t2, `user` as t1 where t1.id=`t2`.uid group by t2.uid order by total_saved desc');
        $result = $query->result();
        return $result;*/
        $this->db->select('t1.`id`,t1.`name`,t1.`username`,count(t2.id) as total_saved');
        $this->db->from('`user` as t1, `product` as t2');
        $this->db->where('t1.id=t2.uid');
        $this->db->group_by('t2.uid');
        $this->db->order_by('total_saved', 'desc');
        //$this->db->where('total_saved >=', 20);
        $this->db->where('t1.expired <=', date('Y-m-d H:i:s'));
        $result = $this->db->get()->result();

        if(!$result) return 0;
        return $result;
    }
}