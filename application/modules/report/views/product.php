<?php
$start_date = $this->input->get('start_date');
$end_date = $this->input->get('end_date');
$start_date = $start_date ? $start_date : date('Y-m-d');
$end_date = $end_date ? $end_date : date('Y-m-d');
?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box">
            <div class="box-header">
                <form action="" method="get" class="primary" id="form_filter">
                    <div class="col-sm-12 col-md-8">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="action">
                                    <?php $action = $this->input->get('action', true);?>
                                    <select name="action" id="action" class="form-control select2" placeholder="Select a action">
                                        <option <?php if($action=='saved_product_log'){echo 'selected';}?> value="saved_product_log">Saved product log</option>
                                        <option <?php if($action=='saved_product_real'){echo 'selected';}?> value="saved_product_real">Saved product real</option>
                                        <option <?php if($action=='user_limited_product'){echo 'selected';}?> value="user_limited_product">User limited product</option>
                                    </select>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>
                                    <input class="form-control date-picker" id="start_date" type="text" value="<?php echo htmlentities($start_date)?>" name="start_date" placeholder="From time">
                                </label>
                            </div>
                        </div>
                        <div style="width:25px;float:left;text-align: center;font-size:25px"><i class="fa fa-angle-double-right"></i></div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>
                                    <input class="form-control date-picker" id="end_date" type="text" value="<?php echo htmlentities($end_date)?>" name="end_date" placeholder="To time">
                                </label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="box-body">
                <table class="table">
                    <tbody>
                    <tr>
                        <th style="width: 20px">TT</th>
                        <th>User ID</th>
                        <th>Họ tên</th>
                        <th>Tên đăng nhập</th>
                        <th>Số lần save product</th>
                    </tr>
                    <?php if(isset($data) && !empty($data)){
                        foreach($data as $index=>$item){?>
                            <tr>
                                <td><?php echo $index+1;?></td>
                                <td><a href="<?php echo base_url('user/edit/'.$item->id);?>" target="_blank"><?php echo $item->id;?></a></td>
                                <td><?php echo $item->name;?></td>
                                <td><?php echo $item->username;?></td>
                                <td><?php echo $item->total_saved;?></td>
                            </tr>
                        <?php }
                    }?>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
<script>
    (function(){

        var initListing = function(){
            $('#action').change(function(){
                $('#form_filter').submit();
            });
            $('#start_date,#end_date').change(function(){
                $('#form_filter').submit();
            });
        };
        $(document).ready(function(){
            initListing();
        });
    }());
</script>
