<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends Admin_Controller{
    function __construct(){
        parent::__construct();
        $user_data = $this->session->userdata('user_data');
        if($user_data['role'] != 1){
            die('Do not permission');
        }
        $this->load->model('report_model');
        $this->load->model('product/product_model');
    }
    function product(){
        $temp = array();
        $start_date = $this->input->get('start_date');
        $start_date = $start_date ? $start_date : date('Y-m-d');
        $end_date = $this->input->get('end_date');
        $end_date = $end_date ? $end_date : date('Y-m-d');


        $action = $this->input->get('action', true);
        $action = $action ? $action : 'saved_product_log';
        if($action == 'saved_product_log'){
            $data = $this->report_model->saved_product_daily($start_date, $end_date);
        }elseif($action == 'saved_product_real'){
            $data = $this->report_model->product_saved($start_date, $end_date);
        }elseif($action == 'user_limited_product'){
            $data = $this->report_model->user_limited_product();
        }else{
            $data = null;
        }
        $temp['data'] = $data;

        $temp['page_title'] = 'Reporting product action';
        $temp['template'] = 'report/product';
        $this->load->view('admin/layout', $temp);
    }

    function order(){
        $this->load->model('order_model');
        $cond = array();
        //$cond['created_on >='] = date('Y-m-d H:i:s', strtotime('-30 days'));
        $data = $this->order_model->find($cond, 1, -1, array());
        $total_money = 0;
        foreach($data as $item){
            $transaction = json_decode($item->transaction_data);
            if(!$transaction) continue;
            $total_money += floatval( str_replace('$', '', $transaction->subtotal) );
        }
        echo $total_money;exit();
    }
}
