<?php
$base_url = base_url();
$public_url = $base_url . 'public/manage/';
?><!doctype html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <title>Browser media</title>
    <link href="<?php echo $public_url . 'font-awesome/css/font-awesome.min.css'; ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $public_url . 'css/common.css'; ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $public_url . 'vcc-finder/style.css'; ?>" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="<?php echo $public_url . 'js/jquery-1.12.4.min.js' ?>"></script>
    <script type="text/javascript" src="<?php echo $public_url . 'vcc-finder/script.js' ?>"></script>
    <script type="text/javascript">
        var base_url = '<?php echo $base_url;?>';
        function getUrlParam(paramName) {
            var reParam = new RegExp('(?:[\?&]|&)' + paramName + '=([^&]+)', 'i');
            var match = window.location.search.match(reParam);
            return (match && match.length > 1) ? match[1] : null;
        }
        function returnFileUrl(fileUrl) {
            var funcNum = getUrlParam('CKEditorFuncNum');
            console.log(funcNum);
            //var fileUrl = '/path/to/file.txt';
            window.opener.CKEDITOR.tools.callFunction(funcNum, fileUrl);
            window.close();
        }

        $(document).ready(function(){
            VccFinder.openFrameSuccess();
            VccFinder.eventListFiles();
            var block = $('#block-finder'),
                btnClose = block.find('.btn-close,.btn-cancel'),
                btnOK = $('.btn-ok');
            btnClose.unbind().click(function (e) {
                e.preventDefault();
                window.close();
                return false;
            });
            btnOK.unbind().click(function (e) {
                e.preventDefault();
                var active = $('#block-files').find('a.active');
                if (active.length > 0) {
                    var urlFile = active.attr("href").replace(base_url, '/');
                    //console.log(typeof(output));
                    active.removeClass('active');
                    returnFileUrl(urlFile);
                }else{
                    window.close();
                }
                return false;
            });
        });
    </script>
</head>
<body class="window-finder">
    <?php $this->load->view('finder/finder');?>
<!--<style>
    .container-finder{
        display: block !important;
        opacity: 1 !important;
    }
</style>-->
</body>
</html>