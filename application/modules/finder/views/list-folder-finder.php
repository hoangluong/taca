<?php
$key_active = isset($key_active) ? intval($key_active) : 0;
function renderItemDir($dirs, $parent, $html='', $key_active=0){
    foreach($dirs as $key=>$item){
        if($item['parent'] != $parent) continue;
        $before = '<li>';
        $cls = '';
        if( $key_active==$key ){
            $cls = ' class="active"';
        }
        $main = '<a data-dir="'.$key.'"'.$cls.' href="'.base_url('finder/render_file?dir='.$key).'">'. basename($item['path']).'</a>';
        $sub = renderItemDir($dirs, $key, '');
        $after = '</li>';
        if($sub){
            $before = '<li class="has-child"><i class="fa fa-plus" aria-hidden="true"></i>';
            $sub = '<ul class="sub-dir">'.$sub.'</ul>';
        }
        $html .= $before . $main . $sub . $after;
    }
    return $html;
}?>
<li>
    <a class="active" href="<?php echo base_url('finder/render_file?dir=0')?>">Uploads</a>
    <ul class="sub-dir" style="display: block;">
        <?php if(isset($dirs) && $dirs) echo renderItemDir($dirs, 0, '', $key_active);?>
    </ul>
</li>
