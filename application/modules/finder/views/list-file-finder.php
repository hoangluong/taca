<?php
$images = isset($images) ? $images : array();
$audios = isset($audios) ? $audios : array();
$videos = isset($videos) ? $videos : array();
if(isset($files) && $files){
    foreach($files as $item){
        if(is_dir($item['url'])){?>
            <li>
            <a data-dir="" href="<?php echo base_url('finder/render_file?dir=');?>">
                <i class="fa fa-folder-open-o" aria-hidden="true"></i>
            </a>
            </li>
        <?php }else {
            ?>
            <li><a href="<?php echo $item['url']; ?>" data-name="<?php echo basename($item['url']); ?>">
                <?php $type = strtolower(pathinfo($item['path'], PATHINFO_EXTENSION));
                if (in_array($type, $images)) {
                    ?>
                    <img alt="<?php echo basename($item['url']); ?>" src="<?php echo $item['url']; ?>">
                <?php } elseif (in_array($type, $audios)) {
                    ?>
                    <i class="fa fa-file-audio-o" aria-hidden="true"></i>
                <?php } elseif (in_array($type, $videos)) {
                    ?>
                    <i class="fa fa-file-video-o" aria-hidden="true"></i>
                <?php } else {
                    ?>
                    <i class="fa fa-file-o" aria-hidden="true"></i>
                <?php } ?>
            </a></li><?php
        }
    }
}?>
