<?php if(isset($total_pages) && isset($paged) && $total_pages>1){?>
    <button class="btn-prew<?php if($paged<=1){echo ' disabled';}?>">
        <i class="fa fa-angle-left" aria-hidden="true"></i>
    </button>
    <input type="text" value="<?php echo $paged;?>" data-total="<?php echo $total_pages;?>">
    <button class="btn-next<?php if($paged>=$total_pages){echo ' disabled';}?>">
        <i class="fa fa-angle-right" aria-hidden="true"></i>
    </button>
<?php }?>