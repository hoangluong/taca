<?php
$images = isset($images) ? $images : array();
$audios = isset($audios) ? $audios : array();
$videos = isset($videos) ? $videos : array();
$mime = isset($mime) ? $mime : '';
$accept = '';
if(in_array($mime, $images)){
    $accept = 'accept="image/*" ';
}elseif(in_array($mime, $audios)){
    $accept = 'accept="audio/*" ';
}elseif(in_array($mime, $videos)){
    $accept = 'accept="video/*" ';
}else{
    $accept = '';
}
?>
<div id="block-finder" class="center-fixed container-finder">
    <div class="top-control">
        <form action="#" method="post" id="form-add-folder" autocomplete="off" class="align-left">
            <input type="text" name="name" placeholder="Thêm thư mục">
            <button type="submit"><i class="fa fa-plus-circle" aria-hidden="true"></i></button>
        </form>
        <button type="button" class="i"></button>
        <form action="#" method="post" id="form-search-file" autocomplete="off" class="align-right">
            <input type="text" name="file-name" placeholder="Tìm kiếm">
            <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
        </form>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="hidden-sm hidden-xs col-md-3 col-lg-3">
                <ul class="block-dir" id="block-dirs">
                    <?php echo $this->load->view('finder/list-folder-finder');?>
                </ul>
            </div>
            <div class="col-sm-12 col-xs-12 col-md-9 col-lg-9">
                <ul class="block-file" id="block-files">
                    <?php echo $this->load->view('finder/list-file-finder');?>
                </ul>
                <div class="file-info"></div>
            </div>
        </div>
    </div>
    <div class="control">
        <form action="" method="post" id="form_upload" class="form_upload">
            <input id="btn-browse-media" name="file_content" multiple type="file" <?php echo $accept;?>style="display: none;">
            <?php $csrf = array(
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            );?>
            <input class="hash" type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
            <label for="btn-browse-media" class="btn-upload"><i class="fa fa-cloud-upload" aria-hidden="true"></i>&nbsp;Upload</label>
            <p class="message error" style="display:none"></p>
        </form>
        <div class="paging" id="paging-file">
            <?php echo $this->load->view('finder/list-file-paging');?>
        </div>
        <button class="btn-cancel">Cancel</button><button class="btn-ok">Ok</button></div>
    <button class="btn-close"><i class="fa fa-times" aria-hidden="true"></i></button>
</div>