<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Finder extends Admin_Controller{
    protected $index = 0;
    protected $file_total_pages = 0;
    protected $file_paged = 1;
    private $base_dir;
    private $images = array('gif','jpg','jpeg','png','swf','psd','bmp','tiff','jpc','jp2','jpx','jb2','swc','iff','wbmp','xbm','ico');
    private $audios = array('mp3','wma');
    private $videos = array('mp4','qt','mov','wmv','ogv','webm');
    public function __construct(){
        parent::__construct();
        $this->per_page = 32;
        $this->base_dir = FCPATH . 'uploads/';
    }
    public function index(){
        $this->index = 0;
        $mime = $this->input->get('mime', true);
        $data = array();
        $data['dirs'] = $this->browserFolder($this->base_dir, 0, array());
        $data['files'] = $this->get_files($this->base_dir, '');
        $data['paged'] = $this->file_paged;
        $data['total_pages'] = $this->file_total_pages;
        $result['files'] = $data['files'];
        $data['mime'] = $mime;
        $data['images'] = $this->images;
        $data['audios'] = $this->audios;
        $data['videos'] = $this->videos;
        $this->load->view('finder/index', $data);
    }
    public function ajx_load_finder(){
        $result = array('success'=>true, 'msg'=>'', 'html'=>'');
        $mime = $this->input->get('mime', true);
        $multiple = $this->input->get('multiple', true);
        try{
            $this->index = 0;
            $data = array();
            $data['dirs'] = $this->browserFolder($this->base_dir, 0, array());
            $data['files'] = $this->get_files($this->base_dir, '');
            $data['paged'] = $this->file_paged;
            $data['total_pages'] = $this->file_total_pages;
            $result['files'] = $data['files'];
            $data['mime'] = $mime;
            $data['images'] = $this->images;
            $data['audios'] = $this->audios;
            $data['videos'] = $this->videos;
            $data['multiple'] = $multiple;
            $result['html'] = $this->load->view('finder/finder', $data, true);
        }catch(Exception $e){
            $result['success'] = false;
            $result['msg'] = $e->getMessage();
        }
        echo json_encode($result);exit();
    }

    /**
     * browser all folder and sub folder in directory
     * @param string $dir
     * @param int $parent
     * @param array $data
     * @return array $data
     */
    private function browserFolder($dir, $parent=0, $data=array()){
        if(!is_dir($dir)) return $data;
        $files = scandir($dir);
        if(count($files)<=2) return $data;
        $dir .= substr($dir, -1)=='/' ? '' : '/';
        foreach($files as $item){
            if($item=='.' || $item == '..' || !is_dir($dir . $item)) continue;
            $path = $dir . $item;
            $this->index++;
            $data[$this->index] = array('path'=>$path, 'parent'=>$parent);
            $data = $this->browserFolder($path, $this->index, $data);
        }
        return $data;
    }
    private function get_mime(){
        $mime = $this->input->get('mime', true);
        if($mime=='video'){
            $mime = ".{".implode(',', $this->videos)."}";
        }elseif($mime=='image'){
            $mime = ".{".implode(',', $this->images)."}";
        }elseif($mime=='audio'){
            $mime = ".{".implode(',', $this->audios)."}";
        }elseif($mime){
            $mime = ".{".$mime."}";
        }else{
            $mime = '';
        }
        return $mime;
    }
    private function get_files($dir, $s=''){
        $dir = substr($dir, -1)=='/' ? $dir : $dir.'/';
        $this->file_paged = $this->input->get('paged', true);
        $this->file_paged = $this->file_paged ? $this->file_paged : 1;
        //get files in directory
        $mime = $this->get_mime();
        if($s && $mime) {
            chdir($dir);
            $pattern = '*'.$s.'*'.$mime;
            $files = glob($pattern, GLOB_BRACE);
        }elseif(!$s && $mime){
            chdir($dir);
            $pattern = '*'.$mime;
            $files = glob($pattern, GLOB_BRACE);
        }elseif($s && !$mime){
            chdir($dir);
            $files = glob('*' . $s . '*', GLOB_BRACE);
        }else{
            $files = scandir($dir);
        }
        $baseUrl = base_url();
        $data = array();
        if(!$files) return $data;
        $total = count($files);
        if($files[0]=='.' && $files[1]=='..'){
            array_shift($files);
            array_shift($files);
        }elseif($files[$total-1]=='..' && $files[$total-2]=='.'){
            array_pop($files);
            array_pop($files);
        }

        $total = count($files);
        if($total==0) return $data;
        $this->file_total_pages = $total%$this->per_page==0 ? $total/$this->per_page : intval($total/$this->per_page)+1;

        if($this->file_paged > $this->file_total_pages) return null;
        $start = ($this->file_paged-1)*$this->per_page;
        $offset = $this->file_paged*$this->per_page;
        $offset = $offset<$total ? $offset : $total;
        for($i=$start;$i<$offset;$i++){
            $item = $files[$i];
            if(is_dir($dir . $item)) continue;
            $name =  str_replace('.'. pathinfo($item, PATHINFO_EXTENSION), '', $item);
            if($name==='') continue;
            $path = $dir . $item;
            $data[] = array('path'=>$path, 'url'=>str_replace(array(FCPATH, DIRECTORY_SEPARATOR), array($baseUrl, '/'), $path));
        }
        return $data;
    }

    /**
     * ajax load files from directory
    */
    public function render_file(){
        $keyDir = $this->input->get('dir', true);
        $keyword = $this->input->get('s', true);
        $keyword = $keyword ? urldecode($keyword) : '';
        $mime = $this->input->get('mime', true);
        $result  = array('success'=>true, 'html'=>'', 'paging'=>'');
        try{
            $this->index = 0;
            $dirs = $this->browserFolder($this->base_dir, 0, array());
            if($keyDir && !isset($dirs[$keyDir])) throw new Exception('Đường dẫn không hợp lệ');
            $path = isset($dirs[$keyDir]) ? $dirs[$keyDir]['path'] : $this->base_dir;

            //$data['dirs'] = $this->browserFolder($this->base_dir, 0, array());
            $data['files'] = $this->get_files($path, $keyword);
            $data['paged'] = $this->file_paged;
            $data['total_pages'] = $this->file_total_pages;
            $data['images'] = $this->images;
            $data['audios'] = $this->audios;
            $data['videos'] = $this->videos;
            $data['mime'] = $mime;
            $result['html'] = $this->load->view('finder/list-file-finder', $data, true);
            $result['paging'] = $this->load->view('finder/list-file-paging', $data, true);
        }catch (Exception $e){
            $result['success'] = false;
            $result['msg'] = $e->getMessage();
        }
        echo json_encode($result);exit();
    }

    /**
     * ajax upload file
    */
    public function upload_media(){
        $result = array('success'=>true, 'msg'=>'', 'html'=>'', 'file_type'=>'', 'hash'=>$this->security->get_csrf_hash());
        $file_field = 'files';
        $file_type_image = array('png','jpe','jpeg','jpg','gif','bmp','ico','tiff','tif','svg','svgz');
        try{
            if(!$_FILES || !isset($_FILES[$file_field]) || !$_FILES[$file_field]) throw new Exception('Bạn chưa chọn file nào');
            $folder = $this->input->post('folder', true);
            $config = array('allowed_types'=>'*');
            if($folder){
                $dirs = $this->browserFolder($this->base_dir, 0, array());
                if(isset($dirs[$folder])){
                    $config['upload_path'] = $dirs[$folder]['path'];
                    $result['upload_path'] = $config['upload_path'];
                }
            }
            $media = $this->save_files($file_field, array(), $config);
            if(!$media) throw new Exception('Xảy ra lỗi khi tải file lên. Vui lòng thử lại.');
            if(is_array($media) && isset($media['msg'])) throw new Exception($media['msg']);

            if(is_array($media)){
                foreach($media as $item){
                    $this->getHtmlItemFile($result, $item);
                }
            }else{
                $this->getHtmlItemFile($result, $media);
            }
        }catch (Exception $e){
            $result['msg'] = $e->getMessage();
            $result['success'] = false;
        }
        echo json_encode($result);exit();
    }
    private function getHtmlItemFile(&$result, $itemMedia){
        $file_type_image = array('png','jpe','jpeg','jpg','gif','bmp','ico','tiff','tif','svg','svgz');

        $result['file_type'] = strtolower( pathinfo($itemMedia, PATHINFO_EXTENSION) );
        if($result['file_type']=='mp4' || $result['file_type']=='ogg') {
            $inner_html = '<i class="fa fa-file-video-o" aria-hidden="true"></i>';
        }elseif($result['file_type']=='mp3' || $result['file_type']=='wma'){
            $inner_html = '<i class="fa fa-file-audio-o" aria-hidden="true"></i>';
        }elseif(in_array( $result['file_type'], $file_type_image)){
            $inner_html = '<img src="'.base_url($itemMedia).'">';
        }else{
            $inner_html = '<i class="fa fa-file-o" aria-hidden="true"></i>';
        }
        $result['html'] .= '<li><a data-name="'.basename($itemMedia).'" href="'.base_url($itemMedia).'">' . $inner_html . '</a></li>';
    }

    /*public function editor_upload_media(){//action: for ajax
        $file = $_FILES['file'];
        $res = (object) array('success'=>false, 'msg'=>'You have not selected any files');
        if($file){
            $folder = input_post('folder', '');
            $res = $this->upload($file,
                array('path' => ($folder ? $folder.'/' : ''),
                    'mime' => array('mp4','ogg','wma','mp3','png','jpe','jpeg','jpg','gif','bmp','ico','tiff','tif','svg','svgz')
                ));
            if($res->success){
                if($res->file_type=='mp4') {
                    $res->html = '<br/><video width="320" height="240" controls>' .
                        '<source src="'.base_url($res->file).'" type="video/mp4">' .
                        '</video>';
                }elseif($res->file_type=='ogg'){
                    $res->html = '<br/><video width="320" height="240" controls>' .
                        '<source src="'.base_url($res->file).'" type="video/ogg">' .
                        '</video>';
                }elseif($res->file_type=='mp3' || $res->file_type=='wma'){
                    $res->html = '<br/><audio controls>' .
                        '<source src="'.base_url($res->file).'" type="audio/mpeg">' .
                        '</audio>';
                }elseif(in_array(strtolower($res->file_type), array('png','jpe','jpeg','jpg','gif','bmp','ico','tiff','tif','svg','svgz'))){
                    $res->html = '<img src="'.base_url($res->file).'">';
                }else{
                    $res->html = '';
                }
            }
        }
        echo json_encode($res);
        exit();
    }*/

    /**
     * ajax remove file
    */
    public function remove_file(){
        $res = (object)array('success'=>true, 'msg'=>'', 'hash'=>$this->security->get_csrf_hash());
        try{
            $files = $this->input->post('files', true);
            if(!$files) throw new Exception('Input is not valid');
            if(is_array($files)){
                foreach($files as $file){
                    $file = str_replace(array(base_url(), '/'), array(FCPATH, DIRECTORY_SEPARATOR), $file);
                    if(file_exists($file)){
                        unlink($file);
                    }
                }
            }else{
                $file = str_replace(array(base_url(), '/'), array(FCPATH, DIRECTORY_SEPARATOR), $files);
                if(file_exists($file)){
                    unlink($file);
                }
            }
            $res->msg = 'File removed';
        }catch (Exception $e){
            $res->success = false;
            $res->msg = $e->getMessage();
        }
        echo json_encode($res);exit();
    }

    /**
     * ajax add folder
    */
    public function add_folder(){
        $res = (object)array('success'=>true, 'msg'=>'', 'hash'=>$this->security->get_csrf_hash());
        try{
            $keyDir = $this->input->post('dir', true);
            $folder_name = $this->input->post('name', true);
            if(!$folder_name) throw new Exception('Input is not valid');

            $dirs = $this->browserFolder($this->base_dir, 0, array());
            $path = isset($dirs[$keyDir]) ? $dirs[$keyDir]['path'] : $this->base_dir;

            $mask=umask(0);
            mkdir($path.DIRECTORY_SEPARATOR.$folder_name, 0777);
            umask($mask);
            $res->msg = 'Thư mục đã được tạo';
            $this->index = 0;
            $new_dirs = $this->browserFolder($this->base_dir, 0, array());
            $res->dirs = $new_dirs;
            $res->key_active = $keyDir;
            $data = array();
            $data['dirs'] = $new_dirs;
            $data['key_active'] = $keyDir;
            $res->html = $this->load->view('finder/list-folder-finder', $data, true);
        }catch (Exception $e){
            $res->success = false;
            $res->msg = $e->getMessage();
        }
        echo json_encode($res);exit();
    }
}