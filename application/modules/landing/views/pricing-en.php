<?php $asset = base_url('assets/');?>
<div style="padding-top:60px;" class="one-column cf ui-sortable" id="le_body_row_1" data-style="eyJlbGVtZW50SWQiOiJsZV9ib2R5X3Jvd18xIiwicGFkZGluZ1RvcCI6IjYwIiwiYm9yZGVyVG9wV2lkdGgiOiIiLCJib3JkZXJUb3BDb2xvciI6IiIsImJvcmRlckJvdHRvbVdpZHRoIjoiIiwiYm9yZGVyQm90dG9tQ29sb3IiOiIiLCJzZWN0aW9uU2VwYXJhdG9yVHlwZSI6Im5vbmUiLCJzZWN0aW9uU2VwYXJhdG9yU3R5bGUiOiIiLCJleHRyYXMiOnsiYW5pbWF0aW9uRWZmZWN0IjoiIiwiYW5pbWF0aW9uRGVsYXkiOiIifSwicm93U2Nyb2xsRml4ZWRQb3NpdGlvbiI6Im5vbmUiLCJhZGRvbiI6e319">
    <div style="position: absolute;width: 100%;height: 100%;top: 0;left: 0;bottom: 0;right: 0;" class="op-row-image-color-overlay"></div>
    <div class="fixed-width">
        <div class="one-column column cols" id="le_body_row_1_col_1">
            <div class="element-container cf" id="le_body_row_1_col_1_el_1">
                <div class="element">
                    <h1 class="op-headline rotate-1" style="font-size:45px;font-family:'Montserrat',sans-serif;font-weight:bold;text-align:center;margin:50px 0 100px;" id="el_ah_1f80ba9fe3118d8fbb28a5db32da49e3">
                        <span>Pricing</span>
                        <span class="op-words-wrapper" style="width:1000px;font-size:20px;line-height: 1.5em;margin:15px 0 0;">
                            Trying to scale your sales & revenue? Get Advanced or Professional Plan with all necessary features!
                            Or use our Free Beginner Plan! No credit card required.
                        </span>
                    </h1>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section pricing-wrap">
    <div class="wrap cols pricing">
        <table class="featured-pricing-table">
            <thead>
            <tr>
                <td>
                    <h3 class="fp-title">FEATURES</h3>
                </td>
                <td class="featured-pplan fpp-title">
                    <h3>Beginner</h3>
                    <div class="pricing-icon"><img src="<?php echo $asset;?>images/beginner.svg" alt=""></div>
                    <div class="pricing-tag">
                        <span class="free">FREE</span>
                        <p>as always</p>
                    </div>
                </td>
                <td class="featured-pplan fpp-title popular">
                    <h3>START</h3>
                    <div class="pricing-icon"><img src="<?php echo $asset;?>images/pro.svg" alt=""></div>
                    <div class="pricing-tag">
                        <span><?php printf("%.2f", isset($pay_config->price_1) ? $pay_config->price_1 : 7.99);?></span>
                        <p>per month</p>
                    </div>
                </td>
                <td class="featured-pplan fpp-title">
                    <h3>BUSINESS</h3>
                    <div class="pricing-icon"><img src="<?php echo $asset;?>images/pro.svg" alt=""></div>
                    <div class="pricing-tag">
                        <span><?php printf("%.2f", isset($pay_config->price_2) ? $pay_config->price_2 : 14.99);?></span>
                        <p>per month</p>
                    </div>
                </td>
                <td class="featured-pplan fpp-title">
                    <h3>PROFESSIONAL</h3>
                    <div class="pricing-icon"><img src="<?php echo $asset;?>images/advanced.svg" alt=""></div>
                    <div class="pricing-tag">
                        <span><?php printf("%.2f", isset($pay_config->price_3) ? $pay_config->price_3 : 24.99);?></span>
                        <p>per month</p>
                    </div>
                </td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    Import listings (title, specifics, description, images)
                    <ul>
                        <li>From Amazon to eBay</li>
                        <li>From eBay to eBay</li>
                        <li>From Aliexpress to eBay</li>
                        <li>From Dhgate to eBay</li>
                    </ul>
                </td>
                <td class="featured-pplan fpp-content">20 Listings/month</td>
                <td class="featured-pplan fpp-content">150 Listings/month</td>
                <td class="featured-pplan fpp-content">500 Listings/month</td>
                <td class="featured-pplan fpp-content">Unlimited</td>
            </tr>
            <tr>
                <td>
                    Copy and manage orders (full order details)
                    <ul>
                        <li>From eBay to Aliexpress</li>
                    </ul>
                </td>
                <td class="featured-pplan fpp-content">10 Orders/month</td>
                <td class="featured-pplan fpp-content">50 Orders/month</td>
                <td class="featured-pplan fpp-content">100 Orders/month</td>
                <td class="featured-pplan fpp-content">Unlimited</td>
            </tr>
            <tr>
                <td>
                    After sale customer services
                    <ul>
                        <li>Auto get buyer’s  information to send emails to customers with more than 10 email templates</li>
                    </ul>
                </td>
                <td class="featured-pplan fpp-content">Unlimited</td>
                <td class="featured-pplan fpp-content">Unlimited</td>
                <td class="featured-pplan fpp-content">Unlimited</td>
                <td class="featured-pplan fpp-content">Unlimited</td>
            </tr>
            <tr>
                <td>
                    Search items by images
                    <ul>
                        <li>From eBay to Aliexpress</li>
                        <li>From eBay to Banggood</li>
                        <li>From eBay to Dhgate</li>
                        <li>From Aliexpress to eBay</li>
                        <li>From Banggood to eBay</li>
                        <li>From Dhgate to eBay</li>
                    </ul>
                </td>
                <td class="featured-pplan fpp-content">Unlimited</td>
                <td class="featured-pplan fpp-content">Unlimited</td>
                <td class="featured-pplan fpp-content">Unlimited</td>
                <td class="featured-pplan fpp-content">Unlimited</td>
            </tr>
            <tr>
                <td>Search bestselling items on eBay (keyword)</td>
                <td class="featured-pplan fpp-content">Unlimited</td>
                <td class="featured-pplan fpp-content">Unlimited</td>
                <td class="featured-pplan fpp-content">Unlimited</td>
                <td class="featured-pplan fpp-content">Unlimited</td>
            </tr>
            <tr>
                <td>eDrop Support Team</td>
                <td class="featured-pplan fpp-content">Limited support in working hours from Monday to Friday</td>
                <td class="featured-pplan fpp-content">Unlimited support 24/7 Call/SMS/Email/Fanpage/Secrete Group</td>
                <td class="featured-pplan fpp-content popular">Unlimited support 24/7 Call/SMS/Email/Fanpage/Secrete Group</td>
                <td class="featured-pplan fpp-content popular">Unlimited support 24/7 Call/SMS/Email/Fanpage/Secrete Group</td>
            </tr>
            <tr class="actions">
                <td></td>
                <td><a href="https://chrome.google.com/webstore/detail/edrop-drop-shipping-tool/jgpilhblmknfggjpkdcabajchcccgfeo?utm_source=chrome-app-launcher-info-dialog" class="btn outline pricing-btn small">Install FREE</a></td>
                <td>
                    <?php if(isset($user_id) && $user_id){
                        if(isset($pay_config) && $pay_config){?>
                    <form action="<?php echo $pay_config->form_buy_action;?>" method="post">

                        <!-- Identify your business so that you can collect the payments. -->
                        <input type="hidden" name="business" value="<?php echo $pay_config->email_business;?>">

                        <!-- Specify a Buy Now button. -->
                        <input type="hidden" name="cmd" value="_xclick">

                        <!-- Specify details about the item that buyers will purchase. -->
                        <input type="hidden" name="item_number" value="<?php echo $user_id;?>">
                        <input type="hidden" name="item_name" value="<?php echo $pay_config->name_1;?>">
                        <input type="hidden" name="amount" value="<?php echo $pay_config->price_1;?>">
                        <input type="hidden" name="currency_code" value="USD">
                        <input type="hidden" name="quantity" value="1">

                        <!-- Display the payment button. -->
                        <input type="image" name="submit" border="0"
                        src="<?php echo $asset.'images/btn_buynow_107x26.png';?>" alt="Buy Now">
                        <img alt="" border="0" width="1" height="1" src="<?php echo $asset.'images/pixel.gif';?>" >

                        <input name="notify_url" value="<?php echo base_url('payment/billing');?>" type="hidden">
                        <p>Upgrade for account: <?php echo $user_data['username'];?></p>

                    </form>
                <?php }
                    }else{?>
                    <a class="btn-login-to-buy" href="<?php echo base_url('auth/login?back='.urlencode(base_url('pricing')));?>"><img src="<?php echo $asset.'images/btn_buynow_107x26.png';?>" alt="Login"/></a>
                <?php }?>

                    <!-- <a href="javascript:void(0)" class="btn outline pricing-btn small">BUY NOW</a> -->
                </td>
                <td>
                    <?php if(isset($user_id) && $user_id){
                        if(isset($pay_config) && $pay_config){?>
                    <form action="<?php echo $pay_config->form_buy_action;?>" method="post">

                        <!-- Identify your business so that you can collect the payments. -->
                        <input type="hidden" name="business" value="<?php echo $pay_config->email_business;?>">

                        <!-- Specify a Buy Now button. -->
                        <input type="hidden" name="cmd" value="_xclick">

                        <!-- Specify details about the item that buyers will purchase. -->
                        <input type="hidden" name="item_number" value="<?php echo $user_id;?>">
                        <input type="hidden" name="item_name" value="<?php echo $pay_config->name_2;?>">
                        <input type="hidden" name="amount" value="<?php echo $pay_config->price_2;?>">
                        <input type="hidden" name="currency_code" value="USD">
                        <input type="hidden" name="quantity" value="1">

                        <!-- Display the payment button. -->
                        <input type="image" name="submit" border="0"
                        src="<?php echo $asset.'images/btn_buynow_107x26.png';?>" alt="Buy Now">
                        <img alt="" border="0" width="1" height="1" src="<?php echo $asset.'images/pixel.gif';?>" >

                        <input name="notify_url" value="<?php echo base_url('payment/billing');?>" type="hidden">

                        <p>Upgrade for account: <?php echo $user_data['username'];?></p>

                    </form>
                    <?php }
                    }else{?>
                        <a class="btn-login-to-buy" href="<?php echo base_url('auth/login?back='.urlencode(base_url('pricing')));?>"><img src="<?php echo $asset.'images/btn_buynow_107x26.png';?>" alt="Login"/></a>
                    <?php }?>
                    <!-- <a href="javascript:void(0)" class="btn gradient pricing-btn small">BUY NOW</a> -->
                </td>
                <td>
                    <?php if(isset($user_id) && $user_id){
                        if(isset($pay_config) && $pay_config){?>
                    <form action="<?php echo $pay_config->form_buy_action;?>" method="post">

                        <!-- Identify your business so that you can collect the payments. -->
                        <input type="hidden" name="business" value="<?php echo $pay_config->email_business;?>">

                        <!-- Specify a Buy Now button. -->
                        <input type="hidden" name="cmd" value="_xclick">

                        <!-- Specify details about the item that buyers will purchase. -->
                        <input type="hidden" name="item_number" value="<?php echo $user_id;?>">
                        <input type="hidden" name="item_name" value="<?php echo $pay_config->name_3;?>">
                        <input type="hidden" name="amount" value="<?php echo $pay_config->price_3;?>">
                        <input type="hidden" name="currency_code" value="USD">
                        <input type="hidden" name="quantity" value="1">

                        <!-- Display the payment button. -->
                        <input type="image" name="submit" border="0"
                        src="<?php echo $asset.'images/btn_buynow_107x26.png';?>" alt="Buy Now">
                        <img alt="" border="0" width="1" height="1" src="<?php echo $asset.'images/pixel.gif';?>" >

                        <input name="notify_url" value="<?php echo base_url('payment/billing');?>" type="hidden">

                        <p>Upgrade for account: <?php echo $user_data['username'];?></p>

                    </form>
                    <?php }
                    }else{?>
                        <a class="btn-login-to-buy" href="<?php echo base_url('auth/login?back='.urlencode(base_url('pricing')));?>"><img src="<?php echo $asset.'images/btn_buynow_107x26.png';?>" alt="Login"/></a>
                    <?php }?>
                    <!-- <a href="javascript:void(0)" class="btn gradient pricing-btn small">BUY NOW</a> -->
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
