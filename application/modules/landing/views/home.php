<?php $asset = base_url('assets/'); ?>
<div class="home-main">
    <div class="home-slider">
        <div class="container">

        </div>
    </div>
    <div class="home-block">
        <div class="container">
            <div class="block-title">
                <span>Các khóa học</span>
            </div>
            <div class="block-title-sub">
                Học viện TACA được thành lập với mong muốn trở thành địa chỉ tin cậy, chuyên sâu về đào tạo & huấn luyện
                kế toán – tài chính – thuế – quản trị cho các bạn làm nghề và phụng sự sự phát triển bền vững của Doanh
                nghiệp Việt.
            </div>
            <div class="block-content">
                <div class="home-course-list">
                    <?php

                    foreach ($allCourses as $item) {
                        ?>
                        <div class="col-md-6">
                            <div class="course-item">
                                <div class="course-thumb">
                                    <img
                                        src="https://www.hellochao.vn/landing-page/index/images/lopgt-update.png#1553157128"
                                        alt="">
                                </div>
                                <div class="course-info">
                                    <div class="title"><?php echo $item['title']; ?></div>
                                    <hr>
                                    <div class="desc">
                                      <?php echo word_limiter($item['desc'],30,'...');?>

                                    </div>
                                    <br>

                                    <div style="text-align: center;"><a href="<?php echo base_url('course/detail/'.$item['id']);?>" class="btn btn-primary">Xem ngay</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="home-block bg-gray">
        <div class="container">
            <div class="block-title">
                <span>Giảng viên tiêu biểu </span>
            </div>
            <div class="block-title-sub">
                Tất cả giảng viên của Học viện là những thầy cô đã trải nghiệm và có thâm niên công tác thực tế triển
                khai tại doanh nghiệp với ít nhất 15 – 25 năm kinh nghiệm.
            </div>
            <div class="block-content">
                <div class="list-teacher">
                    <?php
                    for ($i = 0; $i < 3; $i++) {
                        ?>
                        <div class="div col-md-4">
                            <div class="teacher-item">
                                <div class="thumb"><img
                                        src="https://taca.edu.vn/wp-content/uploads/2018/01/25659980_1467592923294912_6190385219108491200_n.jpg"
                                        alt=""></div>
                                <div class="title">Thầy Nguyễn Ngọc Minh</div>
                                <div class="position">CEO Công ty Kế toán & Đại lý thuế FOSA</div>
                                <div class="desc">25 năm kinh nghiệm tư vấn thuế tại Việt Nam và các doanh nghiệp nước
                                    ngoài
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>