<?php $asset = base_url('assets/');?>

    <div id="content_area" class="">
        <div style="padding-top:60px;" class="row one-column cf ui-sortable   " id="le_body_row_1">

            <div style="position: absolute;width: 100%;height: 100%;top: 0;left: 0;bottom: 0;right: 0;" class="op-row-image-color-overlay"></div>
            <div class="fixed-width">
                <div class="one-column column cols" id="le_body_row_1_col_1">
                    <div class="element-container cf"  id="le_body_row_1_col_1_el_1">
                        <div class="element">
                            <h1 class="op-headline rotate-1 " style="font-size:45px;font-family:'Montserrat',sans-serif;font-weight:bold;text-align:center;margin-top:25px;" id="el_ah_1f80ba9fe3118d8fbb28a5db32da49e3">
                                <span>Edrop-Dropshipping tool</span>
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row two-columns cf ui-sortable   " id="le_body_row_2" >
            <div class="fixed-width">
                <div class="one-half column cols" id="le_body_row_2_col_1">
                    <div class="element-container cf"  id="le_body_row_2_col_1_el_1">
                        <div class="element">
                            <iframe width="100%" height="380" src="https://www.youtube.com/embed/LF1MTnN0VBE?list=PLIGXHMNiuzIRdn85nHG6vN5ruuyh4PNyx" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <div class="one-half column cols" id="le_body_row_2_col_2">
                    <div class="element-container cf top_bullets" id="le_body_row_2_col_2_el_1">
                        <div class="element">
                            <ul class="bullet-list 1">
                                <li>To easily list dropshipped products (images, item description, item specifics) directly into your ebay listing (from aliexpress to ebay and from ebay to ebay) – in only a few clicks.</li>
								<li>To easily copy buyer addresses from ebay to aliexpress (name, full addresses, phone number) – in only two clicks (copy on ebay and paste on aliexpress).</li>
                                <li>To easily manage your orders on ebay (order numbers, tracking code and send email to buyer with more than 10 emails templates).</li>
                                <li>To essily search images for product via ebay.com, aliexpress.com, dhgate.com, bangood.com</li>
                                <li>To essily find product on aliexpress.com , ebay.com</li>
                            </ul>


                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div style="padding-bottom:30px;" class="row one-column cf ui-sortable   " id="le_body_row_3" >
            <div style="position: absolute;width: 100%;height: 100%;top: 0;left: 0;bottom: 0;right: 0;" class="op-row-image-color-overlay"></div>
            <div class="fixed-width">
                <div class="one-column column cols" id="le_body_row_3_col_1">
                    <div class="element-container cf"  id="le_body_row_3_col_1_el_1">
                        <div class="element">
                            <div style="text-align:center">
                                <a target="_blank" href="https://chrome.google.com/webstore/detail/edrop-drop-shipping-tool/jgpilhblmknfggjpkdcabajchcccgfeo" id="btn_1_3cec468c69089e16826d885c5d34fe89" class="css-button style-1"><span class="text">Install Extension</span><span class="hover"></span><span class="active"></span></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div style="background:#e5e5e5;background:-webkit-gradient(linear, left top, left bottom, color-stop(0%, #e5e5e5), color-stop(100%, #e5e5e5));background:-webkit-linear-gradient(top, #e5e5e5 0%, #e5e5e5 100%);background:-moz-linear-gradient(top, #e5e5e5 0%, #e5e5e5 100%);background:-ms-linear-gradient(top, #e5e5e5 0%, #e5e5e5 100%);background:-o-linear-gradient(top, #e5e5e5 0%, #e5e5e5 100%);background:linear-gradient(to bottom, #e5e5e5 0%, #e5e5e5 100%);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#e5e5e5, endColorstr=#e5e5e5, GradientType=0);padding-top:30px;padding-bottom:10px;" class="row two-columns cf ui-sortable section   " id="le_body_row_4">
            <div style="position: absolute;width: 100%;height: 100%;top: 0;left: 0;bottom: 0;right: 0;" class="op-row-image-color-overlay"></div>
            <div class="fixed-width">
                <div class="one-half column cols" id="le_body_row_4_col_1">
                    <div class="element-container cf"  id="le_body_row_4_col_1_el_1">
                        <div class="element">
                            <div class="op-text-block" style="width:100%;margin: 0 auto;text-align:center;">
                                <p style="font-size:22px;font-family:'Droid Serif',sans-serif;font-style:italic;">As Seen </p>
                            </div>
                        </div>
                    </div>
                    <div class="element-container cf"  id="le_body_row_4_col_1_el_2">
                        <div class="element">
                            <div class="image-caption" style="width:300px;margin:0 auto;"><img alt="" src="<?php echo $asset;?>images/ali.png" border="0" class="full-width"></div>
                        </div>
                    </div>
                </div>
                <div class="one-half column cols" id="le_body_row_4_col_2">
                    <div class="element-container cf"  id="le_body_row_4_col_2_el_1">
                        <div class="element">
                            <div class="image-caption" style="width:200px;margin:0 auto"><img alt="" src="<?php echo $asset;?>images/compatibleapplication_vert.png" border="0" class="full-width"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




        <div style="background:#FFFFFF;background:-webkit-gradient(linear, left top, left bottom, color-stop(0%, #FFFFFF), color-stop(100%, #FFFFFF));background:-webkit-linear-gradient(top, #FFFFFF 0%, #FFFFFF 100%);background:-moz-linear-gradient(top, #FFFFFF 0%, #FFFFFF 100%);background:-ms-linear-gradient(top, #FFFFFF 0%, #FFFFFF 100%);background:-o-linear-gradient(top, #FFFFFF 0%, #FFFFFF 100%);background:linear-gradient(to bottom, #FFFFFF 0%, #FFFFFF 100%);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#FFFFFF, endColorstr=#FFFFFF, GradientType=0);padding-top:0;padding-bottom:0;" class="row one-column cf ui-sortable section   " id="le_body_row_9" >
            <div style="position: absolute;width: 100%;height: 100%;top: 0;left: 0;bottom: 0;right: 0;" class="op-row-image-color-overlay"></div>
            <div class="fixed-width">
                <div class="one-column column cols" id="le_body_row_9_col_1">
                    <div class="element-container cf"  id="le_body_row_9_col_1_el_1">
                        <div class="element">
                            <h2 class="headline-style-6" style="text-align:center;margin-top:30px;margin-bottom:30px;">User guild</h2>
                        </div>
                    </div>
                    <div class="element-container cf"  id="le_body_row_9_col_1_el_2">
                        <div class="element">
                            <ul class="feature-block feature-block-style-1 feature-block-one-col cf">
                                <li>
                                    <div>
                                        <h2>
                                            <span class="span_img">1</span>
                                            <span>Install Sell Hub<a href="https://goo.gl/SjxB42">https://goo.gl/SjxB42</a></span>
                                        </h2>

										<div style="display:block;" class="content-1">
                                            <p>Sell ​​Hub is a very important part of selling with eBay. Use the Sell Hub for an overview of the account, making it easy, convenient and optimal for the seller.
                                                 It's also the first step where you can use #edroptool when you make a purchase.</p>
                                            <h5>To install the Sell Hub for the account</h5>
                                            <ul>
                                                <li>Go to Help and Contact</li>
                                                <li>Type Sell Hub and select Getting Started with Sell Hub</li>

                                            </ul>
                                            <p>Then follow the basic eBay instructions to start the installation and use</p>
                                            <p>Images introduction</p>
                                            <p style="text-align:center;"><img src="<?php echo $asset;?>images/sellhub1.png"/></p>
                                            <p style="text-align:center;"><img src="<?php echo $asset;?>images/sellhub2.png"/></p>
                                        </div>

                                    </div>

                                </li>
                                <li>
                                    <div>
                                        <h2>
                                            <span class="span_img">2</span>
                                            <span>Get the buyer address <a href="https://goo.gl/9BqmT4">https://goo.gl/9BqmT4</a></span>
                                            <a href="#content_2" class="read-more">Read more</a>
                                        </h2>
                                        <div style="display:none;" class="content-1" id="content_2">

                                            <p>To get a simple buyer address when using the Sell Hub, you only need:</p>

                                            <ul>
                                                <li>Go to awaiting shippment</li>
                                                <li>Select the ship waiting icon to view buyer address details</li>

                                            </ul>
                                            <p>Hình ảnh minh họa</p>
                                            <p style="text-align:center;"><img src="<?php echo $asset;?>images/sellhub3.png"/></p>
                                            <p style="text-align:center;"><img src="<?php echo $asset;?>images/sellhub4.png"/></p>
                                        </div>
                                    </div>

                                </li>

                                <li style="clear:both;">
                                    <div>
                                        <h2>
                                            <span class="span_img">3</span>
                                            <span>Use #edroptool buy Aliexpress<a href="https://goo.gl/gyzmH5">https://goo.gl/gyzmH5</a></span>
                                            <a href="#content_3" class="read-more">Read more</a>
                                        </h2>
                                        <div style="display:none;" class="content-1" id="content_3">

                                            <ul>
                                                <li>Do not worry about mistakes when copy paste manually</li>

                                                <li>Do not lose google search State on Aliexpress</li>

                                                <li>Accurate, simple, time saving with just 2 clicks</li>
                                                <li>To copy the purchase information, you:</li>
                                                <li>Open buyer address on ebay ▶click icon Copy</li>
                                                <li>Open the recipient address page on Ali ▶ click icon Paste</li>
                                                <li>All information is automatically pasted to Ali</li>

                                            </ul>

                                            <p>Illustrating images</p>
                                            <p style="text-align:center;"><img src="<?php echo $asset;?>images/order1.jpg"/></p>
                                            <p style="text-align:center;"><img src="<?php echo $asset;?>images/order2.png"/></p>
                                        </div>
                                    </div>

                                </li>


                                <li style="clear:both;">
                                    <div>
                                        <h2>
                                            <span class="span_img">4</span>
                                            <span>Import guide, post from aliexpress, ebay to ebay , dhgate to ebay<a href="https://goo.gl/6JxCtd">https://goo.gl/6JxCtd</a></span>
                                        </h2>

                                    </div>

                                </li>

                            </ul>
                        </div>

                    </div>

                </div>
            </div>
        </div>

        <div style="background:#FFFFFF;background:-webkit-gradient(linear, left top, left bottom, color-stop(0%, #FFFFFF), color-stop(100%, #FFFFFF));background:-webkit-linear-gradient(top, #FFFFFF 0%, #FFFFFF 100%);background:-moz-linear-gradient(top, #FFFFFF 0%, #FFFFFF 100%);background:-ms-linear-gradient(top, #FFFFFF 0%, #FFFFFF 100%);background:-o-linear-gradient(top, #FFFFFF 0%, #FFFFFF 100%);background:linear-gradient(to bottom, #FFFFFF 0%, #FFFFFF 100%);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#FFFFFF, endColorstr=#FFFFFF, GradientType=0);padding-top:0;padding-bottom:0;" class="row one-column cf ui-sortable section   " id="le_body_row_9" data-style="=">
            <div style="position: absolute;width: 100%;height: 100%;top: 0;left: 0;bottom: 0;right: 0;" class="op-row-image-color-overlay"></div>
            <div class="fixed-width">
                <div class="one-column column cols" id="le_body_row_9_col_1">
                    <div class="element-container cf"  id="le_body_row_9_col_1_el_1">
                        <div class="element">
                            <h2 class="headline-style-6" style="text-align:center;margin-top:30px;margin-bottom:30px;">Video tutorial</h2>
                        </div>
                    </div>
                    <div class="element-container cf"  id="le_body_row_9_col_1_el_2">
                        <div class="element">
                            <ul class="feature-block feature-block-style-1 feature-block-two-col cf">
                                <li>
                                    <div>
                                        <h2>

                                            <span>How to place order on Alixpress with eDrop Tool</span>
                                        </h2>
                                        <div>
                                            <iframe width="100%" height="360" src="https://www.youtube.com/embed/LF1MTnN0VBE?list=PLIGXHMNiuzIRdn85nHG6vN5ruuyh4PNyx" frameborder="0" allowfullscreen></iframe>
                                        </div>

                                    </div>

                                </li>

                                <li>
                                    <div>
                                        <h2>

                                            <span>How to import listing from Aliexpress to eBay with eDrop Tool</span>
                                        </h2>
                                        <div>
                                            <iframe width="100%" height="360" src="https://www.youtube.com/embed/82yY-bWTl7c?list=PLIGXHMNiuzIRdn85nHG6vN5ruuyh4PNyx" frameborder="0" allowfullscreen></iframe>
                                        </div>

                                    </div>

                                </li>

                                <li>
                                    <div>
                                        <h2>

                                            <span>How to import listing from eBay to eBay with eDrop Tool</span>
                                        </h2>
                                        <div>
                                            <iframe width="100%" height="360" src="https://www.youtube.com/embed/UWg80XWLAt8?list=PLIGXHMNiuzIRdn85nHG6vN5ruuyh4PNyx" frameborder="0" allowfullscreen></iframe>
                                        </div>

                                    </div>

                                </li>

                                <li>
                                    <div>
                                        <h2>

                                            <span>How to send email template to customer with eDrop Tool</span>
                                        </h2>
                                        <div>

                                            <iframe width="100%" height="360" src="https://www.youtube.com/embed/DmyoYeHKGXU?list=PLIGXHMNiuzIRdn85nHG6vN5ruuyh4PNyx" frameborder="0" allowfullscreen></iframe>
                                        </div>

                                    </div>

                                </li>


                            </ul>
                        </div>

                    </div>

                </div>
            </div>
        </div>
