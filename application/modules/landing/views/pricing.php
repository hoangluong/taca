<?php $asset = base_url('assets/');?>
<div style="padding-top:60px;" class="one-column cf ui-sortable" id="le_body_row_1">
    <div style="position: absolute;width: 100%;height: 100%;top: 0;left: 0;bottom: 0;right: 0;" class="op-row-image-color-overlay"></div>
    <div class="fixed-width">
        <div class="one-column column cols" id="le_body_row_1_col_1">
            <div class="element-container cf" id="le_body_row_1_col_1_el_1">
                <div class="element">
                    <h1 class="op-headline rotate-1" style="font-size:45px;font-family:'Montserrat',sans-serif;font-weight:bold;text-align:center;margin:50px 0 100px;" id="el_ah_1f80ba9fe3118d8fbb28a5db32da49e3">
                        <span>Bảng giá</span>

                    </h1>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section pricing-wrap">
    <div class="wrap cols pricing">
        <table class="featured-pricing-table">
            <thead>
            <tr>
                <td>
                    <h3 class="fp-title">Tính năng</h3>
                </td>
                <td class="featured-pplan fpp-title">
                    <h3>Beginner</h3>
                    <div class="pricing-icon"><img src="<?php echo $asset;?>images/beginner.svg" alt=""></div>
                    <div class="pricing-tag">
                        <span class="free">FREE</span>
                        <p>as always</p>
                    </div>
                </td>
                <td class="featured-pplan fpp-title popular">
                    <h3>START</h3>
                    <div class="pricing-icon"><img src="<?php echo $asset;?>images/pro.svg" alt=""></div>
                    <div class="pricing-tag">
                        <span><?php printf("%.2f", isset($pay_config)&&isset($pay_config->price_1) ? $pay_config->price_1 : 7.99);?></span>
                        <p>trên tháng</p>
                    </div>
                </td>
                <td class="featured-pplan fpp-title">
                    <h3>BUSINESS</h3>
                    <div class="pricing-icon"><img src="<?php echo $asset;?>images/advanced.svg" alt=""></div>
                    <div class="pricing-tag">
                        <span><?php printf("%.2f", isset($pay_config)&&isset($pay_config->price_2) ? $pay_config->price_2 : 14.99);?></span>
                        <p>trên tháng</p>
                    </div>
                </td>
                <td class="featured-pplan fpp-title">
                    <h3>PROFESSIONAL</h3>
                    <div class="pricing-icon"><img src="<?php echo $asset;?>images/advanced.svg" alt=""></div>
                    <div class="pricing-tag">
                        <span><?php printf("%.2f", isset($pay_config)&&isset($pay_config->price_3) ? $pay_config->price_3 : 24.99);?></span>
                        <p>trên tháng</p>
                    </div>
                </td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    Nhập sản phẩm (tiêu đề, đặc tính, mô tả, hình ảnh)
                    <ul>
                        <li>Từ Amazon tới eBay</li>
                        <li>Từ eBay tới eBay</li>
                        <li>Từ Aliexpress tới eBay</li>
                        <li>Từ Dhgate tới eBay</li>
                    </ul>
                </td>
                <td class="featured-pplan fpp-content">20 sản phẩm/tháng</td>
                <td class="featured-pplan fpp-content">150 sản phẩm/tháng</td>
                <td class="featured-pplan fpp-content">500 sản phẩm/tháng</td>
                <td class="featured-pplan fpp-content">Unlimited</td>
            </tr>
            <tr>
                <td>
                    Sao chép và quản lý đơn đặt hàng (đầy đủ chi tiết đơn hàng)
                    <ul>
                        <li>Từ eBay tới Aliexpress</li>
                    </ul>
                </td>
                <td class="featured-pplan fpp-content">10 đơn hàng/tháng</td>
                <td class="featured-pplan fpp-content">50 đơn hàng/tháng</td>
                <td class="featured-pplan fpp-content">100 đơn hàng/tháng</td>
                <td class="featured-pplan fpp-content">Unlimited</td>
            </tr>
            <tr>
                <td>
                    Quản lý khách hàng
                    <ul>
                        <li>Tự động lấy thông tin của người mua để gửi email cho khách hàng với hơn 10 mẫu email/li>
                    </ul>
                </td>
                <td class="featured-pplan fpp-content">Unlimited</td>
                <td class="featured-pplan fpp-content">Unlimited</td>
                <td class="featured-pplan fpp-content">Unlimited</td>
                <td class="featured-pplan fpp-content">Unlimited</td>
            </tr>
            <tr>
                <td>
                    Tìm kiếm sản phẩm qua hình ảnh
                    <ul>
                        <li>Từ eBay tới Aliexpress</li>
                        <li>Từ eBay tới Banggood</li>
                        <li>Từ eBay tới Dhgate</li>
                        <li>Từ Aliexpress tới eBay</li>
                        <li>Từ Banggood tới eBay</li>
                        <li>Từ Dhgate tới eBay</li>
                    </ul>
                </td>
                <td class="featured-pplan fpp-content">Unlimited</td>
                <td class="featured-pplan fpp-content">Unlimited</td>
                <td class="featured-pplan fpp-content">Unlimited</td>
                <td class="featured-pplan fpp-content">Unlimited</td>
            </tr>
            <tr>
                <td>Tìm kiếm sản phẩm bán chạy trên ebay,aliepress</td>
                <td class="featured-pplan fpp-content">Unlimited</td>
                <td class="featured-pplan fpp-content">Unlimited</td>
                <td class="featured-pplan fpp-content">Unlimited</td>
                <td class="featured-pplan fpp-content">Unlimited</td>
            </tr>
            <tr>
                <td>eDrop Support Team</td>
                <td class="featured-pplan fpp-content">Hỗ trợ trong giờ làm việc từ thứ 2 đến thứ 6</td>
                <td class="featured-pplan fpp-content">Không giới hạn hỗ trợ 24/7 Call/SMS/Email/Fanpage/Secrete Group</td>
                <td class="featured-pplan fpp-content popular">Không giới hạn hỗ trợ 24/7 Call/SMS/Email/Fanpage/Secrete Group</td>
                <td class="featured-pplan fpp-content popular">Không giới hạn hỗ trợ 24/7 Call/SMS/Email/Fanpage/Secrete Group</td>
            </tr>
            <tr class="actions">
                <td></td>
                <td><a href="https://chrome.google.com/webstore/detail/edrop-drop-shipping-tool/jgpilhblmknfggjpkdcabajchcccgfeo?utm_source=chrome-app-launcher-info-dialog" class="btn outline pricing-btn small">Cài đặt FREE</a></td>
                <td>
                    <?php if(isset($user_id) && $user_id){
                        if(isset($pay_config) && $pay_config){?>
                    <form action="<?php echo $pay_config->form_buy_action;?>" method="post">

                        <!-- Identify your business so that you can collect the payments. -->
                        <input type="hidden" name="business" value="<?php echo $pay_config->email_business;?>">

                        <!-- Specify a Buy Now button. -->
                        <input type="hidden" name="cmd" value="_xclick">

                        <!-- Specify details about the item that buyers will purchase. -->
                        <input type="hidden" name="item_number" value="<?php echo $user_id;?>">
                        <input type="hidden" name="item_name" value="<?php echo $pay_config->name_1;?>">
                        <input type="hidden" name="amount" value="<?php echo $pay_config->price_1;?>">
                        <input type="hidden" name="currency_code" value="USD">
                        <input type="hidden" name="quantity" value="1">

                        <!-- Display the payment button. -->
                        <input type="image" name="submit" border="0"
                        src="<?php echo $asset.'images/btn_buynow_107x26.png';?>" alt="Buy Now">
                        <img alt="" border="0" width="1" height="1" src="<?php echo $asset.'images/pixel.gif';?>" >

                        <input name="notify_url" value="<?php echo base_url('payment/billing');?>" type="hidden">

                        <p>Upgrade for account: <?php echo $user_data['username'];?></p>
                    </form>
                <?php }
                    }else{?>
                    <a class="btn-login-to-buy" href="<?php echo base_url('auth/login?back='.urlencode(base_url('pricing')));?>"><img src="<?php echo $asset.'images/btn_buynow_107x26.png';?>" alt="Login"/></a>
                <?php }?>

                    <!-- <a href="javascript:void(0)" class="btn outline pricing-btn small">BUY NOW</a> -->
                </td>
                <td>
                    <?php if(isset($user_id) && $user_id){
                        if(isset($pay_config) && $pay_config){?>
                    <form action="<?php echo $pay_config->form_buy_action;?>" method="post">

                        <!-- Identify your business so that you can collect the payments. -->
                        <input type="hidden" name="business" value="<?php echo $pay_config->email_business;?>">

                        <!-- Specify a Buy Now button. -->
                        <input type="hidden" name="cmd" value="_xclick">

                        <!-- Specify details about the item that buyers will purchase. -->
                        <input type="hidden" name="item_number" value="<?php echo $user_id;?>">
                        <input type="hidden" name="item_name" value="<?php echo $pay_config->name_2;?>">
                        <input type="hidden" name="amount" value="<?php echo $pay_config->price_2;?>">
                        <input type="hidden" name="currency_code" value="USD">
                        <input type="hidden" name="quantity" value="1">

                        <!-- Display the payment button. -->
                        <input type="image" name="submit" border="0"
                        src="<?php echo $asset.'images/btn_buynow_107x26.png';?>" alt="Buy Now">
                        <img alt="" border="0" width="1" height="1" src="<?php echo $asset.'images/pixel.gif';?>" >

                        <input name="notify_url" value="<?php echo base_url('payment/billing');?>" type="hidden">

                        <p>Upgrade for account: <?php echo $user_data['username'];?></p>
                    </form>
                    <?php }
                    }else{?>
                        <a class="btn-login-to-buy" href="<?php echo base_url('auth/login?back='.urlencode(base_url('pricing')));?>"><img src="<?php echo $asset.'images/btn_buynow_107x26.png';?>" alt="Login"/></a>
                    <?php }?>
                </td>
                <td>
                    <?php if(isset($user_id) && $user_id){
                        if(isset($pay_config) && $pay_config){?>
                    <form action="<?php echo $pay_config->form_buy_action;?>" method="post">

                        <!-- Identify your business so that you can collect the payments. -->
                        <input type="hidden" name="business" value="<?php echo $pay_config->email_business;?>">

                        <!-- Specify a Buy Now button. -->
                        <input type="hidden" name="cmd" value="_xclick">

                        <!-- Specify details about the item that buyers will purchase. -->
                        <input type="hidden" name="item_number" value="<?php echo $user_id;?>">
                        <input type="hidden" name="item_name" value="<?php echo $pay_config->name_3;?>">
                        <input type="hidden" name="amount" value="<?php echo $pay_config->price_3;?>">
                        <input type="hidden" name="currency_code" value="USD">
                        <input type="hidden" name="quantity" value="1">

                        <!-- Display the payment button. -->
                        <input type="image" name="submit" border="0"
                        src="<?php echo $asset.'images/btn_buynow_107x26.png';?>" alt="Buy Now">
                        <img alt="" border="0" width="1" height="1" src="<?php echo $asset.'images/pixel.gif';?>" >

                        <input name="notify_url" value="<?php echo base_url('payment/billing');?>" type="hidden">

                        <p>Upgrade for account: <?php echo $user_data['username'];?></p>
                    </form>
                    <?php }
                    }else{?>
                        <a class="btn-login-to-buy" href="<?php echo base_url('auth/login?back='.urlencode(base_url('pricing')));?>"><img src="<?php echo $asset.'images/btn_buynow_107x26.png';?>" alt="Login"/></a>
                    <?php }?>
                </td>
            </tr>
            </tbody>
        </table>

        <!-- <div class="div_thanhtoan">

            <h3 class="h3_text">Thanh toán qua chuyển khoản với khách hàng Việt Nam : </h3>

            <ul>
                <li>Đối với gói  1 tháng : 325.000 đồng</li>
                <li>Đối với gói  3 tháng : 980.000 đồng</li>
            </ul>

            <div style="width:50%;">

                    <p style="text-align: left;">
                        <img src="<?php echo $asset;?>images/vietcombank-logo.png" alt="Vietcombank">
                    </p>
                    <div class="payment-list clearfix">
                    <p class="payment-title">Ngân hàng Ngoại thương Việt Nam – VIETCOMBANK</p>
                    <p style="text-align: left;">– Chủ tài khoản: Vũ Văn Tâm</p>
                    <p style="text-align: left;">– Mã số tài khoản: 0451000351684</p>
                    <p style="text-align: left;">– Chi nhánh: Thanh Xuân – Hà Nội</p>
                    </div>
            </div>
        </div> -->

    </div>
</div>
