<?php $asset = base_url('assets/');
$ci =& get_instance();
$user_data = $ci->session->userdata('user_data');
?><!DOCTYPE html>
<html lang="en-US" class=" js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="pingback" href="">
    <title>TACA E-Learning</title>
    <meta name="description" content="">
    <meta property="og:type" content="article">
    <meta property="og:url" content="">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <link rel="shortcut icon" href="">
    <?php if (isset($asset_css) && !empty($asset_css)) {
        foreach ($asset_css as $item) {
            echo "\t".'<link rel="stylesheet" href="' . $item . '">'."\n";
        }
    }?>
    <link rel="stylesheet" href="<?php echo $asset;?>css/style.min.css" type="text/css" media="all">
    <link rel="stylesheet" href="<?php echo $asset;?>css/default.min.css" type="text/css" media="all">
    <link rel="stylesheet" href="<?php echo $asset;?>css/styles.css" type="text/css" media="all">
    <link rel="stylesheet" href="<?php echo $asset;?>css/responsive.css" type="text/css" media="all">
    <link rel="stylesheet"  href="<?php echo $asset;?>css/main_style.css" type="text/css" media="all">
    <script type="text/javascript">
        var base_url = '<?php echo base_url('');?>';
    </script>
    <script src="<?php echo $asset;?>js/jquery-3.2.1.min.js"></script>
<?php if (isset($asset_js) && !empty($asset_js)) {
    foreach ($asset_js as $item) {
        echo "\t".'<script type="text/javascript" src="' . $item . '"></script>'."\n";
    }
} ?>
    <script src="<?php echo $asset;?>js/main.js" crossorigin="anonymous"></script>
    <script type="text/javascript">
        $( document ).ready(function() {

            var button_page = $(".button_page");
            var $btn_install = $("#btn_1_3cec468c69089e16826d885c5d34fe89");
            if($btn_install.length>0){
                var content = $btn_install.offset();
                $(window).scroll(function(){
                    var screenPosition = $(document).scrollTop() + 100;
                    if (screenPosition < content.top) {
                        button_page.hide();
                    }
                    if (screenPosition >= content.top) {
                        button_page.show();
                    }
                });
            }
        });
    </script>

<body class="home page-template-default page op-live-editor-page op-theme op-custom-background" style="">
<div class="container main-content">

</div>



</body>

</html>
