<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends Front_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('course/course_model');
    }

    public function index()
    {
        $temp = array();

        $temp['template'] = 'landing/home';
       $allCourses =  $this->course_model->getAllCourses();
        $temp['allCourses'] = $allCourses;
        $this->load->view('site_layout', $temp);
    }

}
