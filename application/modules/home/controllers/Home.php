<?php

class Home extends front_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('course/course_model');
        $this->load->model('teacher/teacher_model');
        $user_data = $this->session->userdata('user_data');

    }
    function index(){
        $temp = array();
        $this->load->view('site_layout.php', $temp);
    }

}