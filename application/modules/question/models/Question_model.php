<?php

class Question_model extends Base_model
{

    protected $table = 'questions';

    function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function get_question_by($type,$id)
    {
        $field = $type.'_id';
        $this->db->select('*')
            ->from($this->table)
            ->where($field, $id);
        $rs = $this->db->get();
        $return = $rs->result_array();
        $rs->free_result();
        return $return;
    }
    function save($data, $id = null)
    {
        if ($id && $id > 0) {
            $this->db->update($this->table, $data)->where('id', $id);
            return $id;
        } else {
            $this->db->insert($this->table, $data);
            return $this->db->insert_id();
        }
    }

}

?>